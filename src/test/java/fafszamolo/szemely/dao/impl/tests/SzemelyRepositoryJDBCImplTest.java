package fafszamolo.szemely.dao.impl.tests;

import fafszamolo.exception.FafDAOException;
import fafszamolo.model.Rendfokozat;
import fafszamolo.model.Szemely;
import fafszamolo.szemely.dao.impl.SzemelyRepositoryJDBCImpl;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 * A SzemelyRepositoryJDBCImpl osztály tesztosztálya.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SzemelyRepositoryJDBCImplTest {
    
    private static SzemelyRepositoryJDBCImpl jdbc;
    private static Szemely testSzemely;
    private static Connection conn;
            
    public SzemelyRepositoryJDBCImplTest() {
    }
    
    @BeforeClass
    public static void setUpClass() throws SQLException {
        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/faftablazat?serverTimezone=UTC", "root", "1234");
            jdbc = new SzemelyRepositoryJDBCImpl(conn);
            int sztsz = 98765432;
            String nev = "Test Test";
            Rendfokozat rf = Rendfokozat.ORNAGY;
            String alakulat = "BOCSKAI";
            LocalDate szulDatum = LocalDate.of(2000, 1, 1);
            String anyjaNeve = "Test Mother";
            boolean ferfi = true;
            int magassag = 190;
            int suly = 95;
            double testZsir = 10.5;
            testSzemely = new Szemely(sztsz, nev, rf, alakulat, szulDatum, anyjaNeve, ferfi, magassag, suly, testZsir);
        } catch (FafDAOException ex) {
            ex.printStackTrace();
        }
    }
    
    @AfterClass
    public static void tearDownClass() {
        try {
            Szemely deleteSzemelyTest = jdbc.findBySZTSZ(98765432);
            jdbc.deleteSzemely(deleteSzemelyTest.getSorszam());
            deleteSzemelyTest = jdbc.findBySZTSZ(98765432);
            assertTrue("A deleteSzemely metódus nem működik jól: szerepel a teszt személy az adatbázisban törlés után!", deleteSzemelyTest == null);
        } catch (FafDAOException ex) {
            ex.printStackTrace();
        }
        
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void findByNevAndSaveSzemelyTest() {
        try {
            assertTrue("Nem null-t állít be sorszámnak!", testSzemely.getSorszam() == null);
            jdbc.saveSzemely(testSzemely);
            List<Szemely> findByNevTest = jdbc.findByNev("Test Test");
            assertTrue("Nem egy személyt ad hozzá az adatbázishoz!", findByNevTest.size() == 1);
            Szemely findByNevSzemelyTest = findByNevTest.get(0);
            assertTrue("Nincs az adatbázisban a teszt személy!", findByNevSzemelyTest != null);
            assertTrue("Nincs az adatbázisban a teszt személy!", findByNevSzemelyTest.getNev().equals("Test Test"));
            assertTrue("Az adatbázis nem ad neki sorszámot!", findByNevSzemelyTest.getSorszam() != null);
        } catch (FafDAOException ex) {
            ex.printStackTrace();
        }
    }
    
    @Test
    public void findBySZTSZTest() {
        try {
            Szemely findBySZTSZTest = jdbc.findBySZTSZ(98765432);
            assertTrue("Nincs az adatbázisban a teszt személy!", findBySZTSZTest.getSztsz() == 98765432);
        } catch (FafDAOException ex) {
            ex.printStackTrace();
        } 
    }
    
    @Test
    public void updateSzemelyTest() {
        try {
            Szemely testSzemelyBefore = jdbc.findBySZTSZ(98765432);
            assertTrue("Nincsenek jól beállítva az adatok, vagy nem működik az insert metódus!", testSzemelyBefore.getRf().equals(Rendfokozat.ORNAGY));
            assertTrue("Nincsenek jól beállítva az adatok, vagy nem működik az insert metódus!", testSzemelyBefore.isFerfi());
            testSzemelyBefore.setRf(Rendfokozat.ALEZREDES);
            testSzemelyBefore.setIsFerfi(false);
            jdbc.updateSzemely(testSzemelyBefore);
            Szemely testSzemelyAfter = jdbc.findBySZTSZ(98765432);
            assertTrue("Nem frissíti a személy adatait!", testSzemelyAfter.getRf().equals(Rendfokozat.ALEZREDES));
            assertFalse("Nem frissíti a személy adatait!", testSzemelyAfter.isFerfi());
        } catch (FafDAOException ex) {
            ex.printStackTrace();
        }
    }
    
}
