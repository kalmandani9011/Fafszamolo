package fafszamolo.model.tests;

import fafszamolo.model.FizikaiAllapotFelmeres;
import java.time.LocalDate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author
 */
public class FizikaiAllapotFelmeresTest {
    
    public FizikaiAllapotFelmeresTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void constructorGetterTestOne() {
        LocalDate vegrehajtasIdeje = LocalDate.of(2020, 5, 5);
        FizikaiAllapotFelmeres felmeresTest = new FizikaiAllapotFelmeres(1, 50034531, vegrehajtasIdeje, 0, 50, 0, 60, 0, 1230);
        assertEquals("Hiba! Nem egyezik a sorszám értéke!", Integer.valueOf(1), felmeresTest.getSorszam());
        assertEquals("Hiba! Nem egyezik az SZTSZ értéke!", 50034531, felmeresTest.getSztsz());
        assertTrue("Hiba! Nem egyezik a végrehajtás ideje!", felmeresTest.getVegrehajtasIdeje().isEqual(LocalDate.of(2020, 5, 5)));
        assertEquals("Hiba! Nem egyezik a kar mozgásforma értéke!", 0, felmeresTest.getKarMozgasforma());
        assertEquals("Hiba! Nem egyezik a kar eredmény értéke!", 50, felmeresTest.getKarEredmeny());
    }
    
    @Test
    public void constructorGetterTestTwo() {
        LocalDate vegrehajtasIdeje = LocalDate.of(2020, 5, 5);
        FizikaiAllapotFelmeres felmeresTest = new FizikaiAllapotFelmeres(1, 50034531, vegrehajtasIdeje, 0, 50, 0, 60, 0, 1230);
        assertEquals("Hiba! Nem egyezik a törzs mozgásforma értéke!", 0, felmeresTest.getTorzsMozgasforma());
        assertEquals("Hiba! Nem egyezik a törzs eredmény értéke!", 60, felmeresTest.getTorzsEredmeny());
        assertEquals("Hiba! Nem egyezik a keringés mozgásforma értéke!", 0, felmeresTest.getKeringesMozgasforma());
        assertEquals("Hiba! Nem egyezik a keringés eredmény értéke!", 1230, felmeresTest.getKeringesEredmeny());
        assertEquals("Hiba! Nem egyezik a pontszám értéke!", 0, felmeresTest.getPontszam());
        assertNull("Hiba! Nem null a bmi értéke!", felmeresTest.getBmi());
        assertNull("Hiba! Nem null a testzsír index értéke!", felmeresTest.getTestZsirIndex());
    }
    
    @Test
    public void constructorGetterTestThree() {
        LocalDate vegrehajtasIdeje = LocalDate.of(2019, 4, 23);
        FizikaiAllapotFelmeres felmeresTest = new FizikaiAllapotFelmeres(2, 50012345, vegrehajtasIdeje, 1, 78, 1, 16, 1, 1352, 300, "MF", "MF");
        assertEquals("Hiba! Nem egyezik a sorszám értéke!", Integer.valueOf(2), felmeresTest.getSorszam());
        assertEquals("Hiba! Nem egyezik az SZTSZ értéke!", 50012345, felmeresTest.getSztsz());
        assertTrue("Hiba! Nem egyezik a végrehajtás ideje!", felmeresTest.getVegrehajtasIdeje().isEqual(LocalDate.of(2019, 4, 23)));
        assertEquals("Hiba! Nem egyezik a kar mozgásforma értéke!", 1, felmeresTest.getKarMozgasforma());
        assertEquals("Hiba! Nem egyezik a kar eredmény értéke!", 78, felmeresTest.getKarEredmeny());
        
    }
    
    @Test
    public void constructorGetterTestFour() {
        LocalDate vegrehajtasIdeje = LocalDate.of(2019, 4, 23);
        FizikaiAllapotFelmeres felmeresTest = new FizikaiAllapotFelmeres(2, 50012345, vegrehajtasIdeje, 1, 78, 1, 16, 1, 1352, 300, "MF", "MF");
        assertEquals("Hiba! Nem egyezik a törzs mozgásforma értéke!", 1, felmeresTest.getTorzsMozgasforma());
        assertEquals("Hiba! Nem egyezik a törzs eredmény értéke!", 16, felmeresTest.getTorzsEredmeny());
        assertEquals("Hiba! Nem egyezik a keringés mozgásforma értéke!", 1, felmeresTest.getKeringesMozgasforma());
        assertEquals("Hiba! Nem egyezik a keringés eredmény értéke!", 1352, felmeresTest.getKeringesEredmeny());
        assertEquals("Hiba! Nem egyezik a pontszám értéke!", 300, felmeresTest.getPontszam());
        assertEquals("Hiba! Nem egyezik a bmi értéke!", "MF", felmeresTest.getBmi());
        assertEquals("Hiba! Nem egyezik a testzsír index értéke!", "MF",  felmeresTest.getTestZsirIndex());
    }
    
    @Test
    public void constructorGetterTestFive() {
        FizikaiAllapotFelmeres felmeresTest = new FizikaiAllapotFelmeres();
        assertNull("Hiba! Nem null a sorszám értéke!", felmeresTest.getSorszam());
        assertEquals("Hiba! Nem nulla az SZTSZ értéke!", 0, felmeresTest.getSztsz());
        assertNull("Hiba! Nem null a végrehajtás ideje!", felmeresTest.getVegrehajtasIdeje());
        assertEquals("Hiba! Nem nulla a kar mozgásforma értéke!", 0, felmeresTest.getKarMozgasforma());
        assertEquals("Hiba! Nem nulla a kar eredmény értéke!", 0, felmeresTest.getKarEredmeny());
        assertEquals("Hiba! Nem nulla a törzs mozgásforma értéke!", 0, felmeresTest.getTorzsMozgasforma());
        assertEquals("Hiba! Nem nulla a törzs eredmény értéke!", 0, felmeresTest.getTorzsEredmeny());
        assertEquals("Hiba! Nem nulla a keringés mozgásforma értéke!", 0, felmeresTest.getKeringesMozgasforma());
        assertEquals("Hiba! Nem nulla a keringés eredmény értéke!", 0, felmeresTest.getKeringesEredmeny());
        assertEquals("Hiba! Nem nulla a pontszám értéke!", 0, felmeresTest.getPontszam());
        assertNull("Hiba! Nem null a bmi értéke!", felmeresTest.getBmi());
        assertNull("Hiba! Nem null a testzsír index értéke!", felmeresTest.getTestZsirIndex());
    }
    
    @Test
    public void constructorGetterSetterTestOne() {
        FizikaiAllapotFelmeres felmeresTest = new FizikaiAllapotFelmeres();
        felmeresTest.setSorszam(3);
        felmeresTest.setSztsz(29349876);
        felmeresTest.setVegrehajtasIdeje(LocalDate.of(2019, 7, 2));
        felmeresTest.setKarMozgasforma(1);
        felmeresTest.setKarEredmeny(20);
        assertEquals("Hiba! Nem egyezik a sorszám értéke!", Integer.valueOf(3), felmeresTest.getSorszam());
        assertEquals("Hiba! Nem egyezik az SZTSZ értéke!", 29349876, felmeresTest.getSztsz());
        assertTrue("Hiba! Nem egyezik a végrehajtás ideje!", felmeresTest.getVegrehajtasIdeje().isEqual(LocalDate.of(2019, 7, 2)));
        assertEquals("Hiba! Nem egyezik a kar mozgásforma értéke!", 1, felmeresTest.getKarMozgasforma());
        assertEquals("Hiba! Nem egyezik a kar eredmény értéke!", 20, felmeresTest.getKarEredmeny());
    }
    
    @Test
    public void constructorGetterSetterTestTwo() {
        FizikaiAllapotFelmeres felmeresTest = new FizikaiAllapotFelmeres();
        felmeresTest.setTorzsMozgasforma(1);
        felmeresTest.setTorzsEredmeny(47);
        felmeresTest.setKeringesMozgasforma(1);
        felmeresTest.setKeringesEredmeny(1534);
        felmeresTest.setPontszam(333);
        felmeresTest.setBmi("MF");
        felmeresTest.setTestZsirIndex("NMF");
        assertEquals("Hiba! Nem egyezik a törzs mozgásforma értéke!", 1, felmeresTest.getTorzsMozgasforma());
        assertEquals("Hiba! Nem egyezik a törzs eredmény értéke!", 47, felmeresTest.getTorzsEredmeny());
        assertEquals("Hiba! Nem egyezik a keringés mozgásforma értéke!", 1, felmeresTest.getKeringesMozgasforma());
        assertEquals("Hiba! Nem egyezik a keringés eredmény értéke!", 1534, felmeresTest.getKeringesEredmeny());
        assertEquals("Hiba! Nem egyezik a pontszám értéke!", 333, felmeresTest.getPontszam());
        assertEquals("Hiba! Nem egyezik a bmi értéke!", "MF", felmeresTest.getBmi());
        assertEquals("Hiba! Nem egyezik a testzsír index értéke!", "NMF",  felmeresTest.getTestZsirIndex());
    }
    
    @Test
    public void constructorGetterSetterTestThree() {
        LocalDate vegrehajtasIdeje = LocalDate.of(2020, 10, 19);
        FizikaiAllapotFelmeres felmeresTest = new FizikaiAllapotFelmeres(4, 50024689, vegrehajtasIdeje, 2, 45, 2, 81, 2, 1316);
        felmeresTest.setSorszam(5);
        felmeresTest.setSztsz(12345678);
        felmeresTest.setVegrehajtasIdeje(LocalDate.of(2018, 9, 30));
        felmeresTest.setKarMozgasforma(0);
        felmeresTest.setKarEredmeny(66);
        assertEquals("Hiba! Nem egyezik a sorszám értéke!", Integer.valueOf(5), felmeresTest.getSorszam());
        assertEquals("Hiba! Nem egyezik az SZTSZ értéke!", 12345678, felmeresTest.getSztsz());
        assertTrue("Hiba! Nem egyezik a végrehajtás ideje!", felmeresTest.getVegrehajtasIdeje().isEqual(LocalDate.of(2018, 9, 30)));
        assertEquals("Hiba! Nem egyezik a kar mozgásforma értéke!", 0, felmeresTest.getKarMozgasforma());
        assertEquals("Hiba! Nem egyezik a kar eredmény értéke!", 66, felmeresTest.getKarEredmeny());
    }
    
    @Test
    public void constructorGetterSetterTestFour() {
        LocalDate vegrehajtasIdeje = LocalDate.of(2020, 10, 19);
        FizikaiAllapotFelmeres felmeresTest = new FizikaiAllapotFelmeres(4, 50024689, vegrehajtasIdeje, 2, 45, 2, 81, 2, 1316);
        felmeresTest.setTorzsMozgasforma(0);
        felmeresTest.setTorzsEredmeny(100);
        felmeresTest.setKeringesMozgasforma(3);
        felmeresTest.setKeringesEredmeny(322);
        felmeresTest.setPontszam(279);
        felmeresTest.setBmi("NMF");
        felmeresTest.setTestZsirIndex("NMF");
        assertEquals("Hiba! Nem egyezik a törzs mozgásforma értéke!", 0, felmeresTest.getTorzsMozgasforma());
        assertEquals("Hiba! Nem egyezik a törzs eredmény értéke!", 100, felmeresTest.getTorzsEredmeny());
        assertEquals("Hiba! Nem egyezik a keringés mozgásforma értéke!", 3, felmeresTest.getKeringesMozgasforma());
        assertEquals("Hiba! Nem egyezik a keringés eredmény értéke!", 322, felmeresTest.getKeringesEredmeny());
        assertEquals("Hiba! Nem egyezik a pontszám értéke!", 279, felmeresTest.getPontszam());
        assertEquals("Hiba! Nem egyezik a bmi értéke!", "NMF", felmeresTest.getBmi());
        assertEquals("Hiba! Nem egyezik a testzsír index értéke!", "NMF",  felmeresTest.getTestZsirIndex());
    }
}
