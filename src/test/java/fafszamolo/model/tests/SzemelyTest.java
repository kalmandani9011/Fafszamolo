package fafszamolo.model.tests;

import fafszamolo.model.FizikaiAllapotFelmeres;
import fafszamolo.model.Rendfokozat;
import fafszamolo.model.Szemely;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author
 */
public class SzemelyTest {
    
    public SzemelyTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void constructorGetterTestOne() {
        Integer sorszam = 1;
        int sztsz = 50012345;
        String nev = "Rogal Dorn";
        Rendfokozat rf = Rendfokozat.VEZEREZREDES;
        String alakulat = "BOCSKAI";
        LocalDate szulDatum = LocalDate.of(1940, 11, 30);
        Szemely szemelyTest = new Szemely(sorszam, sztsz, nev, rf, alakulat, szulDatum, "", true, 250, 200, 10);
        assertEquals("Hiba! Nem egyezik a sorszám értéke!", Integer.valueOf(1), szemelyTest.getSorszam());
        assertEquals("Hiba! Nem egyezik az SZTSZ értéke!", 50012345, szemelyTest.getSztsz());
        assertTrue("Hiba! Nem egyezik a név!", szemelyTest.getNev().equals("Rogal Dorn"));
        assertTrue("Hiba! Nem egyezik a rendfokozat!", szemelyTest.getRf().equals(Rendfokozat.VEZEREZREDES));
        assertTrue("Hiba! Nem egyezik az alakulat!", szemelyTest.getAlakulat().equals("BOCSKAI"));
        assertTrue("Hiba! Nem egyezik a születési dátum!", szemelyTest.getSzulDatum().isEqual(LocalDate.of(1940, 11, 30)));
        assertEquals("Hiba! Nem egyezik a toString() metódus visszatérési értéke!", "1   | 50012345 | Rogal Dorn vezérezredes                 ", szemelyTest.toString());
    }
    
    @Test
    public void constructorGetterTestTwo() {
        Rendfokozat rf = Rendfokozat.VEZEREZREDES;
        String alakulat = "BOCSKAI";
        LocalDate szulDatum = LocalDate.of(1940, 11, 30);
        String anyjaNeve = "Princess Leia";
        boolean ferfi = true;
        int magassag = 250;
        int suly = 200;
        double testZsir = 10.4;
        Szemely szemelyTest = new Szemely(1, 50012345, "Rogal Dorn", rf, alakulat, szulDatum, anyjaNeve, ferfi, magassag, suly, testZsir);
        assertTrue("Hiba! Nem egyezik az anyja neve!", szemelyTest.getAnyjaNeve().equals("Princess Leia"));
        assertTrue("Hiba! Nem egyezik a férfi/nő értéke!", szemelyTest.isFerfi());
        assertEquals("Hiba! Nem egyezik a magasság!", 250, szemelyTest.getMagassag());
        assertEquals("Hiba! Nem egyezik a súly!", 200, szemelyTest.getSuly());
        assertEquals("Hiba! Nem egyezik a test zsír értéke!", 10.4, szemelyTest.getTestZsir(), 0);
        assertEquals("Hiba! Nem egyezik a kor!", 81, szemelyTest.getKor());
        assertTrue("Hiba! Nem üres a felmérések listája!", szemelyTest.getFelmeresek().isEmpty());
    }
    
    @Test
    public void constructorGetterTestThree() {
        Rendfokozat rf = Rendfokozat.ORMESTER;
        LocalDate szulDatum = LocalDate.of(1995, 10, 11);
        Szemely szemelyTest = new Szemely(50054321, "Lukács Adrienn", rf, "KLAPKA", szulDatum, "Nagy Mónika", false, 167, 53, 13.9);
        assertNull("Hiba! Nem null a sorszám értéke!", szemelyTest.getSorszam());
        assertEquals("Hiba! Nem egyezik az SZTSZ értéke!", 50054321, szemelyTest.getSztsz());
        assertTrue("Hiba! Nem egyezik a név!", szemelyTest.getNev().equals("Lukács Adrienn"));
        assertTrue("Hiba! Nem egyezik a rendfokozat!", szemelyTest.getRf().equals(Rendfokozat.ORMESTER));
        assertTrue("Hiba! Nem egyezik az alakulat!", szemelyTest.getAlakulat().equals("KLAPKA"));
        assertTrue("Hiba! Nem egyezik a születési dátum!", szemelyTest.getSzulDatum().isEqual(LocalDate.of(1995, 10, 11)));
        assertTrue("Hiba! Nem egyezik az anyja neve!", szemelyTest.getAnyjaNeve().equals("Nagy Mónika"));
        assertFalse("Hiba! Nem egyezik a férfi/nő értéke!", szemelyTest.isFerfi());
        assertEquals("Hiba! Nem egyezik a magasság!", 167, szemelyTest.getMagassag());
        assertEquals("Hiba! Nem egyezik a súly!", 53, szemelyTest.getSuly());
        assertEquals("Hiba! Nem egyezik a test zsír értéke!", 13.9, szemelyTest.getTestZsir(), 0);
        assertEquals("Hiba! Nem egyezik a kor!", 26, szemelyTest.getKor());
        assertTrue("Hiba! Nem üres a felmérések listája!", szemelyTest.getFelmeresek().isEmpty());
        assertEquals("Hiba! Nem egyezik a toString() metódus visszatérési értéke!", "null| 50054321 | Lukács Adrienn őrmester                 ", szemelyTest.toString());
    }
    
    @Test
    public void constructorGetterTestFour() {
        Szemely szemelyTest = new Szemely();
        assertNull("Hiba! Nem null a sorszám értéke!", szemelyTest.getSorszam());
        assertEquals("Hiba! Nem nulla az SZTSZ értéke!", 0, szemelyTest.getSztsz());
        assertNull("Hiba! Nem null a név értéke!", szemelyTest.getNev());
        assertNull("Hiba! Nem null a rendfokozat értéke!", szemelyTest.getRf());
        assertNull("Hiba! Nem null az alakulat értéke!", szemelyTest.getAlakulat());
        assertNull("Hiba! Nem null a születési dátum értéke!", szemelyTest.getSzulDatum());
        assertNull("Hiba! Nem null az anyja neve értéke!", szemelyTest.getAnyjaNeve());
        assertFalse("Hiba! Nem false a férfi/nő értéke!", szemelyTest.isFerfi());
        assertEquals("Hiba! Nem nulla a magasság értéke!", 0, szemelyTest.getMagassag());
        assertEquals("Hiba! Nem nulla a súly értéke!", 0, szemelyTest.getSuly());
        assertEquals("Hiba! Nem nulla a test zsír értéke!", 0, szemelyTest.getTestZsir(), 0);
        assertEquals("Hiba! Nem nulla a kor értéke!", 0, szemelyTest.getKor());
        assertNull("Hiba! Nem null a felmérések listája!", szemelyTest.getFelmeresek());
        assertEquals("Hiba! Nem egyezik a toString() metódus visszatérési értéke!", "", szemelyTest.toString());
    }
    
    @Test
    public void constructorSetterGetterTestOne() {
        Szemely szemelyTest = new Szemely();
        szemelyTest.setSorszam(2);
        szemelyTest.setSztsz(12345678);
        szemelyTest.setNev("Tóth Tibor");
        szemelyTest.setRf(Rendfokozat.ORVEZETO);
        szemelyTest.setAlakulat("KLAPKA");
        szemelyTest.setSzulDatum(LocalDate.of(1997, 5, 18));
        assertEquals("Hiba! Nem egyezik a sorszám értéke!", Integer.valueOf(2), szemelyTest.getSorszam());
        assertEquals("Hiba! Nem egyezik az SZTSZ értéke!", 12345678, szemelyTest.getSztsz());
        assertTrue("Hiba! Nem egyezik a név!", szemelyTest.getNev().equals("Tóth Tibor"));
        assertTrue("Hiba! Nem egyezik a rendfokozat!", szemelyTest.getRf().equals(Rendfokozat.ORVEZETO));
        assertTrue("Hiba! Nem egyezik az alakulat!", szemelyTest.getAlakulat().equals("KLAPKA"));
        assertTrue("Hiba! Nem egyezik a születési dátum!", szemelyTest.getSzulDatum().isEqual(LocalDate.of(1997, 5, 18)));
        assertEquals("Hiba! Nem egyezik a kor!", 24, szemelyTest.getKor());
        assertEquals("Hiba! Nem egyezik a toString() metódus visszatérési értéke!", "2   | 12345678 | Tóth Tibor őrvezető                     ", szemelyTest.toString());
    }
    
    @Test
    public void constructorSetterGetterTestTwo() {
        Szemely szemelyTest = new Szemely();
        szemelyTest.setAnyjaNeve("Ordas Emese");
        szemelyTest.setIsFerfi(true);
        szemelyTest.setMagassag(182);
        szemelyTest.setSuly(78);
        szemelyTest.setTestZsir(6.7);
        assertTrue("Hiba! Nem egyezik az anyja neve!", szemelyTest.getAnyjaNeve().equals("Ordas Emese"));
        assertTrue("Hiba! Nem egyezik a férfi/nő értéke!", szemelyTest.isFerfi());
        assertEquals("Hiba! Nem egyezik a magasság!", 182, szemelyTest.getMagassag());
        assertEquals("Hiba! Nem egyezik a súly!", 78, szemelyTest.getSuly());
        assertEquals("Hiba! Nem egyezik a test zsír értéke!", 6.7, szemelyTest.getTestZsir(), 0);
        assertNull("Hiba! Nem null a felmérések listája!", szemelyTest.getFelmeresek());
    }
    
    @Test
    public void constructorSetterGetterTestThree() {
        Rendfokozat rf = Rendfokozat.ORMESTER;
        String alakulat = "BOCSKAI";
        LocalDate szulDatum = LocalDate.of(1992, 2, 5);
        FizikaiAllapotFelmeres felmeres = new FizikaiAllapotFelmeres();
        FizikaiAllapotFelmeres felmeres2 = new FizikaiAllapotFelmeres();
        List<FizikaiAllapotFelmeres> felmeresListTest = new ArrayList<>();
        felmeresListTest.add(felmeres);
        felmeresListTest.add(felmeres2);
        Szemely szemelyTest = new Szemely(3, 43215678, "Ambrus Levente", rf, alakulat, szulDatum, "Kiss Anna", true, 177, 72, 11);
        szemelyTest.setSorszam(4);
        szemelyTest.setSztsz(98765432);
        szemelyTest.setNev("Fekete Renáta");
        szemelyTest.setRf(Rendfokozat.FOTORZSORMESTER);
        szemelyTest.setFelmeresek(felmeresListTest);
        assertEquals("Hiba! Nem egyezik a sorszám értéke!", Integer.valueOf(4), szemelyTest.getSorszam());
        assertEquals("Hiba! Nem egyezik az SZTSZ értéke!", 98765432, szemelyTest.getSztsz());
        assertTrue("Hiba! Nem egyezik a név!", szemelyTest.getNev().equals("Fekete Renáta"));
        assertTrue("Hiba! Nem egyezik a rendfokozat!", szemelyTest.getRf().equals(Rendfokozat.FOTORZSORMESTER));
        assertFalse("Hiba! Nem lehet üres a felmérések listája!", szemelyTest.getFelmeresek().isEmpty());
        assertEquals("Hiba! Nem egyezik a felmérések száma!", 2, szemelyTest.getFelmeresek().size());
        assertEquals("Hiba! Nem egyezik a toString() metódus visszatérési értéke!", "4   | 98765432 | Fekete Renáta főtörzsőrmester           ", szemelyTest.toString());
    }
    
    @Test
    public void constructorSetterGetterTestFour() {
        Rendfokozat rf = Rendfokozat.ORMESTER;
        String alakulat = "BOCSKAI";
        LocalDate szulDatum = LocalDate.of(1992, 2, 5);
        Szemely szemelyTest = new Szemely(3, 43215678, "Ambrus Levente", rf, alakulat, szulDatum, "Kiss Anna", true, 177, 72, 11);
        szemelyTest.setAlakulat("KLAPKA");
        szemelyTest.setSzulDatum(LocalDate.of(1988, 9, 23));
        szemelyTest.setAnyjaNeve("Vígh Melinda");
        szemelyTest.setIsFerfi(false);
        szemelyTest.setMagassag(163);
        szemelyTest.setSuly(50);
        szemelyTest.setTestZsir(13.2);
        assertTrue("Hiba! Nem egyezik az alakulat!", szemelyTest.getAlakulat().equals("KLAPKA"));
        assertTrue("Hiba! Nem egyezik a születési dátum!", szemelyTest.getSzulDatum().isEqual(LocalDate.of(1988, 9, 23)));
        assertTrue("Hiba! Nem egyezik az anyja neve!", szemelyTest.getAnyjaNeve().equals("Vígh Melinda"));
        assertFalse("Hiba! Nem egyezik a férfi/nő értéke!", szemelyTest.isFerfi());
        assertEquals("Hiba! Nem egyezik a magasság!", 163, szemelyTest.getMagassag());
        assertEquals("Hiba! Nem egyezik a súly!", 50, szemelyTest.getSuly());
        assertEquals("Hiba! Nem egyezik a test zsír értéke!", 13.2, szemelyTest.getTestZsir(), 0);
        assertEquals("Hiba! Nem egyezik a kor!", 33, szemelyTest.getKor());
    }
    
}
