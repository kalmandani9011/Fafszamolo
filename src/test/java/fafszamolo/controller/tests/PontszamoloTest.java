package fafszamolo.controller.tests;

import fafszamolo.controller.PontSzamolo;
import fafszamolo.exception.FafDAOException;
import fafszamolo.felmeres.dao.FelmeresRepository;
import fafszamolo.felmeres.dao.impl.FelmeresRepositoryJDBCImpl;
import fafszamolo.model.Szemely;
import fafszamolo.mozgasformak.dao.MozgasformakRepository;
import fafszamolo.mozgasformak.dao.impl.MozgasformakRepositoryJDBCImpl;
import fafszamolo.szemely.dao.SzemelyRepository;
import fafszamolo.szemely.dao.impl.SzemelyRepositoryJDBCImpl;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * A PontSzamolo osztály tesztosztálya.
 */
public class PontszamoloTest {
    
    private static PontSzamolo szamolo;
    private Szemely testSzemely;
    private static Connection conn;
    
    public PontszamoloTest() {
    }
    
    @BeforeClass
    public static void setUpClass() throws SQLException {
        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/faftablazat?serverTimezone=UTC", "root", "1234");
            SzemelyRepository szemelyRepo = new SzemelyRepositoryJDBCImpl(conn);
            FelmeresRepository felmeresRepo = new FelmeresRepositoryJDBCImpl(conn);
            MozgasformakRepository mozgasRepo = new MozgasformakRepositoryJDBCImpl(conn);
            szamolo = new PontSzamolo(szemelyRepo, felmeresRepo, mozgasRepo);
        } catch (FafDAOException ex) {
            ex.printStackTrace();
        }
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        testSzemely = new Szemely();
        testSzemely.setIsFerfi(true);
        testSzemely.setTestZsir(7.9);
        testSzemely.setSzulDatum(LocalDate.of(2000, 3, 3));
        testSzemely.setMagassag(180);
        testSzemely.setSuly(90);
    }
    
    @After
    public void tearDown() {
        szamolo.setMinosites("MF");
    }

    @Test
    public void getterTest() {
        assertTrue("Hiba a getterben, vagy a konstruktorban!", szamolo.getSzemelyRepo() != null);
        assertTrue("Hiba a getterben, vagy a konstruktorban!", szamolo.getFelmeresRepo() != null);
    }
    
    @Test
    public void minositesTest() {
        assertEquals("MF", szamolo.getMinosites());
        szamolo.setMinosites("NMF");
        assertEquals("NMF", szamolo.getMinosites());
    }
    
    @Test
    public void testZsirIndexTest() {
        String index = szamolo.testZsirIndexMeghataroz(testSzemely);
        assertEquals("NMF", index);
        
        testSzemely.setTestZsir(8.0);
        index = szamolo.testZsirIndexMeghataroz(testSzemely);
        assertEquals("MF", index);
        
        testSzemely.setTestZsir(15.9);
        index = szamolo.testZsirIndexMeghataroz(testSzemely);
        assertEquals("MF", index);
        
        testSzemely.setTestZsir(17.6);
        index = szamolo.testZsirIndexMeghataroz(testSzemely);
        assertEquals("NMF", index);
        
        testSzemely.setIsFerfi(false);
        testSzemely.setTestZsir(19.9);
        index = szamolo.testZsirIndexMeghataroz(testSzemely);
        assertEquals("NMF", index);
            
        testSzemely.setTestZsir(30.0);
        index = szamolo.testZsirIndexMeghataroz(testSzemely);
        assertEquals("MF", index);
    }

    @Test
    public void bmiIndexTest() {
        try {
            String bmi = szamolo.bmiMeghataroz(testSzemely);
            assertEquals("NMF", bmi);
            
            testSzemely.setSuly(89);
            bmi = szamolo.bmiMeghataroz(testSzemely);
            assertEquals("MF", bmi);
            
            testSzemely.setMagassag(190);
            testSzemely.setSuly(63);
            bmi = szamolo.bmiMeghataroz(testSzemely);
            assertEquals("NMF", bmi);
            
            testSzemely.setSuly(64);
            bmi = szamolo.bmiMeghataroz(testSzemely);
            assertEquals("MF", bmi);
            
            testSzemely.setSuly(118);
            testSzemely.setIsFerfi(false);
            testSzemely.setSzulDatum(LocalDate.of(1975, 1, 1));
            bmi = szamolo.bmiMeghataroz(testSzemely);
            assertEquals("NMF", bmi);
            
            testSzemely.setSuly(117);
            bmi = szamolo.bmiMeghataroz(testSzemely);
            assertEquals("MF", bmi);
            
            testSzemely.setSzulDatum(LocalDate.of(1976, 1, 1));
            bmi = szamolo.bmiMeghataroz(testSzemely);
            assertEquals("NMF", bmi);
            
            testSzemely.setSuly(111);
            bmi = szamolo.bmiMeghataroz(testSzemely);
            assertEquals("MF", bmi);
        } catch (FafDAOException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void getEredmenyTest() {
        try {
            int karFajta = 0;
            int karEredmeny = 69;
            int torzsFajta = 0;
            int torzsEredmeny = 84;
            int keringesFajta = 0;
            int keringesEredmeny = 1340;
            int pontszam = szamolo.getEredmeny(testSzemely.isFerfi(), testSzemely.getKor(), karFajta, karEredmeny, torzsFajta, torzsEredmeny, keringesFajta, keringesEredmeny);
            assertEquals(350, pontszam);
            assertEquals("MF", szamolo.getMinosites());
            
            karEredmeny = 76;
            torzsEredmeny = 90;
            keringesEredmeny = 1310;
            pontszam = szamolo.getEredmeny(testSzemely.isFerfi(), testSzemely.getKor(), karFajta, karEredmeny, torzsFajta, torzsEredmeny, keringesFajta, keringesEredmeny);
            assertEquals(360, pontszam);
            assertEquals("MF", szamolo.getMinosites());
            
            karEredmeny = 13;
            pontszam = szamolo.getEredmeny(testSzemely.isFerfi(), testSzemely.getKor(), karFajta, karEredmeny, torzsFajta, torzsEredmeny, keringesFajta, keringesEredmeny);
            assertEquals(260, pontszam);
            assertEquals("NMF", szamolo.getMinosites());
            szamolo.setMinosites("MF");
            
            testSzemely.setSzulDatum(LocalDate.of(1975, 3, 3));
            karFajta = 1;
            torzsFajta = 1;
            keringesFajta = 1;
            karEredmeny = 19;
            torzsEredmeny = 72;
            keringesEredmeny = 1040;
            pontszam = szamolo.getEredmeny(testSzemely.isFerfi(), testSzemely.getKor(), karFajta, karEredmeny, torzsFajta, torzsEredmeny, keringesFajta, keringesEredmeny);
            assertEquals(326, pontszam);
            assertEquals("MF", szamolo.getMinosites());
            
            testSzemely.setIsFerfi(false);
            karFajta = 2;
            torzsFajta = 2;
            keringesFajta = 2;
            karEredmeny = 25;
            torzsEredmeny = 31;
            keringesEredmeny = 1535;
            pontszam = szamolo.getEredmeny(testSzemely.isFerfi(), testSzemely.getKor(), karFajta, karEredmeny, torzsFajta, torzsEredmeny, keringesFajta, keringesEredmeny);
            assertEquals(257, pontszam);
            assertEquals("MF", szamolo.getMinosites());
            
            keringesFajta = 3;
            keringesEredmeny = 111;
            pontszam = szamolo.getEredmeny(testSzemely.isFerfi(), testSzemely.getKor(), karFajta, karEredmeny, torzsFajta, torzsEredmeny, keringesFajta, keringesEredmeny);
            assertEquals(152, pontszam);
            assertEquals("NMF", szamolo.getMinosites());
        } catch (FafDAOException ex) {
            ex.printStackTrace();
        }
    }
}
