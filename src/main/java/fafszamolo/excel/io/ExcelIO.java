package fafszamolo.excel.io;

import fafszamolo.exception.FafDAOException;
import fafszamolo.model.FizikaiAllapotFelmeres;
import fafszamolo.model.Szemely;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author
 */
public class ExcelIO {
    
    private static final int MINPONTHATAR = 220;
    private Szemely szemely;
    private int felmeresIndex;
    private File newFile;
    private Workbook workbook;
    private Sheet sheet;
    private CellStyle cellStyle;
    private XSSFFont font;
    
    public ExcelIO(Szemely sz, int index, File chosenDir) throws FafDAOException {
        try {
            szemely = sz;
            felmeresIndex = index;
            File templateFile = new File("src/main/java/fafSzamolo/excel/template.xlsx");
            newFile = new File(chosenDir.getAbsolutePath() + "/" + fileNameCreator());
            FileUtils.copyFile(templateFile, newFile);
            workbook = new XSSFWorkbook(new FileInputStream(newFile));
            sheet = workbook.getSheetAt(0);
            font = ((XSSFWorkbook) workbook).createFont();
            cellStyle = workbook.createCellStyle();
            font.setFontName("Times New Roman");
            font.setFontHeightInPoints((short) 10);
            font.setBold(true);
            cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            cellStyle.setFont(font);
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
    
    public String fileNameCreator() {
        return "igazolás_" + szemely.getFelmeresek().get(felmeresIndex).getVegrehajtasIdeje().toString().replace('-', '_')
                + "_" + szemely.getNev().replaceAll(" ", "_") + ".xlsx";
    }
    
    public void fileWriter() throws FafDAOException {
        try {
            cellWriter(getCell(6, 0), szemely.getNev());
            cellWriter(getCell(8, 0), szemely.getRf().getTeljesNev());
            cellWriter(getCell(6, 8), szemely.getSzulDatum().format(DateTimeFormatter.ofPattern("yyyy.MM.dd.")));
            if (szemely.getAlakulat().equals("Bocskai")) {
                cellWriter(getCell(8, 8), "MH 5. Bocskai István Lövészdandár");
            }
            cellWriter(getCell(6, 14), String.valueOf(szemely.getKor()));
            cellWriter(getCell(6, 17), szemely.getAnyjaNeve());
            if (szemely.isFerfi()) {
                cellWriter(getCell(8, 21), "X", getCell(8, 21).getCellStyle());
            } else {
                cellWriter(getCell(8, 24), "X", getCell(8, 24).getCellStyle());
            }
            if (szemely.getAlakulat().equals("Bocskai")) {
                cellWriter(getCell(10, 8), "MH 5. LDD");
            }
            cellWriter(getCell(14, 0), String.valueOf(szemely.getMagassag()), getCell(14, 0).getCellStyle());
            cellWriter(getCell(14, 5), String.valueOf(szemely.getSuly()), getCell(14, 5).getCellStyle());
            cellWriter(getCell(14, 10), String.valueOf(szemely.getTestZsir()), getCell(14, 10).getCellStyle());
            
            FizikaiAllapotFelmeres felmeres = szemely.getFelmeresek().get(felmeresIndex);
            if (felmeres.getBmi().equals("MF")) {
                cellWriter(getCell(14, 17), "X", getCell(14, 17).getCellStyle());
            } else {
                cellWriter(getCell(14, 22), "X", getCell(14, 22).getCellStyle());
            }
            if (felmeres.getKarMozgasforma() == 0) {
                cellWriter(getCell(16, 9), "X", getCell(16, 9).getCellStyle());
                cellWriter(getCell(19, 5), String.valueOf(felmeres.getKarEredmeny()), getCell(19, 5).getCellStyle());
            } else if (felmeres.getKarMozgasforma() == 1) {
                cellWriter(getCell(18, 9), "X", getCell(18, 9).getCellStyle());
                cellWriter(getCell(19, 5), String.valueOf(felmeres.getKarEredmeny()), getCell(19, 5).getCellStyle());
            } else {
                cellWriter(getCell(16, 14), "X", getCell(16, 14).getCellStyle());
                cellWriter(getCell(19, 10), String.valueOf(felmeres.getKarEredmeny()), getCell(19, 10).getCellStyle());
            }
            if (felmeres.getTorzsMozgasforma() == 0) {
                cellWriter(getCell(17, 19), "X", getCell(17, 19).getCellStyle());
                cellWriter(getCell(19, 15), String.valueOf(felmeres.getTorzsEredmeny()), getCell(19, 15).getCellStyle());
            } else if (felmeres.getTorzsMozgasforma() == 1) {
                cellWriter(getCell(16, 24), "X", getCell(16, 24).getCellStyle());
                cellWriter(getCell(19, 20), String.valueOf(felmeres.getTorzsEredmeny()), getCell(19, 20).getCellStyle());
            } else {
                cellWriter(getCell(17, 24), "X", getCell(17, 24).getCellStyle());
                cellWriter(getCell(19, 20), String.valueOf(felmeres.getTorzsEredmeny()), getCell(19, 20).getCellStyle());
            }
            if (felmeres.getKeringesMozgasforma() == 0) {
                cellWriter(getCell(24, 7), String.valueOf(felmeres.getKeringesEredmeny()), getCell(24, 7).getCellStyle());
            } else if (felmeres.getKeringesMozgasforma() == 1) {
                cellWriter(getCell(24, 13), String.valueOf(felmeres.getKeringesEredmeny()), getCell(24, 13).getCellStyle());
            } else if (felmeres.getKeringesMozgasforma() == 2) {
                cellWriter(getCell(24, 19), String.valueOf(felmeres.getKeringesEredmeny()), getCell(24, 19).getCellStyle());
            }
            cellWriter(getCell(31, 10), String.valueOf(felmeres.getPontszam()), getCell(31, 10).getCellStyle());
            cellWriter(getCell(42, 7), "Hódmezővásárhely, " + felmeres.getVegrehajtasIdeje().format(DateTimeFormatter.ofPattern("yyyy.MM.dd.")));
            if (felmeres.getPontszam() >= MINPONTHATAR) {
                cellWriter(getCell(33, 11), "X", getCell(14, 17).getCellStyle());
            } else {
                cellWriter(getCell(36, 11), "X", getCell(14, 22).getCellStyle());
            }
            FileOutputStream ous = new FileOutputStream(newFile);
            workbook.write(ous);
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
    
    private void cellWriter(Cell cell, String s) {
        cell.setCellValue(s);
        cell.setCellStyle(cellStyle);
    }
    
    private void cellWriter(Cell cell, String s, CellStyle cellStyle) {
        cell.setCellValue(s);
        cell.setCellStyle(cellStyle);
    }
    
    private Cell getCell(int row, int coloumn) {
        return sheet.getRow(row).getCell(coloumn);
    }
    
    public void close() throws FafDAOException {
        try {
            workbook.close();
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
}
