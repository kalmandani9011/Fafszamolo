package fafszamolo.gui;

import fafszamolo.model.FizikaiAllapotFelmeres;
import fafszamolo.model.Szemely;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Optional;
import javax.swing.AbstractAction;
import static javax.swing.JComponent.WHEN_IN_FOCUSED_WINDOW;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

/**
 * Új fizikai állapot felméréshez vagy meglévő módosításához használható dialógus ablak.
 */
public class FafDialog extends javax.swing.JDialog {

    private static int FUTASKORHATAR = 45;
    private static int ERGOMETRIAKORHATAR = 54;
    private static final int ELSOCOMBOBOXELEM = 0;
    private static final int MASODIKCOMBOBOXELEM = FUTAS2000COMBOBOXELEM = 1;
    private static final int HARMADIKCOMBOBOXELEM = 2;
    private static int FUTAS2000COMBOBOXELEM;
    private static int ERGOMETRIACOMBOBOXELEM = 3;
    private static int MAXPERC = 59;
    private Szemely szemely;
    private Optional<FizikaiAllapotFelmeres> optFaf;

    public FafDialog(java.awt.Frame parent, Szemely szemely) {
        super(parent);
        initComponents();
        setSize(570, 480);
        setModalityType(DEFAULT_MODALITY_TYPE);
        setTitle("Új fizikai eredmény hozzáadása");
        setLocationRelativeTo(null);
        this.szemely = szemely;
        optFaf = Optional.empty();
        tfSztsz.setText(szemely.getSztsz() + "");
        tfSztsz.setEnabled(false);
        tfKar.setForeground(Color.LIGHT_GRAY);
        tfTorzs.setForeground(Color.LIGHT_GRAY);
        tfKeringes.setForeground(Color.LIGHT_GRAY);
    }
    
    public FafDialog(java.awt.Frame parent, Szemely szemely, Optional<FizikaiAllapotFelmeres> optFaf) {
        super(parent);
        initComponents();
        this.setSize(570, 480);
        this.setModalityType(DEFAULT_MODALITY_TYPE);
        setTitle("Fizikai eredmény módosítása");
        this.setLocationRelativeTo(null);
        this.szemely = szemely;
        if (optFaf.isPresent()) {
            this.optFaf = optFaf;
            tfSztsz.setText(szemely.getSztsz() + "");
            tfSztsz.setEnabled(false);
            tfVegrehajtasIdeje.setText(optFaf.get().getVegrehajtasIdeje().toString());
            cbKar.setSelectedIndex(optFaf.get().getKarMozgasforma());
            tfKar.setText(optFaf.get().getKarEredmeny() + "");
            cbTorzs.setSelectedIndex(optFaf.get().getTorzsMozgasforma());
            tfTorzs.setText(optFaf.get().getTorzsEredmeny() + "");
            cbKeringes.setSelectedIndex(optFaf.get().getKeringesMozgasforma());
            tfKeringes.setText(optFaf.get().getKeringesEredmeny() + "");
        }
    }
    
    public Optional<FizikaiAllapotFelmeres> showDialog() {
        setVisible(true);
        return optFaf;
    }

    private boolean ellenorzes() {
        try {
            LocalDate.parse(tfVegrehajtasIdeje.getText(), DateTimeFormatter.ofPattern("[yyyy.MM.dd.][yyyy.MM.dd][yyyy-MM-dd][yyyyMMdd]"));
        } catch (DateTimeParseException ex) {
            popUpAblak("Hibás végrehajtási idő!");
            return false;
        }
        if (tfKar.getText().isEmpty() || !tfKar.getText().matches("[0-9]+") || Integer.parseInt(tfKar.getText()) < 0 || Integer.parseInt(tfKar.getText()) > 150) {
            popUpAblak("Hiba a Kar-vállöv erő-állóképesség bevitelnél!");
            return false;
        } else if (tfTorzs.getText().isEmpty() || !tfTorzs.getText().matches("[0-9]+") || Integer.parseInt(tfTorzs.getText()) < 0 || Integer.parseInt(tfTorzs.getText()) > 150) {
            popUpAblak("Hiba a Törzs erő-állóképesség bevitelnél!");
            return false;
        } else if (tfKeringes.getText().isEmpty() || !tfKeringes.getText().matches("[0-9]+")) {
            popUpAblak("Hiba a Keringésrendszer állóképesség bevitelnél!");
            return false;
        } else if (cbKeringes.getSelectedIndex() == ERGOMETRIACOMBOBOXELEM && Integer.parseInt(tfKeringes.getText()) > 600
                || cbKeringes.getSelectedIndex() == ERGOMETRIACOMBOBOXELEM && Integer.parseInt(tfKeringes.getText()) < 40) {
            popUpAblak("Nem érvényes watt/kg formátum!");
            return false;
        } else if (cbKeringes.getSelectedIndex() != ERGOMETRIACOMBOBOXELEM && Integer.parseInt(tfKeringes.getText()) > 1000 && Integer.parseInt(tfKeringes.getText().substring(2)) > MAXPERC) {
            popUpAblak("Nem érvényes idő formátum!");
            return false;
        } else if (cbKeringes.getSelectedIndex() != ERGOMETRIACOMBOBOXELEM && Integer.parseInt(tfKeringes.getText()) < 1000 && Integer.parseInt(tfKeringes.getText().substring(1)) > MAXPERC) {
            popUpAblak("Nem érvényes idő formátum!");
            return false;
        } else if (cbKeringes.getSelectedIndex() != ERGOMETRIACOMBOBOXELEM && Integer.parseInt(tfKeringes.getText()) < 400
                || cbKeringes.getSelectedIndex() != ERGOMETRIACOMBOBOXELEM && Integer.parseInt(tfKeringes.getText()) > 2800) {
            popUpAblak("Nem érvényes idő formátum!");
            return false;
        } else if (cbKeringes.getSelectedIndex() == FUTAS2000COMBOBOXELEM && szemely.getKor() < FUTASKORHATAR) {
            popUpAblak("45 év alatt nem lehet 2000 m-t futni!");
            return false;
        } else if (cbKeringes.getSelectedIndex() == ERGOMETRIACOMBOBOXELEM && szemely.getKor() > ERGOMETRIAKORHATAR) {
            popUpAblak("54 év felett nem lehet ergometriát alkalmazni!");
            return false;
        } else {
            return true;
        }
    }

    private int popUpAblak(String hibaUzenet) {
        JPanel panel = new JPanel();
        panel.add(new JLabel(hibaUzenet));
        return JOptionPane.showConfirmDialog(null, panel, "Hiba!", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
    }

    private void hozzaad() {
        LocalDate vegrehajtasIdeje = LocalDate.parse(tfVegrehajtasIdeje.getText(), DateTimeFormatter.ofPattern("[yyyy.MM.dd.][yyyy.MM.dd][yyyy-MM-dd][yyyyMMdd]"));
        int karMozgasforma = cbKar.getSelectedIndex();
        int karEredmeny = Integer.parseInt(tfKar.getText());
        int torzsMozgasforma = cbTorzs.getSelectedIndex();
        int torzsEredmeny = Integer.parseInt(tfTorzs.getText());
        int keringesMozgasforma = cbKeringes.getSelectedIndex();
        int keringesEredmeny = Integer.parseInt(tfKeringes.getText());
        if (optFaf.isPresent()) {
            optFaf = Optional.of(new FizikaiAllapotFelmeres(optFaf.get().getSorszam(), szemely.getSztsz(), vegrehajtasIdeje, karMozgasforma, karEredmeny, 
            torzsMozgasforma, torzsEredmeny, keringesMozgasforma, keringesEredmeny));
        } else {
            optFaf = Optional.of(new FizikaiAllapotFelmeres(szemely.getSztsz(), vegrehajtasIdeje, karMozgasforma, karEredmeny, 
            torzsMozgasforma, torzsEredmeny, keringesMozgasforma, keringesEredmeny));
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        tfSztsz = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        tfVegrehajtasIdeje = new javax.swing.JTextField();
        lblKar = new javax.swing.JLabel();
        cbKar = new javax.swing.JComboBox<>();
        tfKar = new javax.swing.JTextField();
        lblTorzs = new javax.swing.JLabel();
        cbTorzs = new javax.swing.JComboBox<>();
        tfTorzs = new javax.swing.JTextField();
        lblKeringes = new javax.swing.JLabel();
        cbKeringes = new javax.swing.JComboBox<>();
        tfKeringes = new javax.swing.JTextField();
        jbOk = new javax.swing.JButton();
        jbMegsem = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel2.setText("SZTSZ:");

        tfSztsz.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfSztszFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfSztszFocusLost(evt);
            }
        });

        jLabel3.setText("Végrehajtás ideje:");

        tfVegrehajtasIdeje.setToolTipText("Elválasztó lehet: pont, kötőjel vagy semmi.");
        tfVegrehajtasIdeje.setText("1900.01.01.");
        tfVegrehajtasIdeje.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfVegrehajtasIdejeFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfVegrehajtasIdejeFocusLost(evt);
            }
        });

        lblKar.setText("Kar-vállöv erő-állóképesség");

        cbKar.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Fekvőtámasz", "Húzódzkodás", "Hajlított karú függés" }));
        cbKar.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbKarItemStateChanged(evt);
            }
        });
        cbKar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbKarActionPerformed(evt);
            }
        });

        tfKar.setText("0..75");
        tfKar.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfKarFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfKarFocusLost(evt);
            }
        });
        tfKar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfKarActionPerformed(evt);
            }
        });

        lblTorzs.setText("Törzs erő-állóképesség");

        cbTorzs.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Felülés", "Lapocka emelés", "Függő térdemelés" }));
        cbTorzs.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbTorzsItemStateChanged(evt);
            }
        });

        tfTorzs.setText("0..90");
        tfTorzs.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfTorzsFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfTorzsFocusLost(evt);
            }
        });

        lblKeringes.setText("Keringésrendszer állóképesség");

        cbKeringes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "3200m futás", "2000m futás", "1600m menet", "Ergometria" }));
        cbKeringes.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbKeringesItemStateChanged(evt);
            }
        });

        tfKeringes.setToolTipText("Az időt elválasztás nélkül kell beírni! Pl.: 1234");
        tfKeringes.setText("400..2800");
        tfKeringes.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfKeringesFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfKeringesFocusLost(evt);
            }
        });

        jbOk.setText("OK");
        jbOk.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ENTER"), "OK");
        jbOk.getActionMap().put("OK", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jbOkActionPerformed(e);
            }
        });
        jbOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbOkActionPerformed(evt);
            }
        });

        jbMegsem.setText("Mégsem");
        jbMegsem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbMegsemActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(lblKeringes)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cbKeringes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(51, 51, 51)
                                .addComponent(tfKeringes, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel2))
                                .addGap(53, 53, 53)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(tfVegrehajtasIdeje, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(tfSztsz, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(lblTorzs)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(lblKar)
                                        .addGap(141, 141, 141))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(cbKar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(tfKar))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(cbTorzs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(tfTorzs, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(62, 62, 62)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jbMegsem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jbOk, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(tfSztsz, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfVegrehajtasIdeje, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addComponent(lblKar)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbKar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfKar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbOk))
                .addGap(18, 18, 18)
                .addComponent(lblTorzs)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbTorzs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfTorzs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbMegsem))
                .addGap(18, 18, 18)
                .addComponent(lblKeringes)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbKeringes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfKeringes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(65, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbKarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbKarActionPerformed
    }//GEN-LAST:event_cbKarActionPerformed

    private void tfKarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfKarActionPerformed
    }//GEN-LAST:event_tfKarActionPerformed

    private void jbOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbOkActionPerformed
        if (ellenorzes()) {
            hozzaad();
            setVisible(false);
            dispose();
        } else {
            optFaf = Optional.empty();
        }
    }//GEN-LAST:event_jbOkActionPerformed

    private void jbMegsemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbMegsemActionPerformed
        optFaf = Optional.empty();
        setVisible(false);
        dispose();
    }//GEN-LAST:event_jbMegsemActionPerformed

    private void tfSztszFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfSztszFocusGained
    }//GEN-LAST:event_tfSztszFocusGained

    private void tfSztszFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfSztszFocusLost
    }//GEN-LAST:event_tfSztszFocusLost

    private void tfVegrehajtasIdejeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfVegrehajtasIdejeFocusGained
        tfVegrehajtasIdeje.setForeground(Color.BLACK);
        if (tfVegrehajtasIdeje.getText().trim().equals("1900.01.01.")) {
            tfVegrehajtasIdeje.setText("");
        }
    }//GEN-LAST:event_tfVegrehajtasIdejeFocusGained

    private void tfVegrehajtasIdejeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfVegrehajtasIdejeFocusLost
        if (tfVegrehajtasIdeje.getText().trim().isEmpty()) {
            tfVegrehajtasIdeje.setForeground(Color.LIGHT_GRAY);
            tfVegrehajtasIdeje.setText("1900.01.01.");
        }
    }//GEN-LAST:event_tfVegrehajtasIdejeFocusLost

    private void cbKarItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbKarItemStateChanged
        if (tfKar.getText().trim().equals("0..75") || tfKar.getText().trim().equals("0..20") || tfKar.getText().trim().equals("0..70")) {
            tfKar.setForeground(Color.LIGHT_GRAY);
            if (cbKar.getSelectedIndex() == ELSOCOMBOBOXELEM) {
                tfKar.setText("0..75");
            } else if (cbKar.getSelectedIndex() == MASODIKCOMBOBOXELEM) {
                tfKar.setText("0..20");
            } else if (cbKar.getSelectedIndex() == HARMADIKCOMBOBOXELEM) {
                tfKar.setText("0..70");
            }
        }
    }//GEN-LAST:event_cbKarItemStateChanged

    private void tfKarFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfKarFocusGained
        tfKar.setForeground(Color.BLACK);
        if (tfKar.getText().trim().equals("0..75") || tfKar.getText().trim().equals("0..20") || tfKar.getText().trim().equals("0..70")) {
            tfKar.setText("");
        }
    }//GEN-LAST:event_tfKarFocusGained

    private void tfKarFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfKarFocusLost
        if (tfKar.getText().trim().isEmpty()) {
            tfKar.setForeground(Color.LIGHT_GRAY);
            if (cbKar.getSelectedIndex() == ELSOCOMBOBOXELEM) {
                tfKar.setText("0..75");
            } else if (cbKar.getSelectedIndex() == MASODIKCOMBOBOXELEM) {
                tfKar.setText("0..20");
            } else if (cbKar.getSelectedIndex() == HARMADIKCOMBOBOXELEM) {
                tfKar.setText("0..70");
            }
        }
    }//GEN-LAST:event_tfKarFocusLost

    private void cbTorzsItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbTorzsItemStateChanged
        if (tfTorzs.getText().trim().equals("0..90") || tfTorzs.getText().trim().equals("0..72") || tfTorzs.getText().trim().equals("0..51")) {
            tfTorzs.setForeground(Color.LIGHT_GRAY);
            if (cbTorzs.getSelectedIndex() == ELSOCOMBOBOXELEM) {
                tfTorzs.setText("0..90");
            } else if (cbTorzs.getSelectedIndex() == MASODIKCOMBOBOXELEM) {
                tfTorzs.setText("0..72");
            } else if (cbTorzs.getSelectedIndex() == HARMADIKCOMBOBOXELEM) {
                tfTorzs.setText("0..51");
            }
        }
    }//GEN-LAST:event_cbTorzsItemStateChanged

    private void tfTorzsFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfTorzsFocusGained
        tfTorzs.setForeground(Color.BLACK);
        if (tfTorzs.getText().trim().equals("0..90") || tfTorzs.getText().trim().equals("0..72") || tfTorzs.getText().trim().equals("0..51")) {
            tfTorzs.setText("");
        }
    }//GEN-LAST:event_tfTorzsFocusGained

    private void tfTorzsFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfTorzsFocusLost
        if (tfTorzs.getText().trim().isEmpty()) {
            tfTorzs.setForeground(Color.LIGHT_GRAY);
            if (cbTorzs.getSelectedIndex() == ELSOCOMBOBOXELEM) {
                tfTorzs.setText("0..90");
            } else if (cbTorzs.getSelectedIndex() == MASODIKCOMBOBOXELEM) {
                tfTorzs.setText("0..72");
            } else if (cbTorzs.getSelectedIndex() == HARMADIKCOMBOBOXELEM) {
                tfTorzs.setText("0..51");
            }
        }
    }//GEN-LAST:event_tfTorzsFocusLost

    private void cbKeringesItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbKeringesItemStateChanged
    }//GEN-LAST:event_cbKeringesItemStateChanged

    private void tfKeringesFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfKeringesFocusGained
        tfKeringes.setForeground(Color.BLACK);
        if (tfKeringes.getText().trim().equals("400..2800")) {
            tfKeringes.setText("");
        }
    }//GEN-LAST:event_tfKeringesFocusGained

    private void tfKeringesFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfKeringesFocusLost
        if (tfKeringes.getText().trim().isEmpty()) {
            tfKeringes.setForeground(Color.LIGHT_GRAY);
            tfKeringes.setText("400..2800");
        }
    }//GEN-LAST:event_tfKeringesFocusLost

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cbKar;
    private javax.swing.JComboBox<String> cbKeringes;
    private javax.swing.JComboBox<String> cbTorzs;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JButton jbMegsem;
    private javax.swing.JButton jbOk;
    private javax.swing.JLabel lblKar;
    private javax.swing.JLabel lblKeringes;
    private javax.swing.JLabel lblTorzs;
    private javax.swing.JTextField tfKar;
    private javax.swing.JTextField tfKeringes;
    private javax.swing.JTextField tfSztsz;
    private javax.swing.JTextField tfTorzs;
    private javax.swing.JTextField tfVegrehajtasIdeje;
    // End of variables declaration//GEN-END:variables
}
