package fafszamolo.gui;

import fafszamolo.model.Rendfokozat;
import fafszamolo.model.Szemely;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.AbstractAction;
import static javax.swing.JComponent.WHEN_IN_FOCUSED_WINDOW;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

/**
 * Új személy felvileéhez, vagy meglévő módosításához használható dialógus ablak.
 */
public class SzemelyDialog extends javax.swing.JDialog {

    private static int MINMAGASSAG = 130;
    private static int MAXMAGASSAG = 230;
    private static int MINSULY = 40;
    private static int MAXSULY = 170;
    private List<Integer> sztszSzamok;
    private Optional<Szemely> optSzemely;
    private int rendfokozatIndex;
    private int alakulatIndex;
    private int nemIndex;

    public SzemelyDialog(java.awt.Frame parent) {
        super(parent);
        initComponents();
        setSize(570, 480);
        setModalityType(DEFAULT_MODALITY_TYPE);
        setTitle("Új személy");
        optSzemely = Optional.empty();
        setLocationRelativeTo(null);
        tfNev.setForeground(Color.LIGHT_GRAY);
        tfSztsz.setForeground(Color.LIGHT_GRAY);
        tfSzulDatum.setForeground(Color.LIGHT_GRAY);
        tfAnyjaNeve.setForeground(Color.LIGHT_GRAY);
        tfMagassag.setForeground(Color.LIGHT_GRAY);
        tfSuly.setForeground(Color.LIGHT_GRAY);
        tfTestZsir.setForeground(Color.LIGHT_GRAY);
    }
    
    public SzemelyDialog(java.awt.Frame parent, Optional<Szemely> optSzemely) {
        super(parent);
        initComponents();
        setSize(570, 480);
        setModalityType(DEFAULT_MODALITY_TYPE);
        setLocationRelativeTo(null);
        if (optSzemely.isPresent()) {
            setTitle("Személy módosítása");
            this.optSzemely = optSzemely;
            tfNev.setText(optSzemely.get().getNev());
            tfSztsz.setText(Integer.toString(optSzemely.get().getSztsz()));
            tfSzulDatum.setText(optSzemely.get().getSzulDatum().toString());
            tfAnyjaNeve.setText(optSzemely.get().getAnyjaNeve());
            tfMagassag.setText(Integer.toString(optSzemely.get().getMagassag()));
            tfSuly.setText(Integer.toString(optSzemely.get().getSuly()));
            tfTestZsir.setText(Double.toString(optSzemely.get().getTestZsir()));
            cbRendfokozat.setSelectedItem(optSzemely.get().getRf());
            cbNem.setSelectedIndex(optSzemely.get().isFerfi() ? 0 : 1);
            cbAlakulat.setSelectedItem(optSzemely.get().getAlakulat());
        }
    }
    
    public Optional<Szemely> showDialog() {
        setVisible(true);
        return optSzemely;
    }
    
    public void setSztszSzamok(List<Integer> szamok) {
        this.sztszSzamok = szamok;
    }
    
    private boolean ellenorzes() {
        String regex = "[\\p{javaUpperCase}][\\w.]*[ -][\\p{javaUpperCase}][\\w.]*( [\\p{javaUpperCase}][\\w]*)?( [\\p{javaUpperCase}][\\w]*)?";
        Pattern p = Pattern.compile(regex, Pattern.UNICODE_CHARACTER_CLASS);
        Matcher m = p.matcher(tfNev.getText());
        if (tfNev.getText().trim().isEmpty() || !m.matches()) {
            popUpAblak("Hiba a név bevitelnél!");
            return false;
        } else if (tfSztsz.getText().isEmpty() || !tfSztsz.getText().matches("\\b([0-9]{8})\\b")) {
            popUpAblak("Hiba az SZTSZ bevitelnél!");
            return false;
        }
        for (int i : sztszSzamok) {
            if (!optSzemely.isPresent()) {
                if (i == Integer.parseInt(tfSztsz.getText())) {
                    popUpAblak("Már létezik személy ezzel az SZTSZ számmal!");
                    return false;
                }
            } else {
                if (optSzemely.get().getSztsz() != Integer.parseInt(tfSztsz.getText()) && i == Integer.parseInt(tfSztsz.getText())) {
                    popUpAblak("Már létezik személy ezzel az SZTSZ számmal!");
                    return false;
                }
            }
        }
        try {
            LocalDate.parse(tfSzulDatum.getText(), DateTimeFormatter.ofPattern("[yyyy.MM.dd.][yyyy.MM.dd][yyyy-MM-dd][yyyyMMdd]"));
        } catch (DateTimeParseException ex) {
            popUpAblak("Hibás születési dátum!");
            return false;
        }
        m = p.matcher(tfAnyjaNeve.getText().trim());
        if (tfAnyjaNeve.getText().trim().isEmpty() || !m.matches()) {
            popUpAblak("Hiba az anyja neve bevitelnél!");
            return false;
        } else if (tfMagassag.getText().isEmpty() || !tfMagassag.getText().matches("[0-9]+") || Integer.parseInt(tfMagassag.getText()) < MINMAGASSAG
                || Integer.parseInt(tfMagassag.getText()) > MAXMAGASSAG) {
            popUpAblak("Hiba a magasság bevitelnél!");
            return false;
        } else if (tfSuly.getText().isEmpty() || !tfSuly.getText().matches("[0-9]+") || Integer.parseInt(tfSuly.getText()) < MINSULY
                || Integer.parseInt(tfSuly.getText()) > MAXSULY) {
            popUpAblak("Hiba a súly bevitelnél!");
            return false;
        } else if (tfTestZsir.getText().isEmpty() || !tfTestZsir.getText().matches("[0-9]+[.,]?[0-9]?")) {
            popUpAblak("Pontot vagy vesszőt használj tizedesjelnek!");
            return false;
        } else {
            return true;
        }
    }

    private int popUpAblak(String hibaUzenet) {
        JPanel panel = new JPanel();
        panel.add(new JLabel(hibaUzenet));
        return JOptionPane.showConfirmDialog(null, panel, "Hiba!", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
    }
    
    private void hozzaad() {
        String nev = tfNev.getText().trim();
        int sztsz = Integer.parseInt(tfSztsz.getText());
        Rendfokozat rf = cbRendfokozat.getItemAt(rendfokozatIndex);
        String alakulat = cbAlakulat.getItemAt(alakulatIndex);
        LocalDate szulDatum = LocalDate.parse(tfSzulDatum.getText(), DateTimeFormatter.ofPattern("[yyyy.MM.dd.][yyyy.MM.dd][yyyy-MM-dd][yyyyMMdd]"));
        String anyjaNeve = tfAnyjaNeve.getText().trim();
        boolean ferfi = nemIndex == 0;
        int magassag = Integer.parseInt(tfMagassag.getText());
        int suly = Integer.parseInt(tfSuly.getText());
        double testZsir = Double.parseDouble(tfTestZsir.getText().replaceAll(",", "."));
        if (optSzemely.isPresent()) {
            optSzemely = Optional.of(new Szemely(optSzemely.get().getSorszam(), sztsz, nev, rf, alakulat, szulDatum, anyjaNeve, ferfi, magassag, suly, testZsir));
        } else {
            optSzemely = Optional.of(new Szemely(sztsz, nev, rf, alakulat, szulDatum, anyjaNeve, ferfi, magassag, suly, testZsir));
        }
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jbOk = new javax.swing.JButton();
        jbMegsem = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        tfNev = new javax.swing.JTextField();
        tfSzulDatum = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        tfSztsz = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        tfAnyjaNeve = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        tfMagassag = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        tfSuly = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        tfTestZsir = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        cbRendfokozat = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        cbAlakulat = new javax.swing.JComboBox<>();
        cbNem = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jbOk.setText("OK");
        jbOk.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ENTER"), "OK");
        jbOk.getActionMap().put("OK", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jbOkActionPerformed(e);
            }
        });
        jbOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbOkActionPerformed(evt);
            }
        });

        jbMegsem.setText("Mégsem");
        jbMegsem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbMegsemActionPerformed(evt);
            }
        });

        jLabel1.setText("Név:");

        jLabel2.setText("SZTSZ:");

        tfNev.setToolTipText("Max 4 tagú név lehetséges, az első 2 tag között kötőjel is engedélyezett.");
        tfNev.setText("pl.: Dr. Kiss Nagy Aladdin");
        tfNev.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfNevFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfNevFocusLost(evt);
            }
        });

        tfSzulDatum.setToolTipText("Elválasztó lehet: pont, kötőjel vagy semmi.");
        tfSzulDatum.setText("1900.01.01.");
        tfSzulDatum.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfSzulDatumFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfSzulDatumFocusLost(evt);
            }
        });

        jLabel3.setText("Születési dátum:");

        tfSztsz.setToolTipText("8 számjegyből kell állnia!");
        tfSztsz.setText("12345678");
        tfSztsz.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfSztszFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfSztszFocusLost(evt);
            }
        });

        jLabel4.setText("Anyja neve:");

        tfAnyjaNeve.setToolTipText("Max 4 tagú név lehetséges, az első 2 tag között kötőjel is engedélyezett.");
        tfAnyjaNeve.setText("pl.: Dr. Kiss Nagy Aladdin");
        tfAnyjaNeve.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfAnyjaNeveFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfAnyjaNeveFocusLost(evt);
            }
        });

        jLabel5.setText("Magasság:");

        tfMagassag.setToolTipText("Nem kell mértékegység!");
        tfMagassag.setText("pl.: 175");
        tfMagassag.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfMagassagFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfMagassagFocusLost(evt);
            }
        });

        jLabel6.setText("Súly:");

        tfSuly.setToolTipText("Nem kell mértékegység!");
        tfSuly.setText("pl.: 75");
        tfSuly.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfSulyFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfSulyFocusLost(evt);
            }
        });

        jLabel7.setText("Testzsír százalék:");

        tfTestZsir.setToolTipText("Nem kell százalékjel! Tizedesjel pont vagy vessző legyen! Egy tizedesjegyet írj!");
        tfTestZsir.setText("pl.: 10,5");
        tfTestZsir.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfTestZsirFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfTestZsirFocusLost(evt);
            }
        });

        jLabel8.setText("Rendfokozat:");

        cbRendfokozat.setModel(new javax.swing.DefaultComboBoxModel<>(Rendfokozat.values()));
        cbRendfokozat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbRendfokozatActionPerformed(evt);
            }
        });

        jLabel9.setText("Alakulat:");

        cbAlakulat.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Bocskai", "Klapka" }));
        cbAlakulat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbAlakulatActionPerformed(evt);
            }
        });

        cbNem.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Férfi", "Nő" }));
        cbNem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbNemActionPerformed(evt);
            }
        });

        jLabel10.setText("Neme:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6))
                        .addGap(23, 23, 23)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(tfAnyjaNeve, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(tfNev, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(tfSztsz, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(tfSzulDatum, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(tfMagassag, javax.swing.GroupLayout.DEFAULT_SIZE, 55, Short.MAX_VALUE)
                                            .addComponent(tfSuly))
                                        .addGap(75, 75, 75)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel8)
                                            .addComponent(jLabel9)
                                            .addComponent(jLabel10))
                                        .addGap(28, 28, 28)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(cbNem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(cbAlakulat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(cbRendfokozat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(40, 40, 40)
                                        .addComponent(jbOk, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(82, 82, 82)
                                        .addComponent(jbMegsem)))
                                .addContainerGap(84, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(tfTestZsir, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfNev, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(tfSztsz, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfSzulDatum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(tfAnyjaNeve, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(21, 21, 21)
                                .addComponent(jLabel5))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(tfMagassag, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tfSuly, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(tfTestZsir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(36, 36, 36))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(cbRendfokozat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbNem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(cbAlakulat))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbOk)
                    .addComponent(jbMegsem))
                .addContainerGap(41, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbNemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbNemActionPerformed
        nemIndex = cbNem.getSelectedIndex();
    }//GEN-LAST:event_cbNemActionPerformed

    private void jbMegsemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbMegsemActionPerformed
        optSzemely = Optional.empty();
        setVisible(false);
        dispose();
    }//GEN-LAST:event_jbMegsemActionPerformed

    private void jbOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbOkActionPerformed
        if (ellenorzes()) {
            hozzaad();
            setVisible(false);
            dispose();
        } else {
            optSzemely = Optional.empty();
        }
    }//GEN-LAST:event_jbOkActionPerformed

    private void cbRendfokozatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbRendfokozatActionPerformed
        rendfokozatIndex = cbRendfokozat.getSelectedIndex();
    }//GEN-LAST:event_cbRendfokozatActionPerformed

    private void cbAlakulatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbAlakulatActionPerformed
        alakulatIndex = cbAlakulat.getSelectedIndex();
    }//GEN-LAST:event_cbAlakulatActionPerformed

    private void tfNevFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfNevFocusGained
        tfNev.setForeground(Color.BLACK);
        if (tfNev.getText().trim().equals("pl.: Dr. Kiss Nagy Aladdin")) {
            tfNev.setText("");
        }
    }//GEN-LAST:event_tfNevFocusGained

    private void tfNevFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfNevFocusLost
        if (tfNev.getText().trim().isEmpty()) {
            tfNev.setForeground(Color.LIGHT_GRAY);
            tfNev.setText("pl.: Dr. Kiss Nagy Aladdin");
        }
    }//GEN-LAST:event_tfNevFocusLost

    private void tfSztszFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfSztszFocusGained
        tfSztsz.setForeground(Color.BLACK);
        if (tfSztsz.getText().trim().equals("12345678")) {
            tfSztsz.setText("");
        }
    }//GEN-LAST:event_tfSztszFocusGained

    private void tfSztszFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfSztszFocusLost
        if (tfSztsz.getText().trim().isEmpty()) {
            tfSztsz.setForeground(Color.LIGHT_GRAY);
            tfSztsz.setText("12345678");
        }
    }//GEN-LAST:event_tfSztszFocusLost

    private void tfSzulDatumFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfSzulDatumFocusGained
        tfSzulDatum.setForeground(Color.BLACK);
        if (tfSzulDatum.getText().trim().equals("1900.01.01.")) {
            tfSzulDatum.setText("");
        }
    }//GEN-LAST:event_tfSzulDatumFocusGained

    private void tfSzulDatumFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfSzulDatumFocusLost
        if (tfSzulDatum.getText().trim().isEmpty()) {
            tfSzulDatum.setForeground(Color.LIGHT_GRAY);
            tfSzulDatum.setText("1900.01.01.");
        }
    }//GEN-LAST:event_tfSzulDatumFocusLost

    private void tfAnyjaNeveFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfAnyjaNeveFocusGained
        tfAnyjaNeve.setForeground(Color.BLACK);
        if (tfAnyjaNeve.getText().trim().equals("pl.: Dr. Kiss Nagy Aladdin")) {
            tfAnyjaNeve.setText("");
        }
    }//GEN-LAST:event_tfAnyjaNeveFocusGained

    private void tfAnyjaNeveFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfAnyjaNeveFocusLost
        if (tfAnyjaNeve.getText().trim().isEmpty()) {
            tfAnyjaNeve.setForeground(Color.LIGHT_GRAY);
            tfAnyjaNeve.setText("pl.: Dr. Kiss Nagy Aladdin");
        }
    }//GEN-LAST:event_tfAnyjaNeveFocusLost

    private void tfMagassagFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfMagassagFocusGained
        tfMagassag.setForeground(Color.BLACK);
        if (tfMagassag.getText().trim().equals("pl.: 175")) {
            tfMagassag.setText("");
        }
    }//GEN-LAST:event_tfMagassagFocusGained

    private void tfMagassagFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfMagassagFocusLost
        if (tfMagassag.getText().trim().isEmpty()) {
            tfMagassag.setForeground(Color.LIGHT_GRAY);
            tfMagassag.setText("pl.: 175");
        }
    }//GEN-LAST:event_tfMagassagFocusLost

    private void tfSulyFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfSulyFocusGained
        tfSuly.setForeground(Color.BLACK);
        if (tfSuly.getText().trim().equals("pl.: 75")) {
            tfSuly.setText("");
        }
    }//GEN-LAST:event_tfSulyFocusGained

    private void tfSulyFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfSulyFocusLost
        if (tfSuly.getText().trim().isEmpty()) {
            tfSuly.setForeground(Color.LIGHT_GRAY);
            tfSuly.setText("pl.: 75");
        }
    }//GEN-LAST:event_tfSulyFocusLost

    private void tfTestZsirFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfTestZsirFocusGained
        tfTestZsir.setForeground(Color.BLACK);
        if (tfTestZsir.getText().trim().equals("pl.: 10,5")) {
            tfTestZsir.setText("");
        }
    }//GEN-LAST:event_tfTestZsirFocusGained

    private void tfTestZsirFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfTestZsirFocusLost
        if (tfTestZsir.getText().trim().isEmpty()) {
            tfTestZsir.setForeground(Color.LIGHT_GRAY);
            tfTestZsir.setText("pl.: 10,5");
        }
    }//GEN-LAST:event_tfTestZsirFocusLost

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cbAlakulat;
    private javax.swing.JComboBox<String> cbNem;
    private javax.swing.JComboBox<Rendfokozat> cbRendfokozat;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JButton jbMegsem;
    private javax.swing.JButton jbOk;
    private javax.swing.JTextField tfAnyjaNeve;
    private javax.swing.JTextField tfMagassag;
    private javax.swing.JTextField tfNev;
    private javax.swing.JTextField tfSuly;
    private javax.swing.JTextField tfSztsz;
    private javax.swing.JTextField tfSzulDatum;
    private javax.swing.JTextField tfTestZsir;
    // End of variables declaration//GEN-END:variables
}
