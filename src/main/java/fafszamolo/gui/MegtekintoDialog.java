package fafszamolo.gui;

import fafszamolo.model.FizikaiAllapotFelmeres;
import fafszamolo.model.Szemely;
import fafszamolo.exception.FafDAOException;
import fafszamolo.controller.GuiVezerlo;
import fafszamolo.excel.io.ExcelIO;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.AbstractAction;
import static javax.swing.JComponent.WHEN_IN_FOCUSED_WINDOW;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

/**
 * Egy kiválasztott személy összes fizikai állapot felmérését kilistázó dialógus
 * ablak.
 */
public class MegtekintoDialog extends javax.swing.JDialog {

    private static int ALAPERTELMEZETTNULLERTEK = -1;
    private DefaultTableModel adatbazis;
    private Szemely szemely;
    private GuiVezerlo vezerles;
    private JFileChooser chooser;

    public MegtekintoDialog(java.awt.Frame parent, Szemely szemely, GuiVezerlo vezerles) {
        super(parent);
        initComponents();
        setModalityType(DEFAULT_MODALITY_TYPE);
        setTitle(szemely.getNev() + " " + szemely.getRf() + " fizikai eredményei");
        setLocationRelativeTo(null);
        this.szemely = szemely;
        this.vezerles = vezerles;
        adatbazis = (DefaultTableModel) jtblMegtekinto.getModel();
        jtblMegtekinto.getTableHeader().setReorderingAllowed(false);
        betolt();
        chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    }
    
    private void fafIgazolasKeszites(File chosenDir) throws FafDAOException {
        if (jtblMegtekinto.getSelectedRow() == ALAPERTELMEZETTNULLERTEK) {
            JOptionPane.showMessageDialog(null, "Nincs kijelölve felmérés!", "Hiba!", JOptionPane.OK_OPTION);
        } else {
            ExcelIO io = new ExcelIO(szemely, jtblMegtekinto.getSelectedRow(), chosenDir);
            io.fileWriter();
            io.close();
        }
    }

    private void betolt() {
        for (FizikaiAllapotFelmeres f : szemely.getFelmeresek()) {
            Object[] ujSor = {szemely.getSorszam(), szemely.getSztsz(), szemely.getNev(), szemely.getRf(), szemely.getAlakulat(), szemely.getSzulDatum(),
                szemely.getAnyjaNeve(), szemely.isFerfi() ? "férfi" : "nő", szemely.getMagassag(), szemely.getSuly(), szemely.getTestZsir(),
                f.getVegrehajtasIdeje(), getKarMozgasforma(f.getKarMozgasforma()), f.getKarEredmeny(), getTorzsMozgasforma(f.getTorzsMozgasforma()),
                f.getTorzsEredmeny(), getKeringesMozgasforma(f.getKeringesMozgasforma()), f.getKeringesEredmeny(), f.getPontszam(), f.getBmi(), f.getTestZsirIndex()};
            adatbazis.addRow(ujSor);
        }
        oszlopEltuntet(0);
        oszlopEltuntet(4);
        oszlopEltuntet(6);
        oszlopEltuntet(7);
        oszlopEltuntet(8);
        oszlopEltuntet(9);
        oszlopEltuntet(10);
        oszlopEltuntet(19);
        oszlopEltuntet(20);
    }

    private void oszlopEltuntet(int oszlopSzam) {
        jtblMegtekinto.getColumnModel().getColumn(oszlopSzam).setMinWidth(0);
        jtblMegtekinto.getColumnModel().getColumn(oszlopSzam).setMaxWidth(0);
        jtblMegtekinto.getColumnModel().getColumn(oszlopSzam).setWidth(0);
    }

    private void oszlopElohoz(int oszlopSzam, int szelesseg) {
        jtblMegtekinto.getColumnModel().getColumn(oszlopSzam).setWidth(1000);
        jtblMegtekinto.getColumnModel().getColumn(oszlopSzam).setMaxWidth(1000);
        jtblMegtekinto.getColumnModel().getColumn(oszlopSzam).setMinWidth(10);
        jtblMegtekinto.getColumnModel().getColumn(oszlopSzam).setPreferredWidth(szelesseg);
    }

    private String getKarMozgasforma(int kod) {
        String s = "";
        switch (kod) {
            case 0:
                s = "fekvőtámasz";
                break;
            case 1:
                s = "húzódzkodás";
                break;
            case 2:
                s = "hajlított karú függés";
                break;
        }
        return s;
    }

    private String getTorzsMozgasforma(int kod) {
        String s = "";
        switch (kod) {
            case 0:
                s = "felülés";
                break;
            case 1:
                s = "lapocka emelés";
                break;
            case 2:
                s = "függő térdemelés";
                break;
        }
        return s;
    }

    private String getKeringesMozgasforma(int kod) {
        String s = "";
        switch (kod) {
            case 0:
                s = "3200m futás\n";
                break;
            case 1:
                s = "2000m futás";
                break;
            case 2:
                s = "1600m menet";
                break;
            case 3:
                s = "ergometria";
                break;
        }
        return s;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jtblMegtekinto = new javax.swing.JTable();
        jcbSorszam = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        jcbSztsz = new javax.swing.JCheckBox();
        jcbNev = new javax.swing.JCheckBox();
        jcbRendfokozat = new javax.swing.JCheckBox();
        jcbAlakulat = new javax.swing.JCheckBox();
        jcbSzulDatum = new javax.swing.JCheckBox();
        jcbNeme = new javax.swing.JCheckBox();
        jcbAnyjaNeve = new javax.swing.JCheckBox();
        jcbMagassag = new javax.swing.JCheckBox();
        jcbSuly = new javax.swing.JCheckBox();
        jcbTestZsir = new javax.swing.JCheckBox();
        jcbVegrehajtasIdeje = new javax.swing.JCheckBox();
        jcbKarEredmeny = new javax.swing.JCheckBox();
        jcbKarMozgas = new javax.swing.JCheckBox();
        jcbTorzsMozgas = new javax.swing.JCheckBox();
        jcbTorzsEredmeny = new javax.swing.JCheckBox();
        jcbKeringesMozgas = new javax.swing.JCheckBox();
        jcbKeringesEredmeny = new javax.swing.JCheckBox();
        jcbPontszam = new javax.swing.JCheckBox();
        jcbBmi = new javax.swing.JCheckBox();
        jcbTestZsirIndex = new javax.swing.JCheckBox();
        jbOk = new javax.swing.JButton();
        jbModositas = new javax.swing.JButton();
        jbTorles = new javax.swing.JButton();
        jbHozzaadas = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jbIgazolas = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jtblMegtekinto.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Sorszám", "SZTSZ", "Név", "Rendfokozat", "Alakulat", "Születési dátum", "Anyja neve", "Neme", "Magasság", "Súly", "Testzsír", "Végrehajtás Ideje", "Kar mozgásforma", "Kar eredmény", "Törzs mozgásforma", "Törzs eredmény", "Keringés mozgásforma", "Keringés eredmény", "Pontszám", "BMI", "Testzsír index"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Double.class, java.lang.Object.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtblMegtekinto.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jScrollPane1.setViewportView(jtblMegtekinto);

        jcbSorszam.setText("Sorszám");
        jcbSorszam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbSorszamActionPerformed(evt);
            }
        });

        jLabel1.setText("Oszlopok megjelenítése:");

        jcbSztsz.setSelected(true);
        jcbSztsz.setText("SZTSZ");
        jcbSztsz.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbSztszActionPerformed(evt);
            }
        });

        jcbNev.setSelected(true);
        jcbNev.setText("Név");
        jcbNev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbNevActionPerformed(evt);
            }
        });

        jcbRendfokozat.setSelected(true);
        jcbRendfokozat.setText("Rendfokozat");
        jcbRendfokozat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbRendfokozatActionPerformed(evt);
            }
        });

        jcbAlakulat.setText("Alakulat");
        jcbAlakulat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbAlakulatActionPerformed(evt);
            }
        });

        jcbSzulDatum.setSelected(true);
        jcbSzulDatum.setText("Születési dátum");
        jcbSzulDatum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbSzulDatumActionPerformed(evt);
            }
        });

        jcbNeme.setText("Neme");
        jcbNeme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbNemeActionPerformed(evt);
            }
        });

        jcbAnyjaNeve.setText("Anyja neve");
        jcbAnyjaNeve.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbAnyjaNeveActionPerformed(evt);
            }
        });

        jcbMagassag.setText("Magasság");
        jcbMagassag.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbMagassagActionPerformed(evt);
            }
        });

        jcbSuly.setText("Súly");
        jcbSuly.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbSulyActionPerformed(evt);
            }
        });

        jcbTestZsir.setText("Testzsír");
        jcbTestZsir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbTestZsirActionPerformed(evt);
            }
        });

        jcbVegrehajtasIdeje.setSelected(true);
        jcbVegrehajtasIdeje.setText("Végrehajtás ideje");
        jcbVegrehajtasIdeje.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbVegrehajtasIdejeActionPerformed(evt);
            }
        });

        jcbKarEredmeny.setSelected(true);
        jcbKarEredmeny.setText("Kar eredmény");
        jcbKarEredmeny.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbKarEredmenyActionPerformed(evt);
            }
        });

        jcbKarMozgas.setSelected(true);
        jcbKarMozgas.setText("Kar mozgásforma");
        jcbKarMozgas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbKarMozgasActionPerformed(evt);
            }
        });

        jcbTorzsMozgas.setSelected(true);
        jcbTorzsMozgas.setText("Törzs mozgásforma");
        jcbTorzsMozgas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbTorzsMozgasActionPerformed(evt);
            }
        });

        jcbTorzsEredmeny.setSelected(true);
        jcbTorzsEredmeny.setText("Törzs eredmény");
        jcbTorzsEredmeny.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbTorzsEredmenyActionPerformed(evt);
            }
        });

        jcbKeringesMozgas.setSelected(true);
        jcbKeringesMozgas.setText("Keringés mozgásforma");
        jcbKeringesMozgas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbKeringesMozgasActionPerformed(evt);
            }
        });

        jcbKeringesEredmeny.setSelected(true);
        jcbKeringesEredmeny.setText("Keringés eredmény");
        jcbKeringesEredmeny.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbKeringesEredmenyActionPerformed(evt);
            }
        });

        jcbPontszam.setSelected(true);
        jcbPontszam.setText("Pontszám");
        jcbPontszam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbPontszamActionPerformed(evt);
            }
        });

        jcbBmi.setText("BMI");
        jcbBmi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbBmiActionPerformed(evt);
            }
        });

        jcbTestZsirIndex.setText("Testzsír index");
        jcbTestZsirIndex.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbTestZsirIndexActionPerformed(evt);
            }
        });

        jbOk.setText("OK");
        jbOk.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ENTER"), "OK");
        jbOk.getActionMap().put("OK", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jbOkActionPerformed(e);
            }
        });
        jbOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbOkActionPerformed(evt);
            }
        });

        jbModositas.setText("Módosítása");
        jbModositas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbModositasActionPerformed(evt);
            }
        });

        jbTorles.setText("Törlése");
        jbTorles.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbTorlesActionPerformed(evt);
            }
        });

        jbHozzaadas.setText("Hozzáadása");
        jbHozzaadas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbHozzaadasActionPerformed(evt);
            }
        });

        jLabel2.setText("Felmérés");

        jbIgazolas.setText("Fáf igazolás készítése");
        jbIgazolas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbIgazolasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1354, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jcbSorszam)
                                    .addComponent(jcbSztsz))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jcbRendfokozat)
                                    .addComponent(jcbNev))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jcbSzulDatum)
                                    .addComponent(jcbAlakulat))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jcbAnyjaNeve)
                                    .addComponent(jcbNeme))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jcbMagassag)
                                    .addComponent(jcbSuly))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jcbVegrehajtasIdeje)
                                    .addComponent(jcbTestZsir))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jcbKarMozgas)
                                    .addComponent(jcbKarEredmeny))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jcbTorzsMozgas)
                                    .addComponent(jcbTorzsEredmeny))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jcbKeringesMozgas)
                                    .addComponent(jcbKeringesEredmeny))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jcbBmi)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jcbPontszam)
                                        .addGap(18, 18, 18)
                                        .addComponent(jcbTestZsirIndex))))
                            .addComponent(jLabel1))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(115, 115, 115)
                .addComponent(jbHozzaadas)
                .addGap(90, 90, 90)
                .addComponent(jbModositas)
                .addGap(91, 91, 91)
                .addComponent(jbTorles, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(91, 91, 91)
                .addComponent(jbIgazolas)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jbOk, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(251, 251, 251))
            .addGroup(layout.createSequentialGroup()
                .addGap(314, 314, 314)
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcbSorszam)
                    .addComponent(jcbNev)
                    .addComponent(jcbAlakulat)
                    .addComponent(jcbAnyjaNeve)
                    .addComponent(jcbMagassag)
                    .addComponent(jcbTestZsir)
                    .addComponent(jcbKarMozgas)
                    .addComponent(jcbTorzsMozgas)
                    .addComponent(jcbKeringesMozgas)
                    .addComponent(jcbPontszam)
                    .addComponent(jcbTestZsirIndex))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcbSztsz)
                    .addComponent(jcbRendfokozat)
                    .addComponent(jcbSzulDatum)
                    .addComponent(jcbNeme)
                    .addComponent(jcbSuly)
                    .addComponent(jcbVegrehajtasIdeje)
                    .addComponent(jcbKarEredmeny)
                    .addComponent(jcbTorzsEredmeny)
                    .addComponent(jcbKeringesEredmeny)
                    .addComponent(jcbBmi))
                .addGap(11, 11, 11)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbOk)
                    .addComponent(jbModositas)
                    .addComponent(jbTorles)
                    .addComponent(jbHozzaadas)
                    .addComponent(jbIgazolas))
                .addGap(28, 28, 28))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jcbSorszamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbSorszamActionPerformed
        if (!jcbSorszam.isSelected()) {
            oszlopEltuntet(0);
        } else {
            oszlopElohoz(0, 65);
        }
    }//GEN-LAST:event_jcbSorszamActionPerformed

    private void jcbSztszActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbSztszActionPerformed
        if (!jcbSztsz.isSelected()) {
            oszlopEltuntet(1);
        } else {
            oszlopElohoz(1, 90);
        }
    }//GEN-LAST:event_jcbSztszActionPerformed

    private void jcbNevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbNevActionPerformed
        if (!jcbNev.isSelected()) {
            oszlopEltuntet(2);
        } else {
            oszlopElohoz(2, 150);
        }
    }//GEN-LAST:event_jcbNevActionPerformed

    private void jcbRendfokozatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbRendfokozatActionPerformed
        if (!jcbRendfokozat.isSelected()) {
            oszlopEltuntet(3);
        } else {
            oszlopElohoz(3, 100);
        }
    }//GEN-LAST:event_jcbRendfokozatActionPerformed

    private void jcbAlakulatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbAlakulatActionPerformed
        if (!jcbAlakulat.isSelected()) {
            oszlopEltuntet(4);
        } else {
            oszlopElohoz(4, 100);
        }
    }//GEN-LAST:event_jcbAlakulatActionPerformed

    private void jcbSzulDatumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbSzulDatumActionPerformed
        if (!jcbSzulDatum.isSelected()) {
            oszlopEltuntet(5);
        } else {
            oszlopElohoz(5, 100);
        }
    }//GEN-LAST:event_jcbSzulDatumActionPerformed

    private void jcbAnyjaNeveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbAnyjaNeveActionPerformed
        if (!jcbAnyjaNeve.isSelected()) {
            oszlopEltuntet(6);
        } else {
            oszlopElohoz(6, 100);
        }
    }//GEN-LAST:event_jcbAnyjaNeveActionPerformed

    private void jcbNemeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbNemeActionPerformed
        if (!jcbNeme.isSelected()) {
            oszlopEltuntet(7);
        } else {
            oszlopElohoz(7, 45);
        }
    }//GEN-LAST:event_jcbNemeActionPerformed

    private void jcbMagassagActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbMagassagActionPerformed
        if (!jcbMagassag.isSelected()) {
            oszlopEltuntet(8);
        } else {
            oszlopElohoz(8, 75);
        }
    }//GEN-LAST:event_jcbMagassagActionPerformed

    private void jcbSulyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbSulyActionPerformed
        if (!jcbSuly.isSelected()) {
            oszlopEltuntet(9);
        } else {
            oszlopElohoz(9, 35);
        }
    }//GEN-LAST:event_jcbSulyActionPerformed

    private void jcbTestZsirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbTestZsirActionPerformed
        if (!jcbTestZsir.isSelected()) {
            oszlopEltuntet(10);
        } else {
            oszlopElohoz(10, 55);
        }
    }//GEN-LAST:event_jcbTestZsirActionPerformed

    private void jcbVegrehajtasIdejeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbVegrehajtasIdejeActionPerformed
        if (!jcbVegrehajtasIdeje.isSelected()) {
            oszlopEltuntet(11);
        } else {
            oszlopElohoz(11, 110);
        }
    }//GEN-LAST:event_jcbVegrehajtasIdejeActionPerformed

    private void jcbKarMozgasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbKarMozgasActionPerformed
        if (!jcbKarMozgas.isSelected()) {
            oszlopEltuntet(12);
        } else {
            oszlopElohoz(12, 80);
        }
    }//GEN-LAST:event_jcbKarMozgasActionPerformed

    private void jcbKarEredmenyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbKarEredmenyActionPerformed
        if (!jcbKarEredmeny.isSelected()) {
            oszlopEltuntet(13);
        } else {
            oszlopElohoz(13, 80);
        }
    }//GEN-LAST:event_jcbKarEredmenyActionPerformed

    private void jcbTorzsMozgasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbTorzsMozgasActionPerformed
        if (!jcbTorzsMozgas.isSelected()) {
            oszlopEltuntet(14);
        } else {
            oszlopElohoz(14, 80);
        }
    }//GEN-LAST:event_jcbTorzsMozgasActionPerformed

    private void jcbTorzsEredmenyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbTorzsEredmenyActionPerformed
        if (!jcbTorzsEredmeny.isSelected()) {
            oszlopEltuntet(15);
        } else {
            oszlopElohoz(15, 80);
        }
    }//GEN-LAST:event_jcbTorzsEredmenyActionPerformed

    private void jcbKeringesMozgasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbKeringesMozgasActionPerformed
        if (!jcbKeringesMozgas.isSelected()) {
            oszlopEltuntet(16);
        } else {
            oszlopElohoz(16, 80);
        }
    }//GEN-LAST:event_jcbKeringesMozgasActionPerformed

    private void jcbKeringesEredmenyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbKeringesEredmenyActionPerformed
        if (!jcbKeringesEredmeny.isSelected()) {
            oszlopEltuntet(17);
        } else {
            oszlopElohoz(17, 80);
        }
    }//GEN-LAST:event_jcbKeringesEredmenyActionPerformed

    private void jcbPontszamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbPontszamActionPerformed
        if (!jcbPontszam.isSelected()) {
            oszlopEltuntet(18);
        } else {
            oszlopElohoz(18, 70);
        }
    }//GEN-LAST:event_jcbPontszamActionPerformed

    private void jcbBmiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbBmiActionPerformed
        if (!jcbBmi.isSelected()) {
            oszlopEltuntet(19);
        } else {
            oszlopElohoz(19, 35);
        }
    }//GEN-LAST:event_jcbBmiActionPerformed

    private void jcbTestZsirIndexActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbTestZsirIndexActionPerformed
        if (!jcbTestZsirIndex.isSelected()) {
            oszlopEltuntet(20);
        } else {
            oszlopElohoz(20, 85);
        }
    }//GEN-LAST:event_jcbTestZsirIndexActionPerformed

    private void jbOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbOkActionPerformed
        setVisible(false);
        dispose();
    }//GEN-LAST:event_jbOkActionPerformed

    private void jbModositasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbModositasActionPerformed
        try {
            if (jtblMegtekinto.getSelectedRow() != ALAPERTELMEZETTNULLERTEK) {
                vezerles.felmeresModositas(jtblMegtekinto.getSelectedRow());
            } else {
                JOptionPane.showMessageDialog(null, "Nincs felmérés kijelölve!", "Hiba!", JOptionPane.OK_OPTION);
            }
        } catch (FafDAOException ex) {
            System.err.println(ex.getMessage());
        }
    }//GEN-LAST:event_jbModositasActionPerformed

    private void jbTorlesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbTorlesActionPerformed
        try {
            if (jtblMegtekinto.getSelectedRow() != ALAPERTELMEZETTNULLERTEK) {
                vezerles.felmeresTorles((Integer) jtblMegtekinto.getValueAt(jtblMegtekinto.getSelectedRow(), 0));
            } else {
                JOptionPane.showMessageDialog(null, "Nincs felmérés kijelölve!", "Hiba!", JOptionPane.OK_OPTION);
            }
        } catch (FafDAOException ex) {
            System.err.println(ex.getMessage());
        }
    }//GEN-LAST:event_jbTorlesActionPerformed

    private void jbHozzaadasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbHozzaadasActionPerformed
        try {
            vezerles.felmeresHozzaadas();
        } catch (FafDAOException ex) {
            System.err.println(ex.getMessage());
        }
    }//GEN-LAST:event_jbHozzaadasActionPerformed

    private void jbIgazolasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbIgazolasActionPerformed
        try {
            chooser.setCurrentDirectory(chooser.getSelectedFile());
            int choice = chooser.showOpenDialog(this);
            if (choice != JFileChooser.APPROVE_OPTION) return;
            File chosenDir = chooser.getSelectedFile();
            fafIgazolasKeszites(chosenDir);
        } catch (FafDAOException ex) {
            System.err.println(ex.getMessage());
        }
    }//GEN-LAST:event_jbIgazolasActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton jbHozzaadas;
    private javax.swing.JButton jbIgazolas;
    private javax.swing.JButton jbModositas;
    private javax.swing.JButton jbOk;
    private javax.swing.JButton jbTorles;
    private javax.swing.JCheckBox jcbAlakulat;
    private javax.swing.JCheckBox jcbAnyjaNeve;
    private javax.swing.JCheckBox jcbBmi;
    private javax.swing.JCheckBox jcbKarEredmeny;
    private javax.swing.JCheckBox jcbKarMozgas;
    private javax.swing.JCheckBox jcbKeringesEredmeny;
    private javax.swing.JCheckBox jcbKeringesMozgas;
    private javax.swing.JCheckBox jcbMagassag;
    private javax.swing.JCheckBox jcbNeme;
    private javax.swing.JCheckBox jcbNev;
    private javax.swing.JCheckBox jcbPontszam;
    private javax.swing.JCheckBox jcbRendfokozat;
    private javax.swing.JCheckBox jcbSorszam;
    private javax.swing.JCheckBox jcbSuly;
    private javax.swing.JCheckBox jcbSztsz;
    private javax.swing.JCheckBox jcbSzulDatum;
    private javax.swing.JCheckBox jcbTestZsir;
    private javax.swing.JCheckBox jcbTestZsirIndex;
    private javax.swing.JCheckBox jcbTorzsEredmeny;
    private javax.swing.JCheckBox jcbTorzsMozgas;
    private javax.swing.JCheckBox jcbVegrehajtasIdeje;
    private javax.swing.JTable jtblMegtekinto;
    // End of variables declaration//GEN-END:variables
}
