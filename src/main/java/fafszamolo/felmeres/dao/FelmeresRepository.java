package fafszamolo.felmeres.dao;

import fafszamolo.model.FizikaiAllapotFelmeres;
import fafszamolo.exception.FafDAOException;
import java.util.List;

/**
 * A fizikai állapot felméréseket tartalmazó fájlok, adatbázisok elérésére szolgáló osztályok közös interface-je.
 */
public interface FelmeresRepository {
    
    /**
     * Az összes felmérés lekérdezésére szolgáló metódus.
     *
     * @return Visszaadja az összes felmérést tartalmazó listát.
     */
    List<FizikaiAllapotFelmeres> findAll() throws FafDAOException;
    
    /**
     * SZTSZ szám szerint megkeresi a felméréseket.
     *
     * @param sztsz Az SZTSZ szám, ami alapján keressük a felméréseket.
     * @return Visszaadja a felmérések listáját.
     */
    List<FizikaiAllapotFelmeres> findBySZTSZ(int sztsz) throws FafDAOException;
    
    /**
     * Kilistázza az adott évben végrehajtott felméréseket.
     *
     * @param ev Az év, ami alapján keressük a felméréseket.
     * @return Visszaadja a felmérések listáját.
     */
    List<FizikaiAllapotFelmeres> findByEv(int ev) throws FafDAOException;
    
    /**
     * Elmenti az adatbázisba a felmérés objektumot. Ha nem létezik még a felmérés
     * az adatbázisban, akkor újat ad hozzá, ha létezik, akkor felülírja a
     * meglévőt.
     *
     * @param felmeres A felmérés, amit el akarunk menteni az adatbázisba.
     */
    void saveFelmeres(FizikaiAllapotFelmeres felmeres) throws FafDAOException;
    
    /**
     * Új felmérés felvitele az adatbázisba.
     */
    void insertFelmeres(FizikaiAllapotFelmeres felmeres) throws FafDAOException;
    
    /**
     * Felmérés adatainak frissítése az adatbázisban.
     */
    void updateFelmeres(FizikaiAllapotFelmeres felmeres) throws FafDAOException;
    
    /**
     * Töröl egy felmérést a megadott sorszám alapján az adatbázisból.
     */
    void deleteFelmeres(int sorszam) throws FafDAOException;
    
}
