package fafszamolo.felmeres.dao.impl;

import fafszamolo.model.FizikaiAllapotFelmeres;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import fafszamolo.exception.FafDAOException;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import fafszamolo.felmeres.dao.FelmeresRepository;

/**
 * A fizikai állapot felméréseket tartalmazó .json fájl elérésére való osztály.
 */
public class FelmeresRepositoryJSONImpl implements FelmeresRepository {

    private String utvonal;

    /**
     * Normál esetben ezzel a konstruktorral hozzunk létre az objektumot, a
     * személy adatbázis így elérhető.
     */
    public FelmeresRepositoryJSONImpl() {
        this.utvonal = "src/fafszamolo/json/";
    }

    /**
     * Ezzel a konstruktorral beállítható az elérési útvonal a személy
     * adatbázishoz. Ezzel lehetséges a tesztelés is akár.
     */
    public FelmeresRepositoryJSONImpl(String utvonal) {
        this.utvonal = utvonal;
    }
    
    @Override
    public List<FizikaiAllapotFelmeres> findAll() throws FafDAOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(new File(utvonal + "felmeresek.json"));
            List<FizikaiAllapotFelmeres> lista = new ArrayList<>();
            for (int i = 0; i < root.size(); i++) {
                Integer sorszam = root.get(i).get("sorszam").asInt();
                int sztsz = root.get(i).get("sztsz").asInt();
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                LocalDate vegrehajtasIdeje = LocalDate.parse(root.get(i).get("vegrehajtasIdeje").asText(), dtf);
                int karMozgasforma = root.get(i).get("karMozgasforma").asInt();
                int karEredmeny = root.get(i).get("karEredmeny").asInt();
                int torzsMozgasforma = root.get(i).get("torzsMozgasforma").asInt();
                int torzsEredmeny = root.get(i).get("torzsEredmeny").asInt();
                int keringesMozgasforma = root.get(i).get("keringesMozgasforma").asInt();
                int keringesEredmeny = root.get(i).get("keringesEredmeny").asInt();
                int pontszam = root.get(i).get("pontszam").asInt();
                String bmi = root.get(i).get("bmi").asText();
                String testZsirIndex = root.get(i).get("testZsirIndex").asText();
                FizikaiAllapotFelmeres faf = new FizikaiAllapotFelmeres(sorszam, sztsz, vegrehajtasIdeje, karMozgasforma, karEredmeny, torzsMozgasforma,
                        torzsEredmeny, keringesMozgasforma, keringesEredmeny, pontszam, bmi, testZsirIndex);
                lista.add(faf);
            }
            return lista;
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public List<FizikaiAllapotFelmeres> findBySZTSZ(int sztsz) throws FafDAOException {
        List<FizikaiAllapotFelmeres> lista = new ArrayList<>();
        for (FizikaiAllapotFelmeres faf : findAll()) {
            if (faf.getSztsz() == sztsz) {
                lista.add(faf);
            }
        }
        return lista;
    }

    @Override
    public List<FizikaiAllapotFelmeres> findByEv(int ev) throws FafDAOException {
        List<FizikaiAllapotFelmeres> lista = new ArrayList<>();
        LocalDate kezdoDatum = LocalDate.of(ev, 01, 01);
        LocalDate vegsoDatum = LocalDate.of(ev + 1, 01, 01);
        for (FizikaiAllapotFelmeres faf : findAll()) {
            if (faf.getVegrehajtasIdeje().isAfter(kezdoDatum) && faf.getVegrehajtasIdeje().isBefore(vegsoDatum)) {
                lista.add(faf);
            }
        }
        return lista;
    }

    @Override
    public void saveFelmeres(FizikaiAllapotFelmeres faf) throws FafDAOException {
        if (faf.getSorszam() == null) {
            insertFelmeres(faf);
        } else {
            updateFelmeres(faf);
        }
    }

    @Override
    public void insertFelmeres(FizikaiAllapotFelmeres faf) throws FafDAOException {
        List<FizikaiAllapotFelmeres> lista = findAll();
        faf.setSorszam(lista.get(lista.size() - 1).getSorszam() + 1);
        lista.add(faf);
        kiir(lista);
    }

    @Override
    public void updateFelmeres(FizikaiAllapotFelmeres faf) throws FafDAOException {
        List<FizikaiAllapotFelmeres> lista = findAll();
        int index = 0;
        for (FizikaiAllapotFelmeres felmeres : lista) {
            if (felmeres.getSorszam() == faf.getSorszam()) {
                index = lista.indexOf(felmeres);
            }
        }
        lista.set(index, faf);
        kiir(lista);
    }

    @Override
    public void deleteFelmeres(int sorszam) throws FafDAOException {
        List<FizikaiAllapotFelmeres> lista = new ArrayList<>();
        for (FizikaiAllapotFelmeres faf : findAll()) {
            if (faf.getSorszam() != sorszam) {
                lista.add(faf);
            }
        }
        kiir(lista);
    }
    
    /**
     * Segédmetódus a fájlba való kiíráshoz.
     */
    private void kiir(List<FizikaiAllapotFelmeres> lista) throws FafDAOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
            ArrayNode felmeresek = mapper.createArrayNode();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            for (FizikaiAllapotFelmeres faf : lista) {
                ObjectNode ujFaf = felmeresek.addObject();
                ujFaf.put("sorszam", faf.getSorszam());
                ujFaf.put("sztsz", faf.getSztsz());
                ujFaf.put("vegrehajtasIdeje", faf.getVegrehajtasIdeje().format(dtf));
                ujFaf.put("karMozgasforma", faf.getKarMozgasforma());
                ujFaf.put("karEredmeny", faf.getKarEredmeny());
                ujFaf.put("torzsMozgasforma", faf.getTorzsMozgasforma());
                ujFaf.put("torzsEredmeny", faf.getTorzsEredmeny());
                ujFaf.put("keringesMozgasforma", faf.getKeringesMozgasforma());
                ujFaf.put("keringesEredmeny", faf.getKeringesEredmeny());
                ujFaf.put("pontszam", faf.getPontszam());
                ujFaf.put("bmi", faf.getBmi());
                ujFaf.put("testZsirIndex", faf.getTestZsirIndex());
            }
            writer.writeValue(new File(utvonal + "felmeresek.json"), felmeresek);
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
    
}
