package fafszamolo.felmeres.dao.impl;

import fafszamolo.model.FizikaiAllapotFelmeres;
import fafszamolo.exception.FafDAOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import fafszamolo.felmeres.dao.FelmeresRepository;

/**
 * A fizikai állapot felméréseket tartalmazó .csv fájl elérésére való osztály.
 */
public class FelmeresRepositoryCSVImpl implements FelmeresRepository {

    private String utvonal;

    /**
     * Normál esetben ezzel a konstruktorral hozzunk létre az objektumot, a
     * személy adatbázis így elérhető.
     */
    public FelmeresRepositoryCSVImpl() {
        this.utvonal = "src/main/java/fafszamolo/csv/";
    }

    /**
     * Ezzel a konstruktorral beállítható az elérési útvonal a személy
     * adatbázishoz. Ezzel lehetséges a tesztelés is akár.
     */
    public FelmeresRepositoryCSVImpl(String utvonal) {
        this.utvonal = utvonal;
    }

    @Override
    public List<FizikaiAllapotFelmeres> findAll() throws FafDAOException {
        List<FizikaiAllapotFelmeres> lista = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(utvonal + "felmeresek.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (!line.contains("#")) {
                    if (!line.contains(";")) {
                        throw new FafDAOException("Hiba a fájl olvasása közben: túl kevés adat, vagy nem megfelelő szeparátor.");
                    }
                    String[] tomb = line.split(";");
                    Integer sorszam = Integer.parseInt(tomb[0]);
                    int sztsz = Integer.parseInt(tomb[1]);
                    LocalDate vegrehajtasIdeje = LocalDate.parse(tomb[2]);
                    int karMozgasforma = Integer.parseInt(tomb[3]);
                    int karEredmeny = Integer.parseInt(tomb[4]);
                    int torzsMozgasforma = Integer.parseInt(tomb[5]);
                    int torzsEredmeny = Integer.parseInt(tomb[6]);
                    int keringesMozgasforma = Integer.parseInt(tomb[7]);
                    int keringesEredmeny = Integer.parseInt(tomb[8]);
                    int pontszam = Integer.parseInt(tomb[9]);
                    String bmi = tomb[10];
                    String testZsirIndex = tomb[11];
                    lista.add(new FizikaiAllapotFelmeres(sorszam, sztsz, vegrehajtasIdeje, karMozgasforma, karEredmeny, torzsMozgasforma, torzsEredmeny,
                            keringesMozgasforma, keringesEredmeny, pontszam, bmi, testZsirIndex));
                }
            }
            return lista;
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public List<FizikaiAllapotFelmeres> findBySZTSZ(int sztsz) throws FafDAOException {
        List<FizikaiAllapotFelmeres> lista = new ArrayList<>();
        for (FizikaiAllapotFelmeres faf : findAll()) {
            if (faf.getSztsz() == sztsz) {
                lista.add(faf);
            }
        }
        return lista;
    }
    
    @Override
    public List<FizikaiAllapotFelmeres> findByEv(int ev) throws FafDAOException {
        List<FizikaiAllapotFelmeres> lista = new ArrayList<>();
        LocalDate kezdoDatum = LocalDate.of(ev, 01, 01);
        LocalDate vegsoDatum = LocalDate.of(ev + 1, 01, 01);
        for (FizikaiAllapotFelmeres faf : findAll()) {
            if (faf.getVegrehajtasIdeje().isAfter(kezdoDatum) && faf.getVegrehajtasIdeje().isBefore(vegsoDatum)) {
                lista.add(faf);
            }
        }
        return lista;
    }
    
    @Override
    public void saveFelmeres(FizikaiAllapotFelmeres faf) throws FafDAOException {
        if (faf.getSorszam() == null) {
            insertFelmeres(faf);
        } else {
            updateFelmeres(faf);
        }
    }
    
    @Override
    public void insertFelmeres(FizikaiAllapotFelmeres faf) throws FafDAOException {
        List<FizikaiAllapotFelmeres> lista = findAll();
        faf.setSorszam(lista.get(lista.size() - 1).getSorszam() + 1);
        lista.add(faf);
        kiir(lista);
    }

    @Override
    public void updateFelmeres(FizikaiAllapotFelmeres faf) throws FafDAOException {
        List<FizikaiAllapotFelmeres> lista = findAll();
        int index = 0;
        for (FizikaiAllapotFelmeres felmeres : lista) {
            if (felmeres.getSorszam() == faf.getSorszam()) {
                index = lista.indexOf(felmeres);
            }
        }
        lista.set(index, faf);
        kiir(lista);
    }

    @Override
    public void deleteFelmeres(int sorszam) throws FafDAOException {
        List<FizikaiAllapotFelmeres> lista = new ArrayList<>();
        for (FizikaiAllapotFelmeres faf : findAll()) {
            if (faf.getSorszam() != sorszam) {
                lista.add(faf);
            }
        }
        kiir(lista);
    }
    
    /**
     * Segédmetódus a fájlba való kiíráshoz.
     */
    public void kiir(List<FizikaiAllapotFelmeres> lista) throws FafDAOException {
        try (PrintWriter pw = new PrintWriter(new FileWriter(utvonal + "felmeresek.csv"))) {
            for (FizikaiAllapotFelmeres faf : lista) {
                pw.println(faf.getSorszam() + ";" + faf.getSztsz() + ";" + faf.getVegrehajtasIdeje() + ";" + faf.getKarMozgasforma() + ";" + faf.getKarEredmeny()
                 + ";" + faf.getTorzsMozgasforma() + ";" + faf.getTorzsEredmeny() + ";" + faf.getKeringesMozgasforma() + ";" + faf.getKeringesEredmeny()
                 + ";" + faf.getPontszam() + ";" + faf.getBmi() + ";" + faf.getTestZsirIndex());
            }
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
}
