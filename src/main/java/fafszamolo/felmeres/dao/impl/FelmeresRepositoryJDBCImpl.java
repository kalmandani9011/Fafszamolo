package fafszamolo.felmeres.dao.impl;

import fafszamolo.model.FizikaiAllapotFelmeres;
import fafszamolo.exception.FafDAOException;
import fafszamolo.felmeres.dao.FelmeresRepository;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * A fizikai állapot felméréseket tartalmazó adatbázissal való kommunikációra alkalmas osztály.
 */
public class FelmeresRepositoryJDBCImpl implements FelmeresRepository {

    private Connection conn;
    private PreparedStatement findAll;
    private PreparedStatement insertFelmeres;
    private PreparedStatement updateFelmeres;
    private PreparedStatement deleteFelmeres;
    private PreparedStatement findBySZTSZ;
    private PreparedStatement findByEv;

    public FelmeresRepositoryJDBCImpl(Connection conn) throws FafDAOException {
        try {
            this.conn = conn;
            this.findAll = this.conn.prepareStatement("SELECT * FROM faftabla");
            this.findBySZTSZ = this.conn.prepareStatement("SELECT * FROM faftabla WHERE sztsz = ?");
            this.findByEv = this.conn.prepareStatement("SELECT * FROM faftabla WHERE vegrehajtasIdeje > ? AND vegrehajtasIdeje < ?");
            this.insertFelmeres = this.conn.prepareStatement("INSERT INTO faftabla (sztsz, vegrehajtasIdeje, karMozgasforma, karEredmeny, torzsMozgasforma, torzsEredmeny,"
                    + "keringesMozgasforma, keringesEredmeny, pontszam, bmi, testZsirIndex) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
            this.updateFelmeres = this.conn.prepareStatement("UPDATE faftabla SET sztsz = ?, vegrehajtasIdeje = ?, karMozgasforma = ?, karEredmeny = ?,"
                    + "torzsMozgasforma = ?, torzsEredmeny = ?, keringesMozgasforma = ?, keringesEredmeny = ?, pontszam = ?, bmi = ?, testZsirIndex = ? WHERE sorszam = ?");
            this.deleteFelmeres = this.conn.prepareStatement("DELETE FROM faftabla WHERE sorszam = ?");
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
    
    @Override
    public List<FizikaiAllapotFelmeres> findAll() throws FafDAOException {
        try {
            ResultSet all = this.findAll.executeQuery();
            List<FizikaiAllapotFelmeres> lista = makeList(all);
            return lista;
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public List<FizikaiAllapotFelmeres> findBySZTSZ(int sztsz) throws FafDAOException {
        try {
            findBySZTSZ.setInt(1, sztsz);
            ResultSet rsSztsz = this.findBySZTSZ.executeQuery();
            List<FizikaiAllapotFelmeres> lista = makeList(rsSztsz);
            return lista;
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public List<FizikaiAllapotFelmeres> findByEv(int ev) throws FafDAOException {
        try {
            LocalDate kezdoDatum = LocalDate.of(ev, 01, 01);
            LocalDate vegsoDatum = LocalDate.of(ev + 1, 01, 01);
            findByEv.setDate(1, Date.valueOf(kezdoDatum));
            findByEv.setDate(2, Date.valueOf(vegsoDatum));
            ResultSet rsEv = this.findByEv.executeQuery();
            List<FizikaiAllapotFelmeres> lista = makeList(rsEv);
            return lista;
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public void saveFelmeres(FizikaiAllapotFelmeres felmeres) throws FafDAOException {
        if (felmeres.getSorszam() == null) {
            insertFelmeres(felmeres);
        } else {
            updateFelmeres(felmeres);
        }
    }
    
    @Override
    public void insertFelmeres(FizikaiAllapotFelmeres felmeres) throws FafDAOException {
        try {
            insertFelmeres.setInt(1, felmeres.getSztsz());
            insertFelmeres.setDate(2, Date.valueOf(felmeres.getVegrehajtasIdeje()));
            insertFelmeres.setInt(3, felmeres.getKarMozgasforma());
            insertFelmeres.setInt(4, felmeres.getKarEredmeny());
            insertFelmeres.setInt(5, felmeres.getTorzsMozgasforma());
            insertFelmeres.setInt(6, felmeres.getTorzsEredmeny());
            insertFelmeres.setInt(7, felmeres.getKeringesMozgasforma());
            insertFelmeres.setInt(8, felmeres.getKeringesEredmeny());
            insertFelmeres.setInt(9, felmeres.getPontszam());
            insertFelmeres.setString(10, felmeres.getBmi());
            insertFelmeres.setString(11, felmeres.getTestZsirIndex());
            insertFelmeres.executeUpdate();
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public void updateFelmeres(FizikaiAllapotFelmeres felmeres) throws FafDAOException {
        try {
            updateFelmeres.setInt(1, felmeres.getSztsz());
            updateFelmeres.setDate(2, Date.valueOf(felmeres.getVegrehajtasIdeje()));
            updateFelmeres.setInt(3, felmeres.getKarMozgasforma());
            updateFelmeres.setInt(4, felmeres.getKarEredmeny());
            updateFelmeres.setInt(5, felmeres.getTorzsMozgasforma());
            updateFelmeres.setInt(6, felmeres.getTorzsEredmeny());
            updateFelmeres.setInt(7, felmeres.getKeringesMozgasforma());
            updateFelmeres.setInt(8, felmeres.getKeringesEredmeny());
            updateFelmeres.setInt(9, felmeres.getPontszam());
            updateFelmeres.setString(10, felmeres.getBmi());
            updateFelmeres.setString(11, felmeres.getTestZsirIndex());
            updateFelmeres.setInt(12, felmeres.getSorszam());
            updateFelmeres.executeUpdate();
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
    
    @Override
    public void deleteFelmeres(int sorszam) throws FafDAOException {
        try {
            this.deleteFelmeres.setInt(1, sorszam);
            this.deleteFelmeres.executeUpdate();
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
    
    /**
     * Segédmetódus, ami felmérés objektumokból egy listát készít.
     */
    private List<FizikaiAllapotFelmeres> makeList(ResultSet rs) throws FafDAOException {
        try {
            List<FizikaiAllapotFelmeres> lista = new ArrayList();
            while (rs.next()) {
                lista.add(makeOne(rs));
            }
            return lista;
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    /**
     * Segédmetódus, ami egy felmérés objektumot készít.
     */
    private FizikaiAllapotFelmeres makeOne(ResultSet rs) throws FafDAOException {
        try {
            FizikaiAllapotFelmeres felmeres = new FizikaiAllapotFelmeres();
            felmeres.setSorszam(rs.getInt("sorszam"));
            felmeres.setSztsz(rs.getInt("sztsz"));
            felmeres.setVegrehajtasIdeje(rs.getDate("vegrehajtasIdeje").toLocalDate());
            felmeres.setKarMozgasforma(rs.getInt("karMozgasforma"));
            felmeres.setKarEredmeny(rs.getInt("karEredmeny"));
            felmeres.setTorzsMozgasforma(rs.getInt("torzsMozgasforma"));
            felmeres.setTorzsEredmeny(rs.getInt("torzsEredmeny"));
            felmeres.setKeringesMozgasforma(rs.getInt("keringesMozgasforma"));
            felmeres.setKeringesEredmeny(rs.getInt("keringesEredmeny"));
            felmeres.setPontszam(rs.getInt("pontSzam"));
            felmeres.setBmi(rs.getString("bmi"));
            felmeres.setTestZsirIndex(rs.getString("testZsirIndex"));
            return felmeres;
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
}
