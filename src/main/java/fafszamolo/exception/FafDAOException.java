package fafszamolo.exception;

/**
 *
 * @author
 */
public class FafDAOException extends Exception{

    public FafDAOException() {
    }

    public FafDAOException(String message) {
        super(message);
    }

    public FafDAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public FafDAOException(Throwable cause) {
        super(cause);
    }
}
