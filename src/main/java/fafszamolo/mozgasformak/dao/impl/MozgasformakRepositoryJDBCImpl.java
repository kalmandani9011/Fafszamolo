package fafszamolo.mozgasformak.dao.impl;

import fafszamolo.model.BMI;
import fafszamolo.model.Ergometria;
import fafszamolo.model.Fekvo;
import fafszamolo.model.Felules;
import fafszamolo.model.FuggoTerdEmeles;
import fafszamolo.model.Futas2000;
import fafszamolo.model.Futas3200;
import fafszamolo.model.HajlitottFugges;
import fafszamolo.model.Huzodzkodas;
import fafszamolo.model.KarMozgasformak;
import fafszamolo.model.KeringesMozgasformak;
import fafszamolo.model.LapockaEmeles;
import fafszamolo.model.Menet1600;
import fafszamolo.model.TorzsMozgasformak;
import fafszamolo.exception.FafDAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import fafszamolo.mozgasformak.dao.MozgasformakRepository;

/**
 * Az alapadatokat tartalmazó adatbázissal való kommunikációra alkalmas osztály.
 */
public class MozgasformakRepositoryJDBCImpl implements MozgasformakRepository {

    private static final int FELULESCOMBOBOXELEM = 0;
    private static final int LAPOCKAEMELESCOMBOBOXELEM = 1;
    private static final int FUGGOTERDEMELESCOMBOBOXELEM = 2;
    private Connection conn;
    private PreparedStatement fekvoLekerdezes;
    private PreparedStatement huzodzkodasLekerdezes;
    private PreparedStatement hajlitottFuggesLekerdezes;
    private PreparedStatement felulesLekerdezes;
    private PreparedStatement lapockaEmelesLekerdezes;
    private PreparedStatement fuggoTerdEmelesLekerdezes;
    private PreparedStatement futas3200Lekerdezes;
    private PreparedStatement futas2000Lekerdezes;
    private PreparedStatement menet1600Lekerdezes;
    private PreparedStatement ergometriaLekerdezes;
    private PreparedStatement bmiLekerdezes;
    private Map<String, PreparedStatement> lekerdezesek;

    public MozgasformakRepositoryJDBCImpl(Connection conn) throws FafDAOException {
        try {
            this.conn = conn;
            this.fekvoLekerdezes = conn.prepareStatement("SELECT * FROM fekvo WHERE ismetles = ?");
            this.huzodzkodasLekerdezes = conn.prepareStatement("SELECT * FROM huzodzkodas WHERE ismetles = ?");
            this.hajlitottFuggesLekerdezes = conn.prepareStatement("SELECT * FROM hajlitottfugges WHERE ido = ?");
            this.felulesLekerdezes = conn.prepareStatement("SELECT * FROM felules WHERE ismetles = ?");
            this.lapockaEmelesLekerdezes = conn.prepareStatement("SELECT * FROM lapockaemeles WHERE ismetles = ?");
            this.fuggoTerdEmelesLekerdezes = conn.prepareStatement("SELECT * FROM fuggoterdemeles WHERE ismetles = ?");
            this.futas3200Lekerdezes = conn.prepareStatement("SELECT * FROM futas3200 WHERE ido = ?");
            this.futas2000Lekerdezes = conn.prepareStatement("SELECT * FROM futas2000 WHERE ido = ?");
            this.menet1600Lekerdezes = conn.prepareStatement("SELECT * FROM menet1600 WHERE ido = ?");
            this.ergometriaLekerdezes = conn.prepareStatement("SELECT * FROM ergometria WHERE wattkg = ?");
            this.bmiLekerdezes = conn.prepareStatement("SELECT * FROM bmi WHERE magassag = ?");
            lekerdezesek = new HashMap<>();
            lekerdezesek.put("fekvo", fekvoLekerdezes);
            lekerdezesek.put("huzodzkodas", huzodzkodasLekerdezes);
            lekerdezesek.put("hajlitottFugges", hajlitottFuggesLekerdezes);
            lekerdezesek.put("felules", felulesLekerdezes);
            lekerdezesek.put("lapockaEmeles", lapockaEmelesLekerdezes);
            lekerdezesek.put("fuggoTerdEmeles", fuggoTerdEmelesLekerdezes);
            lekerdezesek.put("futas3200", futas3200Lekerdezes);
            lekerdezesek.put("futas2000", futas2000Lekerdezes);
            lekerdezesek.put("menet1600", menet1600Lekerdezes);
            lekerdezesek.put("ergometria", ergometriaLekerdezes);
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public int findPontszam(String tabla, int eredmeny, String korcsoport) throws FafDAOException {
        try {
            ResultSet rs;
            lekerdezesek.get(tabla).setInt(1, eredmeny);
            rs = lekerdezesek.get(tabla).executeQuery();
            rs.next();
            return rs.getInt(korcsoport);
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public int[] findBMIPontszam(int magassag, String korcsoport) throws FafDAOException {
        try {
            ResultSet rs;
            bmiLekerdezes.setInt(1, magassag);
            rs = bmiLekerdezes.executeQuery();
            rs.next();
            return new int[]{rs.getInt(korcsoport), rs.getInt("minimumSuly")};
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public List<BMI> beolvasBMI() throws FafDAOException {
        try {
            List<BMI> lista = new ArrayList<>();
            PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM bmi");
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                BMI bmi = new BMI();
                bmi.setMagassag(rs.getInt("magassag"));
                bmi.setFfiKorcsopEgy(rs.getInt("ffiKorcsopEgy"));
                bmi.setNoKorcsopEgy(rs.getInt("noKorcsopEgy"));
                bmi.setFfiKorcsopKetto(rs.getInt("ffiKorcsopKetto"));
                bmi.setNoKorcsopKetto(rs.getInt("noKorcsopKetto"));
                bmi.setFfiKorcsopHarom(rs.getInt("ffiKorcsopHarom"));
                bmi.setNoKorcsopHarom(rs.getInt("noKorcsopHarom"));
                bmi.setFfiKorcsopNegy(rs.getInt("ffiKorcsopNegy"));
                bmi.setNoKorcsopNegy(rs.getInt("noKorcsopNegy"));
                bmi.setMinSuly(rs.getInt("minimumSuly"));
                lista.add(bmi);
            }
            return lista;
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public List<Fekvo> beolvasFekvo() throws FafDAOException {
        try {
            List<Fekvo> lista = new ArrayList<>();
            PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM fekvo");
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                Fekvo f = new Fekvo();
                f.setIsmetles(rs.getInt("ismetles"));
                f.setFfiKorcsopEgy(rs.getInt("ffiKorcsopEgy"));
                f.setNoKorcsopEgy(rs.getInt("noKorcsopEgy"));
                f.setFfiKorcsopKetto(rs.getInt("ffiKorcsopKetto"));
                f.setNoKorcsopKetto(rs.getInt("noKorcsopKetto"));
                f.setFfiKorcsopHarom(rs.getInt("ffiKorcsopHarom"));
                f.setNoKorcsopHarom(rs.getInt("noKorcsopHarom"));
                f.setFfiKorcsopNegy(rs.getInt("ffiKorcsopNegy"));
                f.setNoKorcsopNegy(rs.getInt("noKorcsopNegy"));
                f.setFfiKorcsopOt(rs.getInt("ffiKorcsopOt"));
                f.setNoKorcsopOt(rs.getInt("noKorcsopOt"));
                f.setFfiKorcsopHat(rs.getInt("ffiKorcsopHat"));
                f.setNoKorcsopHat(rs.getInt("noKorcsopHat"));
                f.setFfiKorcsopHet(rs.getInt("ffiKorcsopHet"));
                f.setNoKorcsopHet(rs.getInt("noKorcsopHet"));
                f.setFfiKorcsopNyolc(rs.getInt("ffiKorcsopNyolc"));
                f.setNoKorcsopNyolc(rs.getInt("noKorcsopNyolc"));
                f.setFfiKorcsopKilenc(rs.getInt("ffiKorcsopKilenc"));
                f.setNoKorcsopKilenc(rs.getInt("noKorcsopKilenc"));
                lista.add(f);
            }
            return lista;
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public List<Huzodzkodas> beolvasHuzodzkodas() throws FafDAOException {
        try {
            List<Huzodzkodas> lista = new ArrayList<>();
            PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM huzodzkodas");
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                Huzodzkodas h = new Huzodzkodas();
                h.setIsmetles(rs.getInt("ismetles"));
                h.setFfiKorcsopEgy(rs.getInt("ffiKorcsopEgy"));
                h.setNoKorcsopEgy(rs.getInt("noKorcsopEgy"));
                h.setFfiKorcsopKetto(rs.getInt("ffiKorcsopKetto"));
                h.setNoKorcsopKetto(rs.getInt("noKorcsopKetto"));
                h.setFfiKorcsopHarom(rs.getInt("ffiKorcsopHarom"));
                h.setNoKorcsopHarom(rs.getInt("noKorcsopHarom"));
                h.setFfiKorcsopNegy(rs.getInt("ffiKorcsopNegy"));
                h.setNoKorcsopNegy(rs.getInt("noKorcsopNegy"));
                h.setFfiKorcsopOt(rs.getInt("ffiKorcsopOt"));
                h.setNoKorcsopOt(rs.getInt("noKorcsopOt"));
                h.setFfiKorcsopHat(rs.getInt("ffiKorcsopHat"));
                h.setNoKorcsopHat(rs.getInt("noKorcsopHat"));
                h.setFfiKorcsopHet(rs.getInt("ffiKorcsopHet"));
                h.setNoKorcsopHet(rs.getInt("noKorcsopHet"));
                h.setFfiKorcsopNyolc(rs.getInt("ffiKorcsopNyolc"));
                h.setNoKorcsopNyolc(rs.getInt("noKorcsopNyolc"));
                h.setFfiKorcsopKilenc(rs.getInt("ffiKorcsopKilenc"));
                h.setNoKorcsopKilenc(rs.getInt("noKorcsopKilenc"));
                lista.add(h);
            }
            return lista;
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public List<HajlitottFugges> beolvasHajlitottFugges() throws FafDAOException {
        try {
            List<HajlitottFugges> lista = new ArrayList<>();
            PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM hajlitottfugges");
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                HajlitottFugges h = new HajlitottFugges();
                h.setIdo(rs.getInt("ido"));
                h.setFfiKorcsopEgy(rs.getInt("ffiKorcsopEgy"));
                h.setNoKorcsopEgy(rs.getInt("noKorcsopEgy"));
                h.setFfiKorcsopKetto(rs.getInt("ffiKorcsopKetto"));
                h.setNoKorcsopKetto(rs.getInt("noKorcsopKetto"));
                h.setFfiKorcsopHarom(rs.getInt("ffiKorcsopHarom"));
                h.setNoKorcsopHarom(rs.getInt("noKorcsopHarom"));
                h.setFfiKorcsopNegy(rs.getInt("ffiKorcsopNegy"));
                h.setNoKorcsopNegy(rs.getInt("noKorcsopNegy"));
                h.setFfiKorcsopOt(rs.getInt("ffiKorcsopOt"));
                h.setNoKorcsopOt(rs.getInt("noKorcsopOt"));
                lista.add(h);
            }
            return lista;
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public <T extends TorzsMozgasformak> List<T> beolvasTorzsMozgasok(int sorszam) throws FafDAOException {
        try {
            List<T> lista = new ArrayList<>();
            PreparedStatement pstmt = null;
            if (sorszam == FELULESCOMBOBOXELEM) {
                pstmt = conn.prepareStatement("SELECT * FROM felules");
            } else if (sorszam == LAPOCKAEMELESCOMBOBOXELEM) {
                pstmt = conn.prepareStatement("SELECT * FROM lapockaemeles");
            } else if (sorszam == FUGGOTERDEMELESCOMBOBOXELEM) {
                pstmt = conn.prepareStatement("SELECT * FROM fuggoterdemeles");
            }
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                TorzsMozgasformak torzs = null;
                if (sorszam == FELULESCOMBOBOXELEM) {
                    torzs = new Felules();
                } else if (sorszam == LAPOCKAEMELESCOMBOBOXELEM) {
                    torzs = new LapockaEmeles();
                } else if (sorszam == FUGGOTERDEMELESCOMBOBOXELEM) {
                    torzs = new FuggoTerdEmeles();
                }
                torzs.setIsmetles(rs.getInt("ismetles"));
                torzs.setFfiKorcsopEgy(rs.getInt("ffiKorcsopEgy"));
                torzs.setNoKorcsopEgy(rs.getInt("noKorcsopEgy"));
                torzs.setFfiKorcsopKetto(rs.getInt("ffiKorcsopKetto"));
                torzs.setNoKorcsopKetto(rs.getInt("noKorcsopKetto"));
                torzs.setFfiKorcsopHarom(rs.getInt("ffiKorcsopHarom"));
                torzs.setNoKorcsopHarom(rs.getInt("noKorcsopHarom"));
                torzs.setFfiKorcsopNegy(rs.getInt("ffiKorcsopNegy"));
                torzs.setNoKorcsopNegy(rs.getInt("noKorcsopNegy"));
                torzs.setFfiKorcsopOt(rs.getInt("ffiKorcsopOt"));
                torzs.setNoKorcsopOt(rs.getInt("noKorcsopOt"));
                torzs.setFfiKorcsopHat(rs.getInt("ffiKorcsopHat"));
                torzs.setNoKorcsopHat(rs.getInt("noKorcsopHat"));
                torzs.setFfiKorcsopHet(rs.getInt("ffiKorcsopHet"));
                torzs.setNoKorcsopHet(rs.getInt("noKorcsopHet"));
                torzs.setFfiKorcsopNyolc(rs.getInt("ffiKorcsopNyolc"));
                torzs.setNoKorcsopNyolc(rs.getInt("noKorcsopNyolc"));
                torzs.setFfiKorcsopKilenc(rs.getInt("ffiKorcsopKilenc"));
                torzs.setNoKorcsopKilenc(rs.getInt("noKorcsopKilenc"));
                lista.add((T) torzs);
            }
            return lista;
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public List<Futas3200> beolvasFutas3200() throws FafDAOException {
        try {
            List<Futas3200> lista = new ArrayList<>();
            PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM futas3200");
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                Futas3200 f = new Futas3200();
                f.setIdo(rs.getInt("ido"));
                f.setFfiKorcsopEgy(rs.getInt("ffiKorcsopEgy"));
                f.setNoKorcsopEgy(rs.getInt("noKorcsopEgy"));
                f.setFfiKorcsopKetto(rs.getInt("ffiKorcsopKetto"));
                f.setNoKorcsopKetto(rs.getInt("noKorcsopKetto"));
                f.setFfiKorcsopHarom(rs.getInt("ffiKorcsopHarom"));
                f.setNoKorcsopHarom(rs.getInt("noKorcsopHarom"));
                f.setFfiKorcsopNegy(rs.getInt("ffiKorcsopNegy"));
                f.setNoKorcsopNegy(rs.getInt("noKorcsopNegy"));
                f.setFfiKorcsopOt(rs.getInt("ffiKorcsopOt"));
                f.setNoKorcsopOt(rs.getInt("noKorcsopOt"));
                lista.add(f);
            }
            return lista;
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public List<Futas2000> beolvasFutas2000() throws FafDAOException {
        try {
            List<Futas2000> lista = new ArrayList<>();
            PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM futas2000");
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                Futas2000 f = new Futas2000();
                f.setIdo(rs.getInt("ido"));
                f.setFfiKorcsopHat(rs.getInt("ffiKorcsopHat"));
                f.setNoKorcsopHat(rs.getInt("noKorcsopHat"));
                f.setFfiKorcsopHet(rs.getInt("ffiKorcsopHet"));
                f.setNoKorcsopHet(rs.getInt("noKorcsopHet"));
                f.setFfiKorcsopNyolc(rs.getInt("ffiKorcsopNyolc"));
                f.setNoKorcsopNyolc(rs.getInt("noKorcsopNyolc"));
                f.setFfiKorcsopKilenc(rs.getInt("ffiKorcsopKilenc"));
                f.setNoKorcsopKilenc(rs.getInt("noKorcsopKilenc"));
                lista.add(f);
            }
            return lista;
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public List<Menet1600> beolvasMenet1600() throws FafDAOException {
        try {
            List<Menet1600> lista = new ArrayList<>();
            PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM menet1600");
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                Menet1600 m = new Menet1600();
                m.setIdo(rs.getInt("ido"));
                m.setFfiKorcsopEgy(rs.getInt("ffiKorcsopEgy"));
                m.setNoKorcsopEgy(rs.getInt("noKorcsopEgy"));
                m.setFfiKorcsopKetto(rs.getInt("ffiKorcsopKetto"));
                m.setNoKorcsopKetto(rs.getInt("noKorcsopKetto"));
                m.setFfiKorcsopHarom(rs.getInt("ffiKorcsopHarom"));
                m.setNoKorcsopHarom(rs.getInt("noKorcsopHarom"));
                m.setFfiKorcsopNegy(rs.getInt("ffiKorcsopNegy"));
                m.setNoKorcsopNegy(rs.getInt("noKorcsopNegy"));
                m.setFfiKorcsopOt(rs.getInt("ffiKorcsopOt"));
                m.setNoKorcsopOt(rs.getInt("noKorcsopOt"));
                m.setFfiKorcsopHat(rs.getInt("ffiKorcsopHat"));
                m.setNoKorcsopHat(rs.getInt("noKorcsopHat"));
                m.setFfiKorcsopHet(rs.getInt("ffiKorcsopHet"));
                m.setNoKorcsopHet(rs.getInt("noKorcsopHet"));
                m.setFfiKorcsopNyolc(rs.getInt("ffiKorcsopNyolc"));
                m.setNoKorcsopNyolc(rs.getInt("noKorcsopNyolc"));
                m.setFfiKorcsopKilenc(rs.getInt("ffiKorcsopKilenc"));
                m.setNoKorcsopKilenc(rs.getInt("noKorcsopKilenc"));
                lista.add(m);
            }
            return lista;
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public List<Ergometria> beolvasErgometria() throws FafDAOException {
        try {
            List<Ergometria> lista = new ArrayList<>();
            PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM ergometria");
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                Ergometria e = new Ergometria();
                e.setWattKg(rs.getInt("wattkg"));
                e.setFfiKorcsopEgy(rs.getInt("ffiKorcsopEgy"));
                e.setNoKorcsopEgy(rs.getInt("noKorcsopEgy"));
                e.setFfiKorcsopKetto(rs.getInt("ffiKorcsopKetto"));
                e.setNoKorcsopKetto(rs.getInt("noKorcsopKetto"));
                e.setFfiKorcsopHarom(rs.getInt("ffiKorcsopHarom"));
                e.setNoKorcsopHarom(rs.getInt("noKorcsopHarom"));
                e.setFfiKorcsopNegy(rs.getInt("ffiKorcsopNegy"));
                e.setNoKorcsopNegy(rs.getInt("noKorcsopNegy"));
                e.setFfiKorcsopOt(rs.getInt("ffiKorcsopOt"));
                e.setNoKorcsopOt(rs.getInt("noKorcsopOt"));
                e.setFfiKorcsopHat(rs.getInt("ffiKorcsopHat"));
                e.setNoKorcsopHat(rs.getInt("noKorcsopHat"));
                e.setFfiKorcsopHet(rs.getInt("ffiKorcsopHet"));
                e.setNoKorcsopHet(rs.getInt("noKorcsopHet"));
                lista.add(e);
            }
            return lista;
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public void insertBMI(BMI b) throws FafDAOException {
        try {
            Statement insertBMI = conn.createStatement();
            StringBuilder oszlopErtekek = new StringBuilder("(");
            oszlopErtekek.append(b.getMagassag()).append(" ,").append(b.getFfiKorcsopEgy()).append(" ,").append(b.getNoKorcsopEgy()).append(" ,")
                    .append(b.getFfiKorcsopKetto()).append(" ,").append(b.getNoKorcsopKetto()).append(" ,").append(b.getFfiKorcsopHarom())
                    .append(" ,").append(b.getNoKorcsopHarom()).append(" ,").append(b.getFfiKorcsopNegy()).append(" ,").append(b.getNoKorcsopNegy())
                    .append(" ,").append(b.getMinSuly()).append(")");
            String sql = "INSERT INTO bmi (magassag, ffiKorcsopEgy, noKorcsopEgy, ffiKorcsopKetto, noKorcsopKetto, ffiKorcsopHarom,"
                    + " noKorcsopHarom, ffiKorcsopNegy, noKorcsopNegy, minimumSuly) VALUES " + oszlopErtekek;
            insertBMI.executeUpdate(sql);
        } catch (Exception ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public <T extends KarMozgasformak> void insertKar(T kar) throws FafDAOException {
        try {
            String tablaNev = null;
            StringBuilder oszlopErtekek = new StringBuilder("(");
            Statement insertKar = conn.createStatement();
            if (kar instanceof HajlitottFugges) {
                tablaNev = "hajlitottfugges";
                oszlopErtekek.append(String.valueOf(((HajlitottFugges) kar).getIdo())).append(" ,");
                oszlopErtekek.append(kar.getFfiKorcsopEgy()).append(" ,").append(kar.getNoKorcsopEgy()).append(" ,").append(kar.getFfiKorcsopKetto())
                        .append(" ,").append(kar.getNoKorcsopKetto()).append(" ,").append(kar.getFfiKorcsopHarom()).append(" ,")
                        .append(kar.getNoKorcsopHarom()).append(" ,").append(kar.getFfiKorcsopNegy()).append(" ,").append(kar.getNoKorcsopNegy())
                        .append(" ,").append(kar.getFfiKorcsopOt()).append(" ,").append(kar.getNoKorcsopOt()).append(")");
                String sql = "INSERT INTO " + tablaNev + " (ido, ffiKorcsopEgy, noKorcsopEgy, ffiKorcsopKetto, noKorcsopKetto, "
                        + "ffiKorcsopHarom, noKorcsopHarom, ffiKorcsopNegy, noKorcsopNegy, ffiKorcsopOt, noKorcsopOt) VALUES " + oszlopErtekek;
                insertKar.executeUpdate(sql);
                return;
            }
            if (kar instanceof Fekvo) {
                tablaNev = "fekvo";
                oszlopErtekek.append(String.valueOf(((Fekvo) kar).getIsmetles())).append(" ,");
            }
            if (kar instanceof Huzodzkodas) {
                tablaNev = "huzodzkodas";
                oszlopErtekek.append(String.valueOf(((Huzodzkodas) kar).getIsmetles())).append(" ,");
            }
            oszlopErtekek.append(kar.getFfiKorcsopEgy()).append(" ,").append(kar.getNoKorcsopEgy()).append(" ,").append(kar.getFfiKorcsopKetto())
                    .append(" ,").append(kar.getNoKorcsopKetto()).append(" ,").append(kar.getFfiKorcsopHarom()).append(" ,")
                    .append(kar.getNoKorcsopHarom()).append(" ,").append(kar.getFfiKorcsopNegy()).append(" ,").append(kar.getNoKorcsopNegy())
                    .append(" ,").append(kar.getFfiKorcsopOt()).append(" ,").append(kar.getNoKorcsopOt()).append(" ,").append(kar.getFfiKorcsopHat())
                    .append(" ,").append(kar.getNoKorcsopHat()).append(" ,").append(kar.getFfiKorcsopHet()).append(" ,").append(kar.getNoKorcsopHet())
                    .append(" ,").append(kar.getFfiKorcsopNyolc()).append(" ,").append(kar.getNoKorcsopNyolc()).append(" ,").append(kar.getFfiKorcsopKilenc())
                    .append(" ,").append(kar.getNoKorcsopKilenc()).append(")");
            String sql = "INSERT INTO " + tablaNev + " (ismetles, ffiKorcsopEgy, noKorcsopEgy, ffiKorcsopKetto, noKorcsopKetto, "
                    + "ffiKorcsopHarom, noKorcsopHarom, ffiKorcsopNegy, noKorcsopNegy, ffiKorcsopOt, noKorcsopOt, ffiKorcsopHat, noKorcsopHat,"
                    + " ffiKorcsopHet, noKorcsopHet, ffiKorcsopNyolc, noKorcsopNyolc, ffiKorcsopKilenc, noKorcsopKilenc)"
                    + " VALUES " + oszlopErtekek;
            insertKar.executeUpdate(sql);
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public <T extends TorzsMozgasformak> void insertTorzs(T torzs) throws FafDAOException {
        try {
            String tablaNev = "";
            StringBuilder oszlopErtekek = new StringBuilder("(");
            if (torzs instanceof Felules) {
                tablaNev = "felules";
            }
            if (torzs instanceof LapockaEmeles) {
                tablaNev = "lapockaemeles";
            }
            if (torzs instanceof FuggoTerdEmeles) {
                tablaNev = "fuggoterdemeles";
            }
            Statement insertTorzs = conn.createStatement();
            oszlopErtekek.append(torzs.getIsmetles()).append(" ,").append(torzs.getFfiKorcsopEgy()).append(" ,").append(torzs.getNoKorcsopEgy()).append(" ,")
                    .append(torzs.getFfiKorcsopKetto()).append(" ,").append(torzs.getNoKorcsopKetto()).append(" ,").append(torzs.getFfiKorcsopHarom()).append(" ,")
                    .append(torzs.getNoKorcsopHarom()).append(" ,").append(torzs.getFfiKorcsopNegy()).append(" ,").append(torzs.getNoKorcsopNegy())
                    .append(" ,").append(torzs.getFfiKorcsopOt()).append(" ,").append(torzs.getNoKorcsopOt()).append(" ,").append(torzs.getFfiKorcsopHat())
                    .append(" ,").append(torzs.getNoKorcsopHat()).append(" ,").append(torzs.getFfiKorcsopHet()).append(" ,").append(torzs.getNoKorcsopHet())
                    .append(" ,").append(torzs.getFfiKorcsopNyolc()).append(" ,").append(torzs.getNoKorcsopNyolc()).append(" ,").append(torzs.getFfiKorcsopKilenc())
                    .append(" ,").append(torzs.getNoKorcsopKilenc()).append(")");
            String sql = "INSERT INTO " + tablaNev + " (ismetles, ffiKorcsopEgy, noKorcsopEgy, ffiKorcsopKetto, noKorcsopKetto, "
                    + "ffiKorcsopHarom, noKorcsopHarom, ffiKorcsopNegy, noKorcsopNegy, ffiKorcsopOt, noKorcsopOt, ffiKorcsopHat, noKorcsopHat,"
                    + " ffiKorcsopHet, noKorcsopHet, ffiKorcsopNyolc, noKorcsopNyolc, ffiKorcsopKilenc, noKorcsopKilenc)"
                    + " VALUES " + oszlopErtekek;
            insertTorzs.executeUpdate(sql);
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public <T extends KeringesMozgasformak> void insertKeringes(T keringes) throws FafDAOException {
        try {
            String tablaNev = "";
            StringBuilder oszlopErtekek = new StringBuilder("(");
            Statement insertKeringes = conn.createStatement();
            if (keringes instanceof Futas3200) {
                tablaNev = "futas3200";
                oszlopErtekek.append(((Futas3200) keringes).getIdo()).append(" ,");
                oszlopErtekek.append(keringes.getFfiKorcsopEgy()).append(" ,").append(keringes.getNoKorcsopEgy()).append(" ,").append(keringes.getFfiKorcsopKetto())
                        .append(" ,").append(keringes.getNoKorcsopKetto()).append(" ,").append(keringes.getFfiKorcsopHarom()).append(" ,").append(keringes.getNoKorcsopHarom())
                        .append(" ,").append(keringes.getFfiKorcsopNegy()).append(" ,").append(keringes.getNoKorcsopNegy()).append(" ,").append(keringes.getFfiKorcsopOt())
                        .append(" ,").append(keringes.getNoKorcsopOt()).append(")");
                String sql = "INSERT INTO " + tablaNev + " (ido, ffiKorcsopEgy, noKorcsopEgy, ffiKorcsopKetto, noKorcsopKetto, "
                        + "ffiKorcsopHarom, noKorcsopHarom, ffiKorcsopNegy, noKorcsopNegy, ffiKorcsopOt, noKorcsopOt) VALUES " + oszlopErtekek;
                insertKeringes.executeUpdate(sql);
                return;
            }
            if (keringes instanceof Futas2000) {
                tablaNev = "futas2000";
                oszlopErtekek.append(((Futas2000) keringes).getIdo()).append(" ,");
                oszlopErtekek.append(keringes.getFfiKorcsopHat()).append(" ,").append(keringes.getNoKorcsopHat()).append(" ,").append(keringes.getFfiKorcsopHet())
                        .append(" ,").append(keringes.getNoKorcsopHet()).append(" ,").append(keringes.getFfiKorcsopNyolc()).append(" ,").append(keringes.getNoKorcsopNyolc())
                        .append(" ,").append(keringes.getFfiKorcsopKilenc()).append(" ,").append(keringes.getNoKorcsopKilenc()).append(")");
                String sql = "INSERT INTO " + tablaNev + " (ido, ffiKorcsopHat, noKorcsopHat, ffiKorcsopHet, noKorcsopHet, ffiKorcsopNyolc, noKorcsopNyolc, "
                        + "ffiKorcsopKilenc, noKorcsopKilenc) VALUES " + oszlopErtekek;
                insertKeringes.executeUpdate(sql);
                return;
            }
            if (keringes instanceof Menet1600) {
                tablaNev = "menet1600";
                oszlopErtekek.append(((Menet1600) keringes).getIdo()).append(" ,");
                oszlopErtekek.append(keringes.getFfiKorcsopEgy()).append(" ,").append(keringes.getNoKorcsopEgy()).append(" ,").append(keringes.getFfiKorcsopKetto())
                        .append(" ,").append(keringes.getNoKorcsopKetto()).append(" ,").append(keringes.getFfiKorcsopHarom()).append(" ,").append(keringes.getNoKorcsopHarom())
                        .append(" ,").append(keringes.getFfiKorcsopNegy()).append(" ,").append(keringes.getNoKorcsopNegy()).append(" ,").append(keringes.getFfiKorcsopOt())
                        .append(" ,").append(keringes.getNoKorcsopOt()).append(" ,").append(keringes.getFfiKorcsopHat()).append(" ,").append(keringes.getNoKorcsopHat())
                        .append(" ,").append(keringes.getFfiKorcsopHet()).append(" ,").append(keringes.getNoKorcsopHet()).append(" ,").append(keringes.getFfiKorcsopNyolc())
                        .append(" ,").append(keringes.getNoKorcsopNyolc()).append(" ,").append(keringes.getFfiKorcsopKilenc()).append(" ,").append(keringes.getNoKorcsopKilenc()).append(")");
                String sql = "INSERT INTO " + tablaNev + " (ido, ffiKorcsopEgy, noKorcsopEgy, ffiKorcsopKetto, noKorcsopKetto, "
                        + "ffiKorcsopHarom, noKorcsopHarom, ffiKorcsopNegy, noKorcsopNegy, ffiKorcsopOt, noKorcsopOt, ffiKorcsopHat, noKorcsopHat,"
                        + " ffiKorcsopHet, noKorcsopHet, ffiKorcsopNyolc, noKorcsopNyolc, ffiKorcsopKilenc, noKorcsopKilenc)"
                        + " VALUES " + oszlopErtekek;
                insertKeringes.executeUpdate(sql);
                return;
            }
            if (keringes instanceof Ergometria) {
                tablaNev = "ergometria";
                oszlopErtekek.append(((Ergometria) keringes).getWattKg()).append(" ,");
                oszlopErtekek.append(keringes.getFfiKorcsopEgy()).append(" ,").append(keringes.getNoKorcsopEgy()).append(" ,").append(keringes.getFfiKorcsopKetto())
                        .append(" ,").append(keringes.getNoKorcsopKetto()).append(" ,").append(keringes.getFfiKorcsopHarom()).append(" ,")
                        .append(keringes.getNoKorcsopHarom()).append(" ,").append(keringes.getFfiKorcsopNegy()).append(" ,").append(keringes.getNoKorcsopNegy())
                        .append(" ,").append(keringes.getFfiKorcsopOt()).append(" ,").append(keringes.getNoKorcsopOt()).append(" ,").append(keringes.getFfiKorcsopHat())
                        .append(" ,").append(keringes.getNoKorcsopHat()).append(" ,").append(keringes.getFfiKorcsopHet()).append(" ,").append(keringes.getNoKorcsopHet())
                        .append(")");
                String sql = "INSERT INTO " + tablaNev + " (wattKg, ffiKorcsopEgy, noKorcsopEgy, ffiKorcsopKetto, noKorcsopKetto,ffiKorcsopHarom, "
                        + "noKorcsopHarom, ffiKorcsopNegy, noKorcsopNegy, ffiKorcsopOt, noKorcsopOt, ffiKorcsopHat, noKorcsopHat,"
                        + " ffiKorcsopHet, noKorcsopHet) VALUES " + oszlopErtekek;
                insertKeringes.executeUpdate(sql);
            }
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public <T extends KarMozgasformak> void updateKar(T kar) throws FafDAOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T extends TorzsMozgasformak> void updateTorzs(T torzs) throws FafDAOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T extends KeringesMozgasformak> void updateKeringes(T keringes) throws FafDAOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T extends KarMozgasformak> void deleteKar(T kar) throws FafDAOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T extends TorzsMozgasformak> void deleteTorzs(T torzs) throws FafDAOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T extends KeringesMozgasformak> void deleteKeringes(T keringes) throws FafDAOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
