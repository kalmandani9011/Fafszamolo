package fafszamolo.mozgasformak.dao.impl;

import fafszamolo.model.BMI;
import fafszamolo.model.Ergometria;
import fafszamolo.model.Fekvo;
import fafszamolo.model.Felules;
import fafszamolo.model.FuggoTerdEmeles;
import fafszamolo.model.Futas2000;
import fafszamolo.model.Futas3200;
import fafszamolo.model.HajlitottFugges;
import fafszamolo.model.Huzodzkodas;
import fafszamolo.model.KarMozgasformak;
import fafszamolo.model.KeringesMozgasformak;
import fafszamolo.model.LapockaEmeles;
import fafszamolo.model.Menet1600;
import fafszamolo.model.TorzsMozgasformak;
import fafszamolo.exception.FafDAOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import fafszamolo.mozgasformak.dao.MozgasformakRepository;

/**
 * Az alapadatokat tartalmazó .csv fájlok elérésére való osztály.
 */
public class MozgasformakRepositoryCSVImpl implements MozgasformakRepository {

    private String utvonal;

    /**
     * Normál esetben ezzel a konstruktorral hozzunk létre az objektumot, az
     * alapadatok így elérhetőek.
     */
    public MozgasformakRepositoryCSVImpl() {
        this.utvonal = "src/main/java/fafszamolo/csv/";
    }

    /**
     * Ezzel a konstruktorral beállítható az elérési útvonal az alapadatokhoz.
     * Ezzel lehetséges a tesztelés is akár.
     */
    public MozgasformakRepositoryCSVImpl(String utvonal) {
        this.utvonal = utvonal;
    }

    @Override
    public int findPontszam(String tabla, int eredmeny, String korcsoport) throws FafDAOException {
        switch (tabla) {
            case "fekvo":
                for (Fekvo f : beolvasFekvo()) {
                    if (f.getIsmetles() == eredmeny) {
                        return f.getKorcsoport(korcsoport);
                    }
                }
                break;
            case "huzodzkodas":
                for (Huzodzkodas h : beolvasHuzodzkodas()) {
                    if (h.getIsmetles() == eredmeny) {
                        return h.getKorcsoport(korcsoport);
                    }
                }
                break;
            case "hajlitottFugges":
                for (HajlitottFugges h : beolvasHajlitottFugges()) {
                    if (h.getIdo() == eredmeny) {
                        return h.getKorcsoport(korcsoport);
                    }
                }
                break;
            case "felules":
                for (TorzsMozgasformak t : beolvasTorzsMozgasok(0)) {
                    if (t.getIsmetles() == eredmeny) {
                        return t.getKorcsoport(korcsoport);
                    }
                }
                break;
            case "lapockaEmeles":
                for (TorzsMozgasformak t : beolvasTorzsMozgasok(1)) {
                    if (t.getIsmetles() == eredmeny) {
                        return t.getKorcsoport(korcsoport);
                    }
                }
                break;
            case "fuggoTerdEmeles":
                for (TorzsMozgasformak t : beolvasTorzsMozgasok(2)) {
                    if (t.getIsmetles() == eredmeny) {
                        return t.getKorcsoport(korcsoport);
                    }
                }
                break;
            case "futas3200":
                for (Futas3200 f : beolvasFutas3200()) {
                    if (f.getIdo() == eredmeny) {
                        return f.getKorcsoport(korcsoport);
                    }
                }
                break;
            case "futas2000":
                for (Futas2000 f : beolvasFutas2000()) {
                    if (f.getIdo() == eredmeny) {
                        return f.getKorcsoport(korcsoport);
                    }
                }
                break;
            case "menet1600":
                for (Menet1600 m : beolvasMenet1600()) {
                    if (m.getIdo() == eredmeny) {
                        return m.getKorcsoport(korcsoport);
                    }
                }
                break;
            case "ergometria":
                for (Ergometria e : beolvasErgometria()) {
                    if (e.getWattKg() == eredmeny) {
                        return e.getKorcsoport(korcsoport);
                    }
                }
                break;
        }
        return -1;
    }

    @Override
    public int[] findBMIPontszam(int magassag, String korcsoport) throws FafDAOException {
        for (BMI b : beolvasBMI()) {
            if (b.getMagassag() == magassag) {
                return new int[]{b.getKorcsoport(korcsoport), b.getMinSuly()};
            }
        }
        return new int[0];
    }

    @Override
    public List<BMI> beolvasBMI() throws FafDAOException {
        List<BMI> lista = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(utvonal + "bmi.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (!line.contains("#")) {
                    if (!line.contains(";")) {
                        throw new FafDAOException("Hiba a fájl olvasása közben: túl kevés adat, vagy nem megfelelő szeparátor.");
                    }
                    String[] tomb = line.split(";");
                    int magassag = Integer.parseInt(tomb[0]);
                    int ffiKorcsopEgy = Integer.parseInt(tomb[1]);
                    int noKorcsopEgy = Integer.parseInt(tomb[2]);
                    int ffiKorcsopKetto = Integer.parseInt(tomb[3]);
                    int noKorcsopKetto = Integer.parseInt(tomb[4]);
                    int ffiKorcsopHarom = Integer.parseInt(tomb[5]);
                    int noKorcsopHarom = Integer.parseInt(tomb[6]);
                    int ffiKorcsopNegy = Integer.parseInt(tomb[7]);
                    int noKorcsopNegy = Integer.parseInt(tomb[8]);
                    int minSuly = Integer.parseInt(tomb[9]);
                    lista.add(new BMI(magassag, ffiKorcsopEgy, noKorcsopEgy, ffiKorcsopKetto, noKorcsopKetto, ffiKorcsopHarom, noKorcsopHarom,
                            ffiKorcsopNegy, noKorcsopNegy, minSuly));
                }
            }
            return lista;
        } catch (IOException ex) {
            throw new FafDAOException("A keresett fájl nem található.");
        }
    }

    @Override
    public List<Fekvo> beolvasFekvo() throws FafDAOException {
        List<Fekvo> lista = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(utvonal + "fekvo.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (!line.contains("#")) {
                    if (!line.contains(";")) {
                        throw new FafDAOException("Hiba a fájl olvasása közben: túl kevés adat, vagy nem megfelelő szeparátor.");
                    }
                    String[] tomb = line.split(";");
                    int ismetles = Integer.parseInt(tomb[0]);
                    int ffiKorcsopEgy = Integer.parseInt(tomb[1]);
                    int noKorcsopEgy = Integer.parseInt(tomb[2]);
                    int ffiKorcsopKetto = Integer.parseInt(tomb[3]);
                    int noKorcsopKetto = Integer.parseInt(tomb[4]);
                    int ffiKorcsopHarom = Integer.parseInt(tomb[5]);
                    int noKorcsopHarom = Integer.parseInt(tomb[6]);
                    int ffiKorcsopNegy = Integer.parseInt(tomb[7]);
                    int noKorcsopNegy = Integer.parseInt(tomb[8]);
                    int ffiKorcsopOt = Integer.parseInt(tomb[9]);
                    int noKorcsopOt = Integer.parseInt(tomb[10]);
                    int ffiKorcsopHat = Integer.parseInt(tomb[11]);
                    int noKorcsopHat = Integer.parseInt(tomb[12]);
                    int ffiKorcsopHet = Integer.parseInt(tomb[13]);
                    int noKorcsopHet = Integer.parseInt(tomb[14]);
                    int ffiKorcsopNyolc = Integer.parseInt(tomb[15]);
                    int noKorcsopNyolc = Integer.parseInt(tomb[16]);
                    int ffiKorcsopKilenc = Integer.parseInt(tomb[17]);
                    int noKorcsopKilenc = Integer.parseInt(tomb[18]);
                    lista.add(new Fekvo(ismetles, ffiKorcsopEgy, noKorcsopEgy, ffiKorcsopKetto, noKorcsopKetto, ffiKorcsopHarom, noKorcsopHarom,
                            ffiKorcsopNegy, noKorcsopNegy, ffiKorcsopOt, noKorcsopOt, ffiKorcsopHat, noKorcsopHat, ffiKorcsopHet, noKorcsopHet, ffiKorcsopNyolc,
                            noKorcsopNyolc, ffiKorcsopKilenc, noKorcsopKilenc));
                }
            }
            return lista;
        } catch (IOException ex) {
            throw new FafDAOException("A keresett fájl nem található.");
        }
    }

    @Override
    public List<Huzodzkodas> beolvasHuzodzkodas() throws FafDAOException {
        List<Huzodzkodas> lista = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(utvonal + "huzodzkodas.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (!line.contains("#")) {
                    if (!line.contains(";")) {
                        throw new FafDAOException("Hiba a fájl olvasása közben: túl kevés adat, vagy nem megfelelő szeparátor.");
                    }
                    String[] tomb = line.split(";");
                    int ismetles = Integer.parseInt(tomb[0]);
                    int ffiKorcsopEgy = Integer.parseInt(tomb[1]);
                    int noKorcsopEgy = Integer.parseInt(tomb[2]);
                    int ffiKorcsopKetto = Integer.parseInt(tomb[3]);
                    int noKorcsopKetto = Integer.parseInt(tomb[4]);
                    int ffiKorcsopHarom = Integer.parseInt(tomb[5]);
                    int noKorcsopHarom = Integer.parseInt(tomb[6]);
                    int ffiKorcsopNegy = Integer.parseInt(tomb[7]);
                    int noKorcsopNegy = Integer.parseInt(tomb[8]);
                    int ffiKorcsopOt = Integer.parseInt(tomb[9]);
                    int noKorcsopOt = Integer.parseInt(tomb[10]);
                    int ffiKorcsopHat = Integer.parseInt(tomb[11]);
                    int noKorcsopHat = Integer.parseInt(tomb[12]);
                    int ffiKorcsopHet = Integer.parseInt(tomb[13]);
                    int noKorcsopHet = Integer.parseInt(tomb[14]);
                    int ffiKorcsopNyolc = Integer.parseInt(tomb[15]);
                    int noKorcsopNyolc = Integer.parseInt(tomb[16]);
                    int ffiKorcsopKilenc = Integer.parseInt(tomb[17]);
                    int noKorcsopKilenc = Integer.parseInt(tomb[18]);
                    lista.add(new Huzodzkodas(ismetles, ffiKorcsopEgy, noKorcsopEgy, ffiKorcsopKetto, noKorcsopKetto, ffiKorcsopHarom, noKorcsopHarom,
                            ffiKorcsopNegy, noKorcsopNegy, ffiKorcsopOt, noKorcsopOt, ffiKorcsopHat, noKorcsopHat, ffiKorcsopHet, noKorcsopHet, ffiKorcsopNyolc,
                            noKorcsopNyolc, ffiKorcsopKilenc, noKorcsopKilenc));
                }
            }
            return lista;
        } catch (IOException ex) {
            throw new FafDAOException("A keresett fájl nem található.");
        }
    }

    @Override
    public List<HajlitottFugges> beolvasHajlitottFugges() throws FafDAOException {
        List<HajlitottFugges> lista = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(utvonal + "hajlitottfugges.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (!line.contains("#")) {
                    if (!line.contains(";")) {
                        throw new FafDAOException("Hiba a fájl olvasása közben: túl kevés adat, vagy nem megfelelő szeparátor.");
                    }
                    String[] tomb = line.split(";");
                    int ido = Integer.parseInt(tomb[0]);
                    int ffiKorcsopEgy = Integer.parseInt(tomb[1]);
                    int noKorcsopEgy = Integer.parseInt(tomb[2]);
                    int ffiKorcsopKetto = Integer.parseInt(tomb[3]);
                    int noKorcsopKetto = Integer.parseInt(tomb[4]);
                    int ffiKorcsopHarom = Integer.parseInt(tomb[5]);
                    int noKorcsopHarom = Integer.parseInt(tomb[6]);
                    int ffiKorcsopNegy = Integer.parseInt(tomb[7]);
                    int noKorcsopNegy = Integer.parseInt(tomb[8]);
                    int ffiKorcsopOt = Integer.parseInt(tomb[9]);
                    int noKorcsopOt = Integer.parseInt(tomb[10]);
                    lista.add(new HajlitottFugges(ido, ffiKorcsopEgy, noKorcsopEgy, ffiKorcsopKetto, noKorcsopKetto, ffiKorcsopHarom, noKorcsopHarom,
                            ffiKorcsopNegy, noKorcsopNegy, ffiKorcsopOt, noKorcsopOt));
                }
            }
            return lista;
        } catch (IOException ex) {
            throw new FafDAOException("A keresett fájl nem található.");
        }
    }

    @Override
    public <T extends TorzsMozgasformak> List<T> beolvasTorzsMozgasok(int sorszam) throws FafDAOException {
        try {
            List<T> lista = new ArrayList<>();
            FileReader fr = null;
            TorzsMozgasformak torzs = null;
            switch (sorszam) {
                case 0:
                    fr = new FileReader(utvonal + "felules.csv");
                    torzs = new Felules();
                    break;
                case 1:
                    fr = new FileReader(utvonal + "lapockaemeles.csv");
                    torzs = new LapockaEmeles();
                    break;
                case 2:
                    fr = new FileReader(utvonal + "fuggoterdemeles.csv");
                    torzs = new FuggoTerdEmeles();
                    break;
            }
            try (BufferedReader br = new BufferedReader(fr)) {
                String line;
                while ((line = br.readLine()) != null) {
                    if (!line.contains("#")) {
                        if (!line.contains(";")) {
                            throw new FafDAOException("Hiba a fájl olvasása közben: túl kevés adat, vagy nem megfelelő szeparátor.");
                        }
                        String[] tomb = line.split(";");
                        torzs.setIsmetles(Integer.parseInt(tomb[0]));
                        torzs.setFfiKorcsopEgy(Integer.parseInt(tomb[1]));
                        torzs.setNoKorcsopEgy(Integer.parseInt(tomb[2]));
                        torzs.setFfiKorcsopKetto(Integer.parseInt(tomb[3]));
                        torzs.setNoKorcsopKetto(Integer.parseInt(tomb[4]));
                        torzs.setFfiKorcsopHarom(Integer.parseInt(tomb[5]));
                        torzs.setNoKorcsopHarom(Integer.parseInt(tomb[6]));
                        torzs.setFfiKorcsopNegy(Integer.parseInt(tomb[7]));
                        torzs.setNoKorcsopNegy(Integer.parseInt(tomb[8]));
                        torzs.setFfiKorcsopOt(Integer.parseInt(tomb[9]));
                        torzs.setNoKorcsopOt(Integer.parseInt(tomb[10]));
                        torzs.setFfiKorcsopHat(Integer.parseInt(tomb[11]));
                        torzs.setNoKorcsopHat(Integer.parseInt(tomb[12]));
                        torzs.setFfiKorcsopHet(Integer.parseInt(tomb[13]));
                        torzs.setNoKorcsopHet(Integer.parseInt(tomb[14]));
                        torzs.setFfiKorcsopNyolc(Integer.parseInt(tomb[15]));
                        torzs.setNoKorcsopNyolc(Integer.parseInt(tomb[16]));
                        torzs.setFfiKorcsopKilenc(Integer.parseInt(tomb[17]));
                        torzs.setNoKorcsopKilenc(Integer.parseInt(tomb[18]));
                        lista.add((T) torzs);
                    }
                }
                return (List<T>) lista;
            } catch (IOException ex) {
                throw new FafDAOException("A keresett fájl nem található.");
            }
        } catch (IOException ex) {
            throw new FafDAOException("A keresett fájl nem található.");
        }
    }

    @Override
    public List<Futas3200> beolvasFutas3200() throws FafDAOException {
        List<Futas3200> lista = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(utvonal + "futas3200.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (!line.contains("#")) {
                    if (!line.contains(";")) {
                        throw new FafDAOException("Hiba a fájl olvasása közben: túl kevés adat, vagy nem megfelelő szeparátor.");
                    }
                    String[] tomb = line.split(";");
                    int ido = Integer.parseInt(tomb[0]);
                    int ffiKorcsopEgy = Integer.parseInt(tomb[1]);
                    int noKorcsopEgy = Integer.parseInt(tomb[2]);
                    int ffiKorcsopKetto = Integer.parseInt(tomb[3]);
                    int noKorcsopKetto = Integer.parseInt(tomb[4]);
                    int ffiKorcsopHarom = Integer.parseInt(tomb[5]);
                    int noKorcsopHarom = Integer.parseInt(tomb[6]);
                    int ffiKorcsopNegy = Integer.parseInt(tomb[7]);
                    int noKorcsopNegy = Integer.parseInt(tomb[8]);
                    int ffiKorcsopOt = Integer.parseInt(tomb[9]);
                    int noKorcsopOt = Integer.parseInt(tomb[10]);
                    lista.add(new Futas3200(ido, ffiKorcsopEgy, noKorcsopEgy, ffiKorcsopKetto, noKorcsopKetto, ffiKorcsopHarom, noKorcsopHarom,
                            ffiKorcsopNegy, noKorcsopNegy, ffiKorcsopOt, noKorcsopOt));
                }
            }
            return lista;
        } catch (IOException ex) {
            throw new FafDAOException("A keresett fájl nem található.");
        }
    }

    @Override
    public List<Futas2000> beolvasFutas2000() throws FafDAOException {
        List<Futas2000> lista = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(utvonal + "futas2000.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (!line.contains("#")) {
                    if (!line.contains(";")) {
                        throw new FafDAOException("Hiba a fájl olvasása közben: túl kevés adat, vagy nem megfelelő szeparátor.");
                    }
                    String[] tomb = line.split(";");
                    int ido = Integer.parseInt(tomb[0]);
                    int ffiKorcsopHat = Integer.parseInt(tomb[1]);
                    int noKorcsopHat = Integer.parseInt(tomb[2]);
                    int ffiKorcsopHet = Integer.parseInt(tomb[3]);
                    int noKorcsopHet = Integer.parseInt(tomb[4]);
                    int ffiKorcsopNyolc = Integer.parseInt(tomb[5]);
                    int noKorcsopNyolc = Integer.parseInt(tomb[6]);
                    int ffiKorcsopKilenc = Integer.parseInt(tomb[7]);
                    int noKorcsopKilenc = Integer.parseInt(tomb[8]);
                    lista.add(new Futas2000(ido, ffiKorcsopHat, noKorcsopHat, ffiKorcsopHet, noKorcsopHet, ffiKorcsopNyolc, noKorcsopNyolc,
                            ffiKorcsopKilenc, noKorcsopKilenc));
                }
            }
            return lista;
        } catch (IOException ex) {
            throw new FafDAOException("A keresett fájl nem található.");
        }
    }

    @Override
    public List<Menet1600> beolvasMenet1600() throws FafDAOException {
        List<Menet1600> lista = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(utvonal + "menet1600.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (!line.contains("#")) {
                    if (!line.contains(";")) {
                        throw new FafDAOException("Hiba a fájl olvasása közben: túl kevés adat, vagy nem megfelelő szeparátor.");
                    }
                    String[] tomb = line.split(";");
                    int ido = Integer.parseInt(tomb[0]);
                    int ffiKorcsopEgy = Integer.parseInt(tomb[1]);
                    int noKorcsopEgy = Integer.parseInt(tomb[2]);
                    int ffiKorcsopKetto = Integer.parseInt(tomb[3]);
                    int noKorcsopKetto = Integer.parseInt(tomb[4]);
                    int ffiKorcsopHarom = Integer.parseInt(tomb[5]);
                    int noKorcsopHarom = Integer.parseInt(tomb[6]);
                    int ffiKorcsopNegy = Integer.parseInt(tomb[7]);
                    int noKorcsopNegy = Integer.parseInt(tomb[8]);
                    int ffiKorcsopOt = Integer.parseInt(tomb[9]);
                    int noKorcsopOt = Integer.parseInt(tomb[10]);
                    int ffiKorcsopHat = Integer.parseInt(tomb[11]);
                    int noKorcsopHat = Integer.parseInt(tomb[12]);
                    int ffiKorcsopHet = Integer.parseInt(tomb[13]);
                    int noKorcsopHet = Integer.parseInt(tomb[14]);
                    int ffiKorcsopNyolc = Integer.parseInt(tomb[15]);
                    int noKorcsopNyolc = Integer.parseInt(tomb[16]);
                    int ffiKorcsopKilenc = Integer.parseInt(tomb[17]);
                    int noKorcsopKilenc = Integer.parseInt(tomb[18]);
                    lista.add(new Menet1600(ido, ffiKorcsopEgy, noKorcsopEgy, ffiKorcsopKetto, noKorcsopKetto, ffiKorcsopHarom, noKorcsopHarom,
                            ffiKorcsopNegy, noKorcsopNegy, ffiKorcsopOt, noKorcsopOt, ffiKorcsopHat, noKorcsopHat, ffiKorcsopHet, noKorcsopHet, ffiKorcsopNyolc,
                            noKorcsopNyolc, ffiKorcsopKilenc, noKorcsopKilenc));
                }
            }
            return lista;
        } catch (IOException ex) {
            throw new FafDAOException("A keresett fájl nem található.");
        }
    }

    @Override
    public List<Ergometria> beolvasErgometria() throws FafDAOException {
        List<Ergometria> lista = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(utvonal + "ergometria.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (!line.contains("#")) {
                    if (!line.contains(";")) {
                        throw new FafDAOException("Hiba a fájl olvasása közben: túl kevés adat, vagy nem megfelelő szeparátor.");
                    }
                    String[] tomb = line.split(";");
                    int wattKg = Integer.parseInt(tomb[0]);
                    int ffiKorcsopEgy = Integer.parseInt(tomb[1]);
                    int noKorcsopEgy = Integer.parseInt(tomb[2]);
                    int ffiKorcsopKetto = Integer.parseInt(tomb[3]);
                    int noKorcsopKetto = Integer.parseInt(tomb[4]);
                    int ffiKorcsopHarom = Integer.parseInt(tomb[5]);
                    int noKorcsopHarom = Integer.parseInt(tomb[6]);
                    int ffiKorcsopNegy = Integer.parseInt(tomb[7]);
                    int noKorcsopNegy = Integer.parseInt(tomb[8]);
                    int ffiKorcsopOt = Integer.parseInt(tomb[9]);
                    int noKorcsopOt = Integer.parseInt(tomb[10]);
                    int ffiKorcsopHat = Integer.parseInt(tomb[11]);
                    int noKorcsopHat = Integer.parseInt(tomb[12]);
                    int ffiKorcsopHet = Integer.parseInt(tomb[13]);
                    int noKorcsopHet = Integer.parseInt(tomb[14]);
                    lista.add(new Ergometria(wattKg, ffiKorcsopEgy, noKorcsopEgy, ffiKorcsopKetto, noKorcsopKetto, ffiKorcsopHarom, noKorcsopHarom,
                            ffiKorcsopNegy, noKorcsopNegy, ffiKorcsopOt, noKorcsopOt, ffiKorcsopHat, noKorcsopHat, ffiKorcsopHet, noKorcsopHet));
                }
            }
            return lista;
        } catch (IOException ex) {
            throw new FafDAOException("A keresett fájl nem található.");
        }
    }

    @Override
    public void insertBMI(BMI b) throws FafDAOException {
        try (PrintWriter pw = new PrintWriter(new FileWriter(utvonal + "bmi.csv", true))) {
            pw.println(b.getMagassag() + ";" + b.getFfiKorcsopEgy() + ";" + b.getNoKorcsopEgy() + ";" + b.getFfiKorcsopKetto() + ";" + b.getNoKorcsopKetto()
                    + ";" + b.getFfiKorcsopHarom() + ";" + b.getNoKorcsopHarom() + ";" + b.getFfiKorcsopNegy() + ";" + b.getNoKorcsopNegy() + ";" + b.getMinSuly());
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public <T extends KarMozgasformak> void insertKar(T kar) throws FafDAOException {
        FileWriter fw = null;
        StringBuilder sor = new StringBuilder("");
        try {
            if (kar instanceof Fekvo) {
                fw = new FileWriter(utvonal + "fekvo.csv", true);
                sor.append(((Fekvo) kar).getIsmetles()).append(";");
            }
            if (kar instanceof Huzodzkodas) {
                fw = new FileWriter(utvonal + "huzodzkodas.csv", true);
                sor.append(((Huzodzkodas) kar).getIsmetles()).append(";");
            }
            if (kar instanceof HajlitottFugges) {
                fw = new FileWriter(utvonal + "hajlitottfugges.csv", true);
                sor.append(((HajlitottFugges) kar).getIdo()).append(";");
            }
            sor.append(kar.getFfiKorcsopEgy()).append(";").append(kar.getNoKorcsopEgy()).append(";").append(kar.getFfiKorcsopKetto()).append(";")
                    .append(kar.getNoKorcsopKetto()).append(";").append(kar.getFfiKorcsopHarom()).append(";").append(kar.getNoKorcsopHarom()).append(";")
                    .append(kar.getFfiKorcsopNegy()).append(";").append(kar.getNoKorcsopNegy()).append(";").append(kar.getFfiKorcsopOt()).append(";")
                    .append(kar.getNoKorcsopOt()).append(";").append(kar.getFfiKorcsopHat()).append(";").append(kar.getNoKorcsopHat()).append(";")
                    .append(kar.getFfiKorcsopHet()).append(";").append(kar.getNoKorcsopHet()).append(";").append(kar.getFfiKorcsopNyolc()).append(";")
                    .append(kar.getNoKorcsopNyolc()).append(";").append(kar.getFfiKorcsopKilenc()).append(";").append(kar.getNoKorcsopKilenc());
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
        try (PrintWriter pw = new PrintWriter(fw)) {
            pw.println(sor);
        }
    }

    @Override
    public <T extends TorzsMozgasformak> void insertTorzs(T torzs) throws FafDAOException {
        FileWriter fw = null;
        StringBuilder sor = new StringBuilder("");
        try {
            if (torzs instanceof Felules) {
                fw = new FileWriter(utvonal + "felules.csv", true);
            }
            if (torzs instanceof LapockaEmeles) {
                fw = new FileWriter(utvonal + "lapockaemeles.csv", true);
            }
            if (torzs instanceof FuggoTerdEmeles) {
                fw = new FileWriter(utvonal + "fuggoterdemeles.csv", true);
            }
            sor.append(torzs.getIsmetles()).append(";").append(torzs.getFfiKorcsopEgy()).append(";").append(torzs.getNoKorcsopEgy()).append(";").append(torzs.getFfiKorcsopKetto()).append(";")
                    .append(torzs.getNoKorcsopKetto()).append(";").append(torzs.getFfiKorcsopHarom()).append(";").append(torzs.getNoKorcsopHarom()).append(";")
                    .append(torzs.getFfiKorcsopNegy()).append(";").append(torzs.getNoKorcsopNegy()).append(";").append(torzs.getFfiKorcsopOt()).append(";")
                    .append(torzs.getNoKorcsopOt()).append(";").append(torzs.getFfiKorcsopHat()).append(";").append(torzs.getNoKorcsopHat()).append(";")
                    .append(torzs.getFfiKorcsopHet()).append(";").append(torzs.getNoKorcsopHet()).append(";").append(torzs.getFfiKorcsopNyolc()).append(";")
                    .append(torzs.getNoKorcsopNyolc()).append(";").append(torzs.getFfiKorcsopKilenc()).append(";").append(torzs.getNoKorcsopKilenc());
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
        try (PrintWriter pw = new PrintWriter(fw)) {
            pw.println(sor);
        }
    }

    @Override
    public <T extends KeringesMozgasformak> void insertKeringes(T keringes) throws FafDAOException {
        FileWriter fw = null;
        StringBuilder sor = new StringBuilder("");
        try {
            if (keringes instanceof Futas3200) {
                fw = new FileWriter(utvonal + "futas3200.csv", true);
                sor.append(((Futas3200) keringes).getIdo()).append(";").append(keringes.getFfiKorcsopEgy()).append(";").append(keringes.getNoKorcsopEgy())
                        .append(";").append(keringes.getFfiKorcsopKetto()).append(";").append(keringes.getNoKorcsopKetto()).append(";").append(keringes.getFfiKorcsopHarom())
                        .append(";").append(keringes.getNoKorcsopHarom()).append(";").append(keringes.getFfiKorcsopNegy()).append(";").append(keringes.getNoKorcsopNegy())
                        .append(";").append(keringes.getFfiKorcsopOt()).append(";").append(keringes.getNoKorcsopOt());
            }
            if (keringes instanceof Futas2000) {
                fw = new FileWriter(utvonal + "futas2000.csv", true);
                sor.append(((Futas2000) keringes).getIdo()).append(";").append(keringes.getFfiKorcsopHat()).append(";").append(keringes.getNoKorcsopHat()).append(";")
                        .append(keringes.getFfiKorcsopHet()).append(";").append(keringes.getNoKorcsopHet()).append(";").append(keringes.getFfiKorcsopNyolc()).append(";")
                        .append(keringes.getNoKorcsopNyolc()).append(";").append(keringes.getFfiKorcsopKilenc()).append(";").append(keringes.getNoKorcsopKilenc());
            }
            if (keringes instanceof Menet1600) {
                fw = new FileWriter(utvonal + "menet1600.csv", true);
                sor.append(((Menet1600) keringes).getIdo()).append(";").append(keringes.getFfiKorcsopEgy()).append(";").append(keringes.getNoKorcsopEgy()).append(";")
                        .append(keringes.getFfiKorcsopKetto()).append(";").append(keringes.getNoKorcsopKetto()).append(";").append(keringes.getFfiKorcsopHarom()).append(";")
                        .append(keringes.getNoKorcsopHarom()).append(";").append(keringes.getFfiKorcsopNegy()).append(";").append(keringes.getNoKorcsopNegy()).append(";")
                        .append(keringes.getFfiKorcsopOt()).append(";").append(keringes.getNoKorcsopOt()).append(";").append(keringes.getFfiKorcsopHat()).append(";")
                        .append(keringes.getNoKorcsopHat()).append(";").append(keringes.getFfiKorcsopHet()).append(";").append(keringes.getNoKorcsopHet()).append(";")
                        .append(keringes.getFfiKorcsopNyolc()).append(";").append(keringes.getNoKorcsopNyolc()).append(";").append(keringes.getFfiKorcsopKilenc()).append(";")
                        .append(keringes.getNoKorcsopKilenc());
            }
            if (keringes instanceof Ergometria) {
                fw = new FileWriter(utvonal + "ergometria.csv", true);
                sor.append(((Ergometria) keringes).getWattKg()).append(";").append(keringes.getFfiKorcsopEgy()).append(";").append(keringes.getNoKorcsopEgy())
                        .append(";").append(keringes.getFfiKorcsopKetto()).append(";").append(keringes.getNoKorcsopKetto()).append(";").append(keringes.getFfiKorcsopHarom())
                        .append(";").append(keringes.getNoKorcsopHarom()).append(";").append(keringes.getFfiKorcsopNegy()).append(";").append(keringes.getNoKorcsopNegy())
                        .append(";").append(keringes.getFfiKorcsopOt()).append(";").append(keringes.getNoKorcsopOt()).append(";").append(keringes.getFfiKorcsopHat())
                        .append(";").append(keringes.getNoKorcsopHat()).append(";").append(keringes.getFfiKorcsopHet()).append(";").append(keringes.getNoKorcsopHet());
            }
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
        try (PrintWriter pw = new PrintWriter(fw)) {
            pw.println(sor);
        }
    }

    @Override
    public <T extends KarMozgasformak> void updateKar(T kar) throws FafDAOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T extends TorzsMozgasformak> void updateTorzs(T torzs) throws FafDAOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T extends KeringesMozgasformak> void updateKeringes(T keringes) throws FafDAOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T extends KarMozgasformak> void deleteKar(T kar) throws FafDAOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T extends TorzsMozgasformak> void deleteTorzs(T torzs) throws FafDAOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T extends KeringesMozgasformak> void deleteKeringes(T keringes) throws FafDAOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
