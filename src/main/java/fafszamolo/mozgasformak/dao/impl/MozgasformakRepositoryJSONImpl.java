package fafszamolo.mozgasformak.dao.impl;

import fafszamolo.model.BMI;
import fafszamolo.model.Ergometria;
import fafszamolo.model.Fekvo;
import fafszamolo.model.Felules;
import fafszamolo.model.FuggoTerdEmeles;
import fafszamolo.model.Futas2000;
import fafszamolo.model.Futas3200;
import fafszamolo.model.HajlitottFugges;
import fafszamolo.model.Huzodzkodas;
import fafszamolo.model.KarMozgasformak;
import fafszamolo.model.KeringesMozgasformak;
import fafszamolo.model.LapockaEmeles;
import fafszamolo.model.Menet1600;
import fafszamolo.model.TorzsMozgasformak;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import fafszamolo.exception.FafDAOException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import fafszamolo.mozgasformak.dao.MozgasformakRepository;

/**
 * Az alapadatokat tartalmazó .json fájlok elérésére való osztály.
 */
public class MozgasformakRepositoryJSONImpl implements MozgasformakRepository {
    
    private String utvonal;
    
    /**
     * Normál esetben ezzel a konstruktorral hozzunk létre az objektumot, az alapadatok így elérhetőek.
     */
    public MozgasformakRepositoryJSONImpl() {
        this.utvonal = "src/fafszamolo/json/";
    }
    
    /**
     * Ezzel a konstruktorral beállítható az elérési útvonal az alapadatokhoz.
     */
    public MozgasformakRepositoryJSONImpl(String utVonal) {
        this.utvonal = utVonal;
    }
    
    @Override
    public int findPontszam(String tabla, int eredmeny, String korcsoport) throws FafDAOException {
        switch (tabla) {
            case "fekvo":
                for (Fekvo f : beolvasFekvo()) {
                    if (f.getIsmetles() == eredmeny) {
                        return f.getKorcsoport(korcsoport);
                    }
                }
                break;
            case "huzodzkodas":
                for (Huzodzkodas h : beolvasHuzodzkodas()) {
                    if (h.getIsmetles() == eredmeny) {
                        return h.getKorcsoport(korcsoport);
                    }
                }
                break;
            case "hajlitottFugges":
                for (HajlitottFugges h : beolvasHajlitottFugges()) {
                    if (h.getIdo() == eredmeny) {
                        return h.getKorcsoport(korcsoport);
                    }
                }
                break;
            case "felules":
                for (TorzsMozgasformak t : beolvasTorzsMozgasok(0)) {
                    if (t.getIsmetles() == eredmeny) {
                        return t.getKorcsoport(korcsoport);
                    }
                }
                break;
            case "lapockaEmeles":
                for (TorzsMozgasformak t : beolvasTorzsMozgasok(1)) {
                    if (t.getIsmetles() == eredmeny) {
                        return t.getKorcsoport(korcsoport);
                    }
                }
                break;
            case "fuggoTerdEmeles":
                for (TorzsMozgasformak t : beolvasTorzsMozgasok(2)) {
                    if (t.getIsmetles() == eredmeny) {
                        return t.getKorcsoport(korcsoport);
                    }
                }
                break;
            case "futas3200":
                for (Futas3200 f : beolvasFutas3200()) {
                    if (f.getIdo() == eredmeny) {
                        return f.getKorcsoport(korcsoport);
                    }
                }
                break;
            case "futas2000":
                for (Futas2000 f : beolvasFutas2000()) {
                    if (f.getIdo() == eredmeny) {
                        return f.getKorcsoport(korcsoport);
                    }
                }
                break;
            case "menet1600":
                for (Menet1600 m : beolvasMenet1600()) {
                    if (m.getIdo() == eredmeny) {
                        return m.getKorcsoport(korcsoport);
                    }
                }
                break;
            case "ergometria":
                for (Ergometria e : beolvasErgometria()) {
                    if (e.getWattKg() == eredmeny) {
                        return e.getKorcsoport(korcsoport);
                    }
                }
                break;
        }
        return -1;
    }

    @Override
    public int[] findBMIPontszam(int magassag, String korcsoport) throws FafDAOException {
        for (BMI b : beolvasBMI()) {
            if (b.getMagassag() == magassag) {
                return new int[]{b.getKorcsoport(korcsoport), b.getMinSuly()};
            }
        }
        return new int[0];
    }
    
    @Override
    public List<BMI> beolvasBMI() throws FafDAOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(new File(utvonal + "bmi.json"));
            List<BMI> lista = new ArrayList<>();
            for (int i = 0; i < root.size(); i++) {
                int magassag = root.get(i).get("magassag").asInt();
                int fEgy = root.get(i).get("ffiKorcsopEgy").asInt();
                int nEgy = root.get(i).get("noKorcsopEgy").asInt();
                int fKetto = root.get(i).get("ffiKorcsopKetto").asInt();
                int nKetto = root.get(i).get("noKorcsopKetto").asInt();
                int fHarom = root.get(i).get("ffiKorcsopHarom").asInt();
                int nHarom = root.get(i).get("noKorcsopHarom").asInt();
                int fNegy = root.get(i).get("ffiKorcsopNegy").asInt();
                int nNegy = root.get(i).get("noKorcsopNegy").asInt();
                int minSuly = root.get(i).get("minimumSuly").asInt();
                BMI bmi = new BMI(magassag, fEgy, nEgy, fKetto, nKetto, fHarom, nHarom, fNegy, nNegy, minSuly);
                lista.add(bmi);
            }
            return lista;
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public List<Fekvo> beolvasFekvo() throws FafDAOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(new File(utvonal + "fekvo.json"));
            List<Fekvo> lista = new ArrayList<>();
            for (int i = 0; i < root.size(); i++) {
                int ismetles = root.get(i).get("ismetles").asInt();
                int fEgy = root.get(i).get("ffiKorcsopEgy").asInt();
                int nEgy = root.get(i).get("noKorcsopEgy").asInt();
                int fKetto = root.get(i).get("ffiKorcsopKetto").asInt();
                int nKetto = root.get(i).get("noKorcsopKetto").asInt();
                int fHarom = root.get(i).get("ffiKorcsopHarom").asInt();
                int nHarom = root.get(i).get("noKorcsopHarom").asInt();
                int fNegy = root.get(i).get("ffiKorcsopNegy").asInt();
                int nNegy = root.get(i).get("noKorcsopNegy").asInt();
                int fOt = root.get(i).get("ffiKorcsopOt").asInt();
                int nOt = root.get(i).get("noKorcsopOt").asInt();
                int fHat = root.get(i).get("ffiKorcsopHat").asInt();
                int nHat = root.get(i).get("noKorcsopHat").asInt();
                int fHet = root.get(i).get("ffiKorcsopHet").asInt();
                int nHet = root.get(i).get("noKorcsopHet").asInt();
                int fNyolc = root.get(i).get("ffiKorcsopNyolc").asInt();
                int nNyolc = root.get(i).get("noKorcsopNyolc").asInt();
                int fKilenc = root.get(i).get("ffiKorcsopKilenc").asInt();
                int nKilenc = root.get(i).get("noKorcsopKilenc").asInt();
                Fekvo fekvo = new Fekvo(ismetles, fEgy, nEgy, fKetto, nKetto, fHarom, nHarom, fNegy, nNegy, fOt, nOt, fHat, nHat, fHet, nHet,
                        fNyolc, nNyolc, fKilenc, nKilenc);
                lista.add(fekvo);
            }
            return lista;
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public List<Huzodzkodas> beolvasHuzodzkodas() throws FafDAOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(new File(utvonal + "huzodzkodas.json"));
            List<Huzodzkodas> lista = new ArrayList<>();
            for (int i = 0; i < root.size(); i++) {
                int ismetles = root.get(i).get("ismetles").asInt();
                int fEgy = root.get(i).get("ffiKorcsopEgy").asInt();
                int nEgy = root.get(i).get("noKorcsopEgy").asInt();
                int fKetto = root.get(i).get("ffiKorcsopKetto").asInt();
                int nKetto = root.get(i).get("noKorcsopKetto").asInt();
                int fHarom = root.get(i).get("ffiKorcsopHarom").asInt();
                int nHarom = root.get(i).get("noKorcsopHarom").asInt();
                int fNegy = root.get(i).get("ffiKorcsopNegy").asInt();
                int nNegy = root.get(i).get("noKorcsopNegy").asInt();
                int fOt = root.get(i).get("ffiKorcsopOt").asInt();
                int nOt = root.get(i).get("noKorcsopOt").asInt();
                int fHat = root.get(i).get("ffiKorcsopHat").asInt();
                int nHat = root.get(i).get("noKorcsopHat").asInt();
                int fHet = root.get(i).get("ffiKorcsopHet").asInt();
                int nHet = root.get(i).get("noKorcsopHet").asInt();
                int fNyolc = root.get(i).get("ffiKorcsopNyolc").asInt();
                int nNyolc = root.get(i).get("noKorcsopNyolc").asInt();
                int fKilenc = root.get(i).get("ffiKorcsopKilenc").asInt();
                int nKilenc = root.get(i).get("noKorcsopKilenc").asInt();
                Huzodzkodas huzodzkodas = new Huzodzkodas(ismetles, fEgy, nEgy, fKetto, nKetto, fHarom, nHarom, fNegy, nNegy, fOt, nOt, fHat, nHat, fHet, nHet,
                        fNyolc, nNyolc, fKilenc, nKilenc);
                lista.add(huzodzkodas);
            }
            return lista;
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public List<HajlitottFugges> beolvasHajlitottFugges() throws FafDAOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(new File(utvonal + "hajlitottfugges.json"));
            List<HajlitottFugges> lista = new ArrayList<>();
            for (int i = 0; i < root.size(); i++) {
                int ido = root.get(i).get("ido").asInt();
                int fEgy = root.get(i).get("ffiKorcsopEgy").asInt();
                int nEgy = root.get(i).get("noKorcsopEgy").asInt();
                int fKetto = root.get(i).get("ffiKorcsopKetto").asInt();
                int nKetto = root.get(i).get("noKorcsopKetto").asInt();
                int fHarom = root.get(i).get("ffiKorcsopHarom").asInt();
                int nHarom = root.get(i).get("noKorcsopHarom").asInt();
                int fNegy = root.get(i).get("ffiKorcsopNegy").asInt();
                int nNegy = root.get(i).get("noKorcsopNegy").asInt();
                int fOt = root.get(i).get("ffiKorcsopOt").asInt();
                int nOt = root.get(i).get("noKorcsopOt").asInt();
                HajlitottFugges hajlitottFugges = new HajlitottFugges(ido, fEgy, nEgy, fKetto, nKetto, fHarom, nHarom, fNegy, nNegy, fOt, nOt);
                lista.add(hajlitottFugges);
            }
            return lista;
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public <T extends TorzsMozgasformak> List<T> beolvasTorzsMozgasok(int sorszam) throws FafDAOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = null;
            List<T> lista = new ArrayList<>();
            TorzsMozgasformak torzs = null;
            switch (sorszam) {
                case 0:
                    root = mapper.readTree(new File(utvonal + "felules.json"));
                    torzs = new Felules();
                    break;
                case 1:
                    root = mapper.readTree(new File(utvonal + "lapockaemeles.json"));
                    torzs = new LapockaEmeles();
                    break;
                case 2:
                    root = mapper.readTree(new File(utvonal + "fuggoterdemeles.json"));
                    torzs = new FuggoTerdEmeles();
                    break;
            }
            for (int i = 0; i < root.size(); i++) {
                torzs.setIsmetles(root.get(i).get("ismetles").asInt());
                torzs.setFfiKorcsopEgy(root.get(i).get("ffiKorcsopEgy").asInt());
                torzs.setNoKorcsopEgy(root.get(i).get("noKorcsopEgy").asInt());
                torzs.setFfiKorcsopKetto(root.get(i).get("ffiKorcsopKetto").asInt());
                torzs.setNoKorcsopKetto(root.get(i).get("noKorcsopKetto").asInt());
                torzs.setFfiKorcsopHarom(root.get(i).get("ffiKorcsopHarom").asInt());
                torzs.setNoKorcsopHarom(root.get(i).get("noKorcsopHarom").asInt());
                torzs.setFfiKorcsopNegy(root.get(i).get("ffiKorcsopNegy").asInt());
                torzs.setNoKorcsopNegy(root.get(i).get("noKorcsopNegy").asInt());
                torzs.setFfiKorcsopOt(root.get(i).get("ffiKorcsopOt").asInt());
                torzs.setNoKorcsopOt(root.get(i).get("noKorcsopOt").asInt());
                torzs.setFfiKorcsopHat(root.get(i).get("ffiKorcsopHat").asInt());
                torzs.setNoKorcsopHat(root.get(i).get("noKorcsopHat").asInt());
                torzs.setFfiKorcsopHet(root.get(i).get("ffiKorcsopHet").asInt());
                torzs.setNoKorcsopHet(root.get(i).get("noKorcsopHet").asInt());
                torzs.setFfiKorcsopNyolc(root.get(i).get("ffiKorcsopNyolc").asInt());
                torzs.setNoKorcsopNyolc(root.get(i).get("noKorcsopNyolc").asInt());
                torzs.setFfiKorcsopKilenc(root.get(i).get("ffiKorcsopKilenc").asInt());
                torzs.setNoKorcsopKilenc(root.get(i).get("noKorcsopKilenc").asInt());
                lista.add((T) torzs);
            }
            return lista;
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public List<Futas3200> beolvasFutas3200() throws FafDAOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(new File(utvonal + "futas3200.json"));
            List<Futas3200> lista = new ArrayList<>();
            for (int i = 0; i < root.size(); i++) {
                int ido = root.get(i).get("ido").asInt();
                int fEgy = root.get(i).get("ffiKorcsopEgy").asInt();
                int nEgy = root.get(i).get("noKorcsopEgy").asInt();
                int fKetto = root.get(i).get("ffiKorcsopKetto").asInt();
                int nKetto = root.get(i).get("noKorcsopKetto").asInt();
                int fHarom = root.get(i).get("ffiKorcsopHarom").asInt();
                int nHarom = root.get(i).get("noKorcsopHarom").asInt();
                int fNegy = root.get(i).get("ffiKorcsopNegy").asInt();
                int nNegy = root.get(i).get("noKorcsopNegy").asInt();
                int fOt = root.get(i).get("ffiKorcsopOt").asInt();
                int nOt = root.get(i).get("noKorcsopOt").asInt();
                Futas3200 futas = new Futas3200(ido, fEgy, nEgy, fKetto, nKetto, fHarom, nHarom, fNegy, nNegy, fOt, nOt);
                lista.add(futas);
            }
            return lista;
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public List<Futas2000> beolvasFutas2000() throws FafDAOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(new File(utvonal + "futas2000.json"));
            List<Futas2000> lista = new ArrayList<>();
            for (int i = 0; i < root.size(); i++) {
                int ido = root.get(i).get("ido").asInt();
                int fHat = root.get(i).get("ffiKorcsopHat").asInt();
                int nHat = root.get(i).get("noKorcsopHat").asInt();
                int fHet = root.get(i).get("ffiKorcsopHet").asInt();
                int nHet = root.get(i).get("noKorcsopHet").asInt();
                int fNyolc = root.get(i).get("ffiKorcsopNyolc").asInt();
                int nNyolc = root.get(i).get("noKorcsopNyolc").asInt();
                int fKilenc = root.get(i).get("ffiKorcsopKilenc").asInt();
                int nKilenc = root.get(i).get("noKorcsopKilenc").asInt();
                Futas2000 futas = new Futas2000(ido, fHat, nHat, fHet, nHet, fNyolc, nNyolc, fKilenc, nKilenc);
                lista.add(futas);
            }
            return lista;
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public List<Menet1600> beolvasMenet1600() throws FafDAOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(new File(utvonal + "menet1600.json"));
            List<Menet1600> lista = new ArrayList<>();
            for (int i = 0; i < root.size(); i++) {
                int ido = root.get(i).get("ido").asInt();
                int fEgy = root.get(i).get("ffiKorcsopEgy").asInt();
                int nEgy = root.get(i).get("noKorcsopEgy").asInt();
                int fKetto = root.get(i).get("ffiKorcsopKetto").asInt();
                int nKetto = root.get(i).get("noKorcsopKetto").asInt();
                int fHarom = root.get(i).get("ffiKorcsopHarom").asInt();
                int nHarom = root.get(i).get("noKorcsopHarom").asInt();
                int fNegy = root.get(i).get("ffiKorcsopNegy").asInt();
                int nNegy = root.get(i).get("noKorcsopNegy").asInt();
                int fOt = root.get(i).get("ffiKorcsopOt").asInt();
                int nOt = root.get(i).get("noKorcsopOt").asInt();
                int fHat = root.get(i).get("ffiKorcsopHat").asInt();
                int nHat = root.get(i).get("noKorcsopHat").asInt();
                int fHet = root.get(i).get("ffiKorcsopHet").asInt();
                int nHet = root.get(i).get("noKorcsopHet").asInt();
                int fNyolc = root.get(i).get("ffiKorcsopNyolc").asInt();
                int nNyolc = root.get(i).get("noKorcsopNyolc").asInt();
                int fKilenc = root.get(i).get("ffiKorcsopKilenc").asInt();
                int nKilenc = root.get(i).get("noKorcsopKilenc").asInt();
                Menet1600 menet = new Menet1600(ido, fEgy, nEgy, fKetto, nKetto, fHarom, nHarom, fNegy, nNegy, fOt, nOt,
                        fHat, nHat, fHet, nHet, fNyolc, nNyolc, fKilenc, nKilenc);
                lista.add(menet);
            }
            return lista;
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public List<Ergometria> beolvasErgometria() throws FafDAOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(new File(utvonal + "ergometria.json"));
            List<Ergometria> lista = new ArrayList<>();
            for (int i = 0; i < root.size(); i++) {
                int wattKg = root.get(i).get("wattKg").asInt();
                int fEgy = root.get(i).get("ffiKorcsopEgy").asInt();
                int nEgy = root.get(i).get("noKorcsopEgy").asInt();
                int fKetto = root.get(i).get("ffiKorcsopKetto").asInt();
                int nKetto = root.get(i).get("noKorcsopKetto").asInt();
                int fHarom = root.get(i).get("ffiKorcsopHarom").asInt();
                int nHarom = root.get(i).get("noKorcsopHarom").asInt();
                int fNegy = root.get(i).get("ffiKorcsopNegy").asInt();
                int nNegy = root.get(i).get("noKorcsopNegy").asInt();
                int fOt = root.get(i).get("ffiKorcsopOt").asInt();
                int nOt = root.get(i).get("noKorcsopOt").asInt();
                int fHat = root.get(i).get("ffiKorcsopHat").asInt();
                int nHat = root.get(i).get("noKorcsopHat").asInt();
                int fHet = root.get(i).get("ffiKorcsopHet").asInt();
                int nHet = root.get(i).get("noKorcsopHet").asInt();
                Ergometria ergometria = new Ergometria(wattKg, fEgy, nEgy, fKetto, nKetto, fHarom, nHarom, fNegy, nNegy, fOt, nOt, fHat, nHat, fHet, nHet);
                lista.add(ergometria);
            }
            return lista;
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
    
    @Override
    public void insertBMI(BMI b) throws FafDAOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
            List<BMI> lista = beolvasBMI();
            lista.add(b);
            writer.writeValue(new File(utvonal + "bmi.json"), lista);
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
    
    @Override
    public <T extends KarMozgasformak> void insertKar(T kar) throws FafDAOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
            if (kar instanceof Fekvo) {
                List<Fekvo> lista = beolvasFekvo();
                lista.add((Fekvo) kar);
                writer.writeValue(new File(utvonal + "fekvo.json"), lista);
            }
            if (kar instanceof Huzodzkodas) {
                List<Huzodzkodas> lista = beolvasHuzodzkodas();
                lista.add((Huzodzkodas) kar);
                writer.writeValue(new File(utvonal + "huzodzkodas.json"), lista);
            }
            if (kar instanceof HajlitottFugges) {
                List<HajlitottFugges> lista = beolvasHajlitottFugges();
                lista.add((HajlitottFugges) kar);
                writer.writeValue(new File(utvonal + "hajlitottfugges.json"), lista);
            }
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public <T extends TorzsMozgasformak> void insertTorzs(T torzs) throws FafDAOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
            if (torzs instanceof Felules) {
                List<Felules> lista = beolvasTorzsMozgasok(0);
                lista.add((Felules) torzs);
                writer.writeValue(new File(utvonal + "felules.json"), lista);
            }
            if (torzs instanceof LapockaEmeles) {
                List<LapockaEmeles> lista = beolvasTorzsMozgasok(1);
                lista.add((LapockaEmeles) torzs);
                writer.writeValue(new File(utvonal + "lapockaemeles.json"), lista);
            }
            if (torzs instanceof FuggoTerdEmeles) {
                List<FuggoTerdEmeles> lista = beolvasTorzsMozgasok(2);
                lista.add((FuggoTerdEmeles) torzs);
                writer.writeValue(new File(utvonal + "fuggoterdemeles.json"), lista);
            }
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public <T extends KeringesMozgasformak> void insertKeringes(T keringes) throws FafDAOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
            if (keringes instanceof Futas3200) {
                List<Futas3200> lista = beolvasFutas3200();
                lista.add((Futas3200) keringes);
                writer.writeValue(new File(utvonal + "futas3200.json"), lista);
            }
            if (keringes instanceof Futas2000) {
                List<Futas2000> lista = beolvasFutas2000();
                lista.add((Futas2000) keringes);
                writer.writeValue(new File(utvonal + "futas2000.json"), lista);
            }
            if (keringes instanceof Menet1600) {
                List<Menet1600> lista = beolvasMenet1600();
                lista.add((Menet1600) keringes);
                writer.writeValue(new File(utvonal + "menet1600.json"), lista);
            }
            if (keringes instanceof Ergometria) {
                List<Ergometria> lista = beolvasErgometria();
                lista.add((Ergometria) keringes);
                writer.writeValue(new File(utvonal + "ergometria.json"), lista);
            }
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
    
    @Override
    public <T extends KarMozgasformak> void updateKar(T kar) throws FafDAOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T extends TorzsMozgasformak> void updateTorzs(T torzs) throws FafDAOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T extends KeringesMozgasformak> void updateKeringes(T keringes) throws FafDAOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T extends KarMozgasformak> void deleteKar(T kar) throws FafDAOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T extends TorzsMozgasformak> void deleteTorzs(T torzs) throws FafDAOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T extends KeringesMozgasformak> void deleteKeringes(T keringes) throws FafDAOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
