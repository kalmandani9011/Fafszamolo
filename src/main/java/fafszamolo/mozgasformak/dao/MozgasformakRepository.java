package fafszamolo.mozgasformak.dao;

import fafszamolo.model.BMI;
import fafszamolo.model.Ergometria;
import fafszamolo.model.Fekvo;
import fafszamolo.model.Futas2000;
import fafszamolo.model.Futas3200;
import fafszamolo.model.HajlitottFugges;
import fafszamolo.model.Huzodzkodas;
import fafszamolo.model.KarMozgasformak;
import fafszamolo.model.KeringesMozgasformak;
import fafszamolo.model.Menet1600;
import fafszamolo.model.TorzsMozgasformak;
import fafszamolo.exception.FafDAOException;
import java.util.List;

/**
 * Az alapadatokat tartalmazó fájlok, adatbázisok elérésére szolgáló osztályok közös interface-je.
 */
public interface MozgasformakRepository {
    
    /**
     * Kikeresi a pontszámot az adott táblából.
     * @param tabla A tábla neve, amelyikből a pontszámot kell kikeresni.
     * @param eredmeny Ami alapján a sort azonosítjuk be.
     * @param korcsoport Ami alapján az oszlopot azonosítjuk be.
     * @return A megadott cella (pontszám) értékét adja vissza.
     */
    int findPontszam(String tabla, int eredmeny, String korcsoport) throws FafDAOException;
    
    /**
     * Kikeresi a BMI értéket a táblából.
     * @param magassag Ami alapján a sort azonosítjuk be.
     * @param korcsoport Ami alapján az oszlopot azonosítjuk be.
     * @return Egy tömböt ad vissza, melynek első eleme az adott cella értéke (ez a maximális engedélyezett súly),
     * a második eleme pedig a minimum súly. E két értéken belül megfelelő az adott személy BMI indexe.
     */
    int[] findBMIPontszam(int magassag, String korcsoport) throws FafDAOException;
    
    /**
     * A BMI táblázat listába való beolvasása.
     */
    List<BMI> beolvasBMI() throws FafDAOException;
    
    /**
     * A fekvő táblázat listába való beolvasása.
     */
    List<Fekvo> beolvasFekvo() throws FafDAOException;
    
    /**
     * A húzódzkodás táblázat listába való beolvasása.
     */
    List<Huzodzkodas> beolvasHuzodzkodas() throws FafDAOException;
    
    /**
     * A hajlított függés táblázat listába való beolvasása.
     */
    List<HajlitottFugges> beolvasHajlitottFugges() throws FafDAOException;
    
    /**
     * A törzs mozgásformák (felülés, lapocka emelés, függő térdemelés)
     * táblázatai listába való beolvasása.
     */
    <T extends TorzsMozgasformak> List<T> beolvasTorzsMozgasok(int sorszam) throws FafDAOException;
    
    /**
     * A 3200m futás táblázat listába való beolvasása.
     */
    List<Futas3200> beolvasFutas3200() throws FafDAOException;
    
    /**
     * A 2000m futás táblázat listába való beolvasása.
     */
    List<Futas2000> beolvasFutas2000() throws FafDAOException;
    
    /**
     * Az 1600m menet táblázat listába való beolvasása.
     */
    List<Menet1600> beolvasMenet1600() throws FafDAOException;
    
    /**
     * Az ergometria táblázat listába való beolvasása.
     */
    List<Ergometria> beolvasErgometria() throws FafDAOException;
    
    /**
     * BMI objektum adatbázisba való hozzáadása.
     */
    void insertBMI(BMI b) throws FafDAOException;
    
    /**
     * Fekvőtámasz, húzódzkodás, függő térdemelés objektum adatbázisba való
     * hozzáadása.
     */
    <T extends KarMozgasformak> void insertKar(T kar) throws FafDAOException;
    
    /**
     * Felülés, lapocka emelés, függő térdemelés objektum adatbázisba való
     * hozzáadása.
     */
    <T extends TorzsMozgasformak> void insertTorzs(T torzs) throws FafDAOException;
    
    /**
     * 3200 m, 2000 m futás, 1600 m menet, ergometria objektum adatbázisba való
     * hozzáadása.
     */
    <T extends KeringesMozgasformak> void insertKeringes(T keringes) throws FafDAOException;
    
    <T extends KarMozgasformak> void updateKar(T kar) throws FafDAOException;
    
    <T extends TorzsMozgasformak> void updateTorzs(T torzs) throws FafDAOException;
    
    <T extends KeringesMozgasformak> void updateKeringes(T keringes) throws FafDAOException;
    
    <T extends KarMozgasformak> void deleteKar(T kar) throws FafDAOException;
    
    <T extends TorzsMozgasformak> void deleteTorzs(T torzs) throws FafDAOException;
    
    <T extends KeringesMozgasformak> void deleteKeringes(T keringes) throws FafDAOException;
}
