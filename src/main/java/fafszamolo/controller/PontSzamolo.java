package fafszamolo.controller;

import fafszamolo.exception.FafDAOException;
import fafszamolo.faf.dao.FafRepository;
import fafszamolo.model.Ergometria;
import fafszamolo.model.Fekvo;
import fafszamolo.model.Felules;
import fafszamolo.model.FuggoTerdEmeles;
import fafszamolo.model.Futas2000;
import fafszamolo.model.Futas3200;
import fafszamolo.model.HajlitottFugges;
import fafszamolo.model.Huzodzkodas;
import fafszamolo.model.KarMozgasformak;
import fafszamolo.model.KeringesMozgasformak;
import fafszamolo.model.LapockaEmeles;
import fafszamolo.model.Menet1600;
import fafszamolo.model.Szemely;
import fafszamolo.model.TorzsMozgasformak;
import fafszamolo.szemely.dao.SzemelyRepository;
import fafszamolo.felmeres.dao.FelmeresRepository;
import fafszamolo.mozgasformak.dao.MozgasformakRepository;

/**
 * A fizikai felmérés eredményének számítására szolgáló osztály.
 */
public class PontSzamolo {

    private static final int MINPONTHATAR = 220;
    private static final int MAXFEKVO = 75;
    private static final int MINFEKVO = 8;
    private static final int MAXHUZODZKODAS = 20;
    private static final int MINHUZODZKODAS = 1;
    private static final int MAXFUGGES = 70;
    private static final int MINFUGGES = 5;
    private static final int MAXFELULES = 90;
    private static final int MINFELULES = 6;
    private static final int MAXLAPOCKAEMELES = 72;
    private static final int MINLAPOCKAEMELES = 13;
    private static final int MAXTERDEMELES = 51;
    private static final int MINTERDEMELES = 10;
    private static final int MAXFUTAS3200 = 1330;
    private static final int MINFUTAS3200 = 2411;
    private static final int MAXFUTAS2000 = 1005;
    private static final int MINFUTAS2000 = 1808;
    private static final int MAXMENET1600 = 922;
    private static final int MINMENET1600 = 1826;
    private static final int MAXERGOMETRIA = 320;
    private static final int MINERGOMETRIA = 99;
    private static final int FEKVOTAMASZCOMBOBOXELEM = 0;
    private static final int HUZODZKODASCOMBOBOXELEM = 1;
    private static final int HAJLITOTTFUGGESCOMBOBOXELEM = 2;
    private static final int FELULESCOMBOBOXELEM = 0;
    private static final int LAPOCKAEMELESCOMBOBOXELEM = 1;
    private static final int FUGGOTERDEMELESCOMBOBOXELEM = 2;
    private static final int FUTAS3200COMBOBOXELEM = 0;
    private static final int FUTAS2000COMBOBOXELEM = 1;
    private static final int MENET1600COMBOBOXELEM = 2;
    private static final int ERGOMETRIACOMBOBOXELEM = 3;
    private String minosites = "MF";
    private SzemelyRepository szemelyRepo;
    private FelmeresRepository felmeresRepo;
    private MozgasformakRepository adatRepo;

    public PontSzamolo(SzemelyRepository szemelyRepo, FelmeresRepository felmeresRepo, MozgasformakRepository adatRepo) {
        this.szemelyRepo = szemelyRepo;
        this.felmeresRepo = felmeresRepo;
        this.adatRepo = adatRepo;
    }

    public SzemelyRepository getSzemelyRepo() {
        return this.szemelyRepo;
    }
    
    public FelmeresRepository getFelmeresRepo() {
        return this.felmeresRepo;
    }
    
    public String getMinosites() {
        return this.minosites;
    }

    public void setMinosites(String minosites) {
        this.minosites = minosites;
    }

    /**
     * Pontszám számítás.
     * @param isFerfi A pontszámok függnek a személy nemétől.
     * @param kor A pontszámok függnek a személy korától.
     * @param karFajta A kar mozgásforma meghatározására szolgál.
     * @param karEredmeny A kar mozgásforma során végrehajtott eredmény.
     * @param torzsFajta A törzs mozgásforma meghatározására szolgál.
     * @param torzsEredmeny A törzs mozgásforma során végrehajtott eredmény.
     * @param keringesFajta A keringés mozgásforma meghatározására szolgál.
     * @param keringesEredmeny A keringés mozgásforma során végrehajtott eredmény.
     * @return Az összpontszámot adja vissza.
     */
    public int getEredmeny(boolean isFerfi, int kor, int karFajta, int karEredmeny, int torzsFajta, int torzsEredmeny, int keringesFajta, int keringesEredmeny) throws FafDAOException {
        try {
            String korcsoport;
            int karPont = 0;
            if (karFajta == HAJLITOTTFUGGESCOMBOBOXELEM) {
                korcsoport = hajlitottFuggesKorcsoportMeghataroz(isFerfi, kor);
            } else {
                korcsoport = korcsoportMeghataroz(isFerfi, kor);
            }
            karPont = karLekerdezes(karFajta, karEredmeny, korcsoport);
            korcsoport = korcsoportMeghataroz(isFerfi, kor);
            int torzsPont = torzsLekerdezes(torzsFajta, torzsEredmeny, korcsoport);
            int keringesPont = keringesLekerdezes(keringesFajta, keringesEredmeny, korcsoport);
            int eredmeny = karPont + torzsPont + keringesPont;
            if (eredmeny < MINPONTHATAR || karPont == 0 || torzsPont == 0 || keringesPont == 0) {
                this.minosites = "NMF";
            }
            return eredmeny;
        } catch (FafDAOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
    
    /**
     * Az adott személy BMI indexét határozza meg. A BMI indexek külön korcsoportokra vannak meghatározva.
     * @param sz A személy, akinek a BMI indexét kell meghatározni.
     * @return Megfelelt vagy nem megfelelt eredményt ad vissza.
     */
    public String bmiMeghataroz(Szemely sz) throws FafDAOException {
        int[] suly = adatRepo.findBMIPontszam(sz.getMagassag(), bmiKorcsoportMeghataroz(sz.isFerfi(), sz.getKor()));
        int maxSuly = suly[0];
        int minSuly = suly[1];
        if (sz.getSuly() >= minSuly && sz.getSuly() <= maxSuly) {
            return "MF";
        } else {
            return "NMF";
        }
    }
    
    /**
     * Az adott személy testzsír indexét határozza meg. Függ a személy nemétől, a testzsír százalékától és a korától.
     * @param sz A személy akinek a testzsír indexét kell meghatározni.
     * @return Megfelelő vagy nem megfelelő értéket ad vissza.
     */
    public String testZsirIndexMeghataroz(Szemely sz) {
        if (sz.isFerfi()) {
            if (sz.getKor() <= 30) {
                if (sz.getTestZsir() >= 8.0 && sz.getTestZsir() <= 17.5) {
                    return "MF";
                } else {
                    return "NMF";
                }
            } else if (sz.getKor() >= 31 && sz.getKor() <= 40) {
                if (sz.getTestZsir() >= 10.0 && sz.getTestZsir() <= 20.0) {
                    return "MF";
                } else {
                    return "NMF";
                }
            } else if (sz.getKor() >= 41 && sz.getKor() <= 59) {
                if (sz.getTestZsir() >= 11.0 && sz.getTestZsir() <= 22.0) {
                    return "MF";
                } else {
                    return "NMF";
                }
            } else if (sz.getKor() >= 60) {
                if (sz.getTestZsir() >= 13.0 && sz.getTestZsir() <= 25.0) {
                    return "MF";
                } else {
                    return "NMF";
                }
            }
        } else {
            if (sz.getKor() <= 30) {
                if (sz.getTestZsir() >= 20.0 && sz.getTestZsir() <= 30.0) {
                    return "MF";
                } else {
                    return "NMF";
                }
            } else if (sz.getKor() >= 31 && sz.getKor() <= 40) {
                if (sz.getTestZsir() >= 21.0 && sz.getTestZsir() <= 33.0) {
                    return "MF";
                } else {
                    return "NMF";
                }
            } else if (sz.getKor() >= 41 && sz.getKor() <= 59) {
                if (sz.getTestZsir() >= 23.0 && sz.getTestZsir() <= 34.0) {
                    return "MF";
                } else {
                    return "NMF";
                }
            } else if (sz.getKor() >= 60) {
                if (sz.getTestZsir() >= 24.0 && sz.getTestZsir() <= 36.0) {
                    return "MF";
                } else {
                    return "NMF";
                }
            }
        }
        return "";
    }

    /**
     * A legtöbb mozgásformára alkalmazandó korcsoport meghatározó metódus.
     * Az adatbázis megfelelő oszlopának kiválasztásához szükséges.
     * Amelyeket nem eszerint határozunk meg, azokat külön metódus szerint határozzuk meg. 
     * @param isFerfi A személy neme.
     * @param kor A személy kora.
     * @return Az adatbázis oszlop nevét adja vissza.
     */
    private String korcsoportMeghataroz(boolean isFerfi, int kor) {
        if (isFerfi) {
            if (kor < 25) {
                return "ffiKorcsopEgy";
            } else if (kor >= 25 && kor <= 29) {
                return "ffiKorcsopKetto";
            } else if (kor >= 30 && kor <= 34) {
                return "ffiKorcsopHarom";
            } else if (kor >= 35 && kor <= 39) {
                return "ffiKorcsopNegy";
            } else if (kor >= 40 && kor <= 44) {
                return "ffiKorcsopOt";
            } else if (kor >= 45 && kor <= 49) {
                return "ffiKorcsopHat";
            } else if (kor >= 50 && kor <= 54) {
                return "ffiKorcsopHet";
            } else if (kor >= 55 && kor <= 59) {
                return "ffiKorcsopNyolc";
            } else if (kor >= 60 && kor <= 65) {
                return "ffiKorcsopKilenc";
            }
        } else {
            if (kor < 25) {
                return "noKorcsopEgy";
            } else if (kor >= 25 && kor <= 29) {
                return "noKorcsopKetto";
            } else if (kor >= 30 && kor <= 34) {
                return "noKorcsopHarom";
            } else if (kor >= 35 && kor <= 39) {
                return "noKorcsopNegy";
            } else if (kor >= 40 && kor <= 44) {
                return "noKorcsopOt";
            } else if (kor >= 45 && kor <= 49) {
                return "noKorcsopHat";
            } else if (kor >= 50 && kor <= 54) {
                return "noKorcsopHet";
            } else if (kor >= 55 && kor <= 59) {
                return "noKorcsopNyolc";
            } else if (kor >= 60 && kor <= 65) {
                return "noKorcsopKilenc";
            }
        }
        return "";
    }

    /**
     * A hajlított függés mozgásformára alkalmazandó korcsoport meghatározó metódus.
     * Az adatbázis megfelelő oszlopának kiválasztásához szükséges.
     * @param isFerfi A személy neme.
     * @param kor A személy kora.
     * @return Az adatbázis oszlop nevét adja vissza.
     */
    private String hajlitottFuggesKorcsoportMeghataroz(boolean isFerfi, int kor) {
        if (isFerfi) {
            if (kor < 31) {
                return "ffiKorcsopEgy";
            } else if (kor >= 31 && kor <= 40) {
                return "ffiKorcsopKetto";
            } else if (kor >= 41 && kor <= 50) {
                return "ffiKorcsopHarom";
            } else if (kor >= 51 && kor <= 54) {
                return "ffiKorcsopNegy";
            } else if (kor >= 55 && kor <= 65) {
                return "ffiKorcsopOt";
            }
        } else {
            if (kor < 31) {
                return "noKorcsopEgy";
            } else if (kor >= 31 && kor <= 40) {
                return "noKorcsopKetto";
            } else if (kor >= 41 && kor <= 50) {
                return "noKorcsopHarom";
            } else if (kor >= 51 && kor <= 54) {
                return "noKorcsopNegy";
            } else if (kor >= 55 && kor <= 65) {
                return "noKorcsopOt";
            }
        }
        return "";
    }
    
    /**
     * A BMI index meghatározásához szükséges metódus.
     * Az adatbázis megfelelő oszlopának kiválasztásához szükséges.
     * @param isFerfi A személy neme.
     * @param kor A személy kora.
     * @return Az adatbázis oszlop nevét adja vissza.
     */
    private String bmiKorcsoportMeghataroz(boolean isFerfi, int kor) {
        if (isFerfi) {
            if (kor < 26) {
                return "ffiKorcsopEgy";
            } else if (kor >= 26 && kor <= 35) {
                return "ffiKorcsopKetto";
            } else if (kor >= 36 && kor <= 45) {
                return "ffiKorcsopHarom";
            } else if (kor >= 46) {
                return "ffiKorcsopNegy";
            }
        } else {
            if (kor < 26) {
                return "noKorcsopEgy";
            } else if (kor >= 26 && kor <= 35) {
                return "noKorcsopKetto";
            } else if (kor >= 36 && kor <= 45) {
                return "noKorcsopHarom";
            } else if (kor >= 46) {
                return "noKorcsopNegy";
            }
        }
        return "";
    }

    /**
     * A kar eredményének lekérdezéséhez szükséges metódus.
     * @param karFajta A grafikus felületen kiválasztott kar mozgásforma indexe.
     * @param karEredmeny A kar mozgásforma elért eredménye.
     * @param korcsoport A korcsoport, amely az adatbázisból való kiválasztáshoz szükséges.
     * @return Az elért pontszámot adja vissza.
     */
    private int karLekerdezes(int karFajta, int karEredmeny, String korcsoport) throws FafDAOException {
        KarMozgasformak kar = null;
        switch (karFajta) {
            case FEKVOTAMASZCOMBOBOXELEM:
                kar = new Fekvo();
                break;
            case HUZODZKODASCOMBOBOXELEM:
                kar = new Huzodzkodas();
                break;
            case HAJLITOTTFUGGESCOMBOBOXELEM:
                kar = new HajlitottFugges();
                break;
        }
        if (kar instanceof Fekvo) {
            if (karEredmeny > MAXFEKVO) {
                return 100;
            } else if(karEredmeny < MINFEKVO) {
                return 0;
            } else {
                return adatRepo.findPontszam("fekvo", karEredmeny, korcsoport);
            }
        }
        if (kar instanceof Huzodzkodas) {
            if (karEredmeny > MAXHUZODZKODAS) {
                return 100;
            } else if(karEredmeny < MINHUZODZKODAS) {
                return 0;
            } else {
                return adatRepo.findPontszam("huzodzkodas", karEredmeny, korcsoport);
            }
        }
        if (kar instanceof HajlitottFugges) {
            if (karEredmeny > MAXFUGGES) {
                return 100;
            } else if(karEredmeny < MINFUGGES) {
                return 0;
            } else {
                return adatRepo.findPontszam("hajlitottFugges", karEredmeny, korcsoport);
            }
        }
        return -1;
    }

    /**
     * A törzs eredményének lekérdezéséhez szükséges metódus.
     * @param torzsFajta A grafikus felületen kiválasztott törzs mozgásforma indexe.
     * @param torzsEredmeny A törzs mozgásforma elért eredménye.
     * @param korcsoport A korcsoport, amely az adatbázisból való kiválasztáshoz szükséges.
     * @return Az elért pontszámot adja vissza.
     */
    private int torzsLekerdezes(int torzsFajta, int torzsEredmeny, String korcsoport) throws FafDAOException {
        TorzsMozgasformak torzs = null;
        switch (torzsFajta) {
            case FELULESCOMBOBOXELEM:
                torzs = new Felules();
                break;
            case LAPOCKAEMELESCOMBOBOXELEM:
                torzs = new LapockaEmeles();
                break;
            case FUGGOTERDEMELESCOMBOBOXELEM:
                torzs = new FuggoTerdEmeles();
                break;
        }
        if (torzs instanceof Felules) {
            if (torzsEredmeny > MAXFELULES) {
                return 100;
            } else if(torzsEredmeny < MINFELULES) {
                return 0;
            } else {
                return adatRepo.findPontszam("felules", torzsEredmeny, korcsoport);
            }
        }
        if (torzs instanceof LapockaEmeles) {
            if (torzsEredmeny > MAXLAPOCKAEMELES) {
                return 80;
            } else if(torzsEredmeny < MINLAPOCKAEMELES) {
                return 0;
            } else {
                return adatRepo.findPontszam("lapockaEmeles", torzsEredmeny, korcsoport);
            }
        }
        if (torzs instanceof FuggoTerdEmeles) {
            if (torzsEredmeny > MAXTERDEMELES) {
                return 80;
            } else if(torzsEredmeny < MINTERDEMELES) {
                return 0;
            } else {
                return adatRepo.findPontszam("fuggoTerdEmeles", torzsEredmeny, korcsoport);
            }
        }
        return -1;
    }

    /**
     * A keringés eredményének lekérdezéséhez szükséges metódus.
     * @param keringesFajta A grafikus felületen kiválasztott keringés mozgásforma indexe.
     * @param keringesEredmeny A keringés mozgásforma elért eredménye.
     * @param korcsoport A korcsoport, amely az adatbázisból való kiválasztáshoz szükséges.
     * @return Az elért pontszámot adja vissza.
     */
    private int keringesLekerdezes(int keringesFajta, int keringesEredmeny, String korcsoport) throws FafDAOException {
        KeringesMozgasformak keringes = null;
        switch (keringesFajta) {
            case FUTAS3200COMBOBOXELEM:
                keringes = new Futas3200();
                break;
            case FUTAS2000COMBOBOXELEM:
                keringes = new Futas2000();
                break;
            case MENET1600COMBOBOXELEM:
                keringes = new Menet1600();
                break;
            case ERGOMETRIACOMBOBOXELEM:
                keringes = new Ergometria();
                break;
        }
        if (keringes instanceof Futas3200) {
            if (keringesEredmeny < MAXFUTAS3200) {
                return 160;
            } else if(keringesEredmeny > MINFUTAS3200) {
                return 0;
            } else {
                return adatRepo.findPontszam("futas3200", keringesEredmeny, korcsoport);
            }
        }
        if (keringes instanceof Futas2000) {
            if (keringesEredmeny < MAXFUTAS2000) {
                return 160;
            } else if(keringesEredmeny > MINFUTAS2000) {
                return 0;
            } else {
                return adatRepo.findPontszam("futas2000", keringesEredmeny, korcsoport);
            }
        }
        if (keringes instanceof Menet1600) {
            if (keringesEredmeny < MAXMENET1600) {
                return 160;
            } else if(keringesEredmeny > MINMENET1600) {
                return 0;
            } else {
                return adatRepo.findPontszam("menet1600", keringesEredmeny, korcsoport);
            }
        }
        if (keringes instanceof Ergometria) {
            if (keringesEredmeny > MAXERGOMETRIA) {
                return 160;
            } else if(keringesEredmeny < MINERGOMETRIA) {
                return 0;
            } else {
                return adatRepo.findPontszam("ergometria", keringesEredmeny, korcsoport);
            }
        }
        return -1;
    }
}
