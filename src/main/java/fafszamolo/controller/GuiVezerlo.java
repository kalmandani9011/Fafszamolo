package fafszamolo.controller;

import fafszamolo.exception.FafDAOException;
import fafszamolo.felmeres.dao.FelmeresRepository;
import fafszamolo.felmeres.dao.impl.FelmeresRepositoryCSVImpl;
import fafszamolo.felmeres.dao.impl.FelmeresRepositoryJDBCImpl;
import fafszamolo.gui.KeresesDialog;
import fafszamolo.gui.FafDialog;
import fafszamolo.gui.MegtekintoDialog;
import fafszamolo.gui.SzemelyDialog;
import fafszamolo.model.FizikaiAllapotFelmeres;
import fafszamolo.model.Szemely;
import fafszamolo.mozgasformak.dao.MozgasformakRepository;
import fafszamolo.mozgasformak.dao.impl.MozgasformakRepositoryCSVImpl;
import fafszamolo.mozgasformak.dao.impl.MozgasformakRepositoryJDBCImpl;
import fafszamolo.szemely.dao.SzemelyRepository;
import fafszamolo.szemely.dao.impl.SzemelyRepositoryCSVImpl;
import fafszamolo.szemely.dao.impl.SzemelyRepositoryJDBCImpl;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 * A GUI és a számoló osztály közötti kapcsolatot teremti meg. Ez az osztály
 * végzi a legtöbb háttérmunkát, a grafikus felületekről bejövő adatok és az
 * adatbázisok alapján.
 */
public class GuiVezerlo {

    private static int ALAPERTELMEZETTNULLERTEK = -1;
    private Connection conn;
    private PontSzamolo szamolo;
    private DefaultListModel<Szemely> model;
    private DefaultTableModel adatbazis;
    private Szemely szemely;
    private List<Szemely> szemelyek;
    private SzemelyDialog szemelyDialogus;
    private FafDialog fafDialogus;
    private MegtekintoDialog megtekintoDialogus;
    private Integer listaIndex = ALAPERTELMEZETTNULLERTEK;
    private List<Integer> sztszSzamok = new ArrayList<>();
    private FizikaiAllapotFelmeres felmeres;

    public GuiVezerlo() throws FafDAOException {
        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/faftablazat?serverTimezone=UTC", "root", "1234");
            SzemelyRepository szemelyRepo = new SzemelyRepositoryCSVImpl();
            FelmeresRepository felmeresRepo = new FelmeresRepositoryCSVImpl();
            MozgasformakRepository adatRepo = new MozgasformakRepositoryCSVImpl();
            szamolo = new PontSzamolo(szemelyRepo, felmeresRepo, adatRepo);
            szemelyek = szamolo.getSzemelyRepo().findAll();
            sztszSzamGeneralo();
            for (Szemely sz : szemelyek) {
                sz.setFelmeresek(szamolo.getFelmeresRepo().findBySZTSZ(sz.getSztsz()));
            }
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    public PontSzamolo getPontSzamolo() {
        return szamolo;
    }

    private void sztszSzamGeneralo() {
        sztszSzamok.clear();
        for (int i = 0; i < szemelyek.size(); i++) {
            sztszSzamok.add(szemelyek.get(i).getSztsz());
        }
    }

    public void setModel(DefaultListModel model) {
        this.model = model;
    }

    public void setAdatbazisModel(DefaultTableModel adatbazis) {
        this.adatbazis = adatbazis;
    }

    public void setListaIndex(Integer index) {
        this.listaIndex = index;
    }

    /**
     * A lista elemeinek újratöltése.
     */
    public void listatRendez() {
        for (Szemely sz : szemelyek) {
            model.addElement(sz);
        }
    }

    /**
     * A megadott Comparatorral rendezi a lista elemeit.
     */
    public void rendezes(Comparator comp) {
        szemelyek.sort(comp);
        model.clear();
        listatRendez();
    }

    /**
     * Segédmetódus a személy lista és a személy adatbázis elemeinek
     * módosítására vagy új személy hozzáadására.
     */
    private void updateSzemelyList() throws FafDAOException {
        szamolo.getSzemelyRepo().saveSzemely(szemely);
        if (szemely.getSorszam() == null) {
            System.out.println(szamolo.getSzemelyRepo().findBySZTSZ(szemely.getSztsz()).getSorszam());
            Integer sorszam = szamolo.getSzemelyRepo().findBySZTSZ(szemely.getSztsz()).getSorszam();
            szemely.setSorszam(sorszam);
            szemelyek.add(szemely);
            model.addElement(szemely);
        } else {
            szemelyek.set(listaIndex, szemely);
            model.set(listaIndex, szemely);
        }
        sztszSzamGeneralo();
    }

    /**
     * Kiválasztott személy adatainak módosítása külön dialógus ablak
     * megnyitásával.
     */
    public void szemelyModositas() throws FafDAOException {
        if (listaIndex != ALAPERTELMEZETTNULLERTEK) {
            szemelyDialogus = new SzemelyDialog(null, Optional.of(model.get(listaIndex)));
            szemelyDialogus.setSztszSzamok(sztszSzamok);
            Optional<Szemely> optSzemely = szemelyDialogus.showDialog();
            if (optSzemely.isPresent()) {
                szemely = optSzemely.get();
                updateSzemelyList();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Nincs személy kijelölve!", "Hiba!", JOptionPane.OK_OPTION);
        }
    }

    /**
     * Személy törlése a listából és az adatbázisból.
     */
    public void szemelyTorles(Szemely sz) throws FafDAOException {
        try {
            if (listaIndex != ALAPERTELMEZETTNULLERTEK) {
                JPanel panel = new JPanel();
                panel.add(new JLabel("Biztosan törölni szeretnéd a kiválasztott elemet?"));
                int result = JOptionPane.showConfirmDialog(null, panel, "Törlés", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
                if (result == JOptionPane.YES_OPTION) {
                    szamolo.getSzemelyRepo().deleteSzemely(sz.getSorszam());
                    szemelyek.remove(listaIndex.intValue());
                    model.remove(listaIndex);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Nincs személy kijelölve!", "Hiba!", JOptionPane.OK_OPTION);
            }
        } catch (FafDAOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    /**
     * Új személy felvitele külön dialógus ablak megnyitásával
     */
    public void ujSzemely() throws FafDAOException {
        szemelyDialogus = new SzemelyDialog(null);
        szemelyDialogus.setSztszSzamok(sztszSzamok);
        Optional<Szemely> optSzemely = szemelyDialogus.showDialog();
        if (optSzemely.isPresent()) {
            szemely = optSzemely.get();
            updateSzemelyList();
        }
    }

    /**
     * Segédmetódus az adatbázisban szereplő felmérések módosítására, vagy új
     * létrehozására.
     */
    private void updateFelmeresList() throws FafDAOException {
        int pontszam = szamolo.getEredmeny(model.get(listaIndex).isFerfi(), model.get(listaIndex).getKor(), felmeres.getKarMozgasforma(), felmeres.getKarEredmeny(),
                felmeres.getTorzsMozgasforma(), felmeres.getTorzsEredmeny(), felmeres.getKeringesMozgasforma(), felmeres.getKeringesEredmeny());
        felmeres.setPontszam(pontszam);
        felmeres.setBmi(szamolo.bmiMeghataroz(model.get(listaIndex)));
        felmeres.setTestZsirIndex(szamolo.testZsirIndexMeghataroz(model.get(listaIndex)));
        szamolo.getFelmeresRepo().saveFelmeres(felmeres);
        model.get(listaIndex).setFelmeresek(szamolo.getFelmeresRepo().findBySZTSZ(model.get(listaIndex).getSztsz()));
        szemelyek.get(listaIndex).setFelmeresek(szamolo.getFelmeresRepo().findBySZTSZ(szemelyek.get(listaIndex).getSztsz()));
    }

    /**
     * Fizikai felmérés hozzáadása egy személyhez.
     */
    public void felmeresHozzaadas() throws FafDAOException {
        if (listaIndex != ALAPERTELMEZETTNULLERTEK) {
            fafDialogus = new FafDialog(null, model.get(listaIndex));
            Optional<FizikaiAllapotFelmeres> optFaf = fafDialogus.showDialog();
            if (optFaf.isPresent()) {
                felmeres = optFaf.get();
                updateFelmeresList();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Nincs személy kijelölve!", "Hiba!", JOptionPane.OK_OPTION);
        }
    }

    /**
     * Felmérés törlése az adatbázisból.
     *
     * @param sorszam Ami alapján töröljük a felmérést az adatbázisból.
     */
    public void felmeresTorles(int sorszam) throws FafDAOException {
        JPanel panel = new JPanel();
        panel.add(new JLabel("Biztosan törölni szeretnéd a kiválasztott elemet?"));
        int result = JOptionPane.showConfirmDialog(null, panel, "Törlés", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
        if (result == JOptionPane.YES_OPTION) {
            szamolo.getFelmeresRepo().deleteFelmeres(sorszam);
            model.get(listaIndex).setFelmeresek(szamolo.getFelmeresRepo().findBySZTSZ(model.get(listaIndex).getSztsz()));
            szemelyek.get(listaIndex).setFelmeresek(szamolo.getFelmeresRepo().findBySZTSZ(szemelyek.get(listaIndex).getSztsz()));
        }
    }

    /**
     * A megtekintő dialógus ablakban kijelölt felmérés módosítására szolgál.
     *
     * @param sorszam Ami alapján megtalálja a felmérést a listában.
     */
    public void felmeresModositas(int sorszam) throws FafDAOException {
        FafDialog fafDialogus = new FafDialog(null, model.get(listaIndex), Optional.of(model.get(listaIndex).getFelmeresek().get(sorszam)));
        Optional<FizikaiAllapotFelmeres> optFaf = fafDialogus.showDialog();
        if (optFaf.isPresent()) {
            felmeres = optFaf.get();
            updateFelmeresList();
            megtekintoDialogus.setVisible(false);
            megtekintes();
        }
    }

    /**
     * Egy kijelölt személy fizikai felméréseinek kilistázása külön ablakban.
     */
    public void megtekintes() throws FafDAOException {
        if (listaIndex != ALAPERTELMEZETTNULLERTEK) {
            if (model.get(listaIndex).getFelmeresek().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Nincs a személyhez tartozó felmérés!", "Hiba!", JOptionPane.OK_OPTION);
                return;
            }
            megtekintoDialogus = new MegtekintoDialog(null, model.get(listaIndex), this);
            megtekintoDialogus.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "Nincs személy kijelölve!", "Hiba!", JOptionPane.OK_OPTION);
        }
    }

    /**
     * Keresést hajt végre az adatbázisban név vagy SZTSZ szám szerint.
     *
     * @return Visszaadja a keresett személy indexét a listában.
     */
    public int kereses() throws FafDAOException {
        KeresesDialog keresesDialogus = new KeresesDialog(null, this);
        Optional<String> optKereses = keresesDialogus.showDialog();
        if (optKereses == null) {
            return -2;
        }
        if (optKereses.isPresent()) {
            String kereses = optKereses.get();
            if (kereses.matches("[0-9]+")) {
                int index = Integer.parseInt(kereses);
                for (Szemely sz : szemelyek) {
                    if (sz.getSztsz() == index) {
                        return szemelyek.indexOf(sz);
                    }
                }
            } else {
                for (Szemely sz : szemelyek) {
                    if (sz.getNev().equalsIgnoreCase(kereses)) {
                        return szemelyek.indexOf(sz);
                    }
                }
            }
        }
        return -1;
    }

    /**
     * Az adatbázisba betölti az összes személyt és ha van, akkor az összes
     * hozzá tartozó felmérést.
     */
    public void betoltMind() {
        adatbazis.setRowCount(0);
        for (Szemely sz : szemelyek) {
            if (sz.getFelmeresek().isEmpty()) {
                adatbazis.addRow(ujSzemelySor(sz).toArray());
            } else {
                for (FizikaiAllapotFelmeres faf : sz.getFelmeresek()) {
                    List<Object> ujSor = ujSzemelySor(sz);
                    ujSor.addAll(ujFelmeresSor(faf));
                    adatbazis.addRow(ujSor.toArray());
                }
            }
        }
    }

    /**
     * Az adatbázisba betölti összes felmérést az adott évben.
     */
    public void betoltAdottEv(int ev) throws FafDAOException {
        adatbazis.setRowCount(0);
        for (Szemely sz : szemelyek) {
            List<FizikaiAllapotFelmeres> felmeresek = szamolo.getFelmeresRepo().findByEv(ev);
            for (FizikaiAllapotFelmeres f : felmeresek) {
                if (f.getSztsz() == sz.getSztsz()) {
                    List<Object> ujSor = ujSzemelySor(sz);
                    ujSor.addAll(ujFelmeresSor(f));
                    adatbazis.addRow(ujSor.toArray());
                }
            }
        }
    }

    /**
     * Az adatbázisba betölti azokat a személyeket, akiknek van felmérésük.
     */
    public void betoltVanFaf() {
        adatbazis.setRowCount(0);
        for (Szemely sz : szemelyek) {
            if (!sz.getFelmeresek().isEmpty()) {
                for (FizikaiAllapotFelmeres faf : sz.getFelmeresek()) {
                    List<Object> ujSor = ujSzemelySor(sz);
                    ujSor.addAll(ujFelmeresSor(faf));
                    adatbazis.addRow(ujSor.toArray());
                }
            }
        }
    }

    private List<Object> ujSzemelySor(Szemely sz) {
        List<Object> ujSor = new ArrayList<>();
        ujSor.add(sz.getSorszam());
        ujSor.add(sz.getSztsz());
        ujSor.add(sz.getNev());
        ujSor.add(sz.getRf());
        ujSor.add(sz.getAlakulat());
        ujSor.add(sz.getSzulDatum());
        ujSor.add(sz.getAnyjaNeve());
        ujSor.add(sz.isFerfi() ? "férfi" : "nő");
        ujSor.add(sz.getMagassag());
        ujSor.add(sz.getSuly());
        ujSor.add(sz.getTestZsir());
        return ujSor;
    }

    private List<Object> ujFelmeresSor(FizikaiAllapotFelmeres faf) {
        List<Object> ujFafSor = new ArrayList<>();
        ujFafSor.add(faf.getVegrehajtasIdeje());
        ujFafSor.add(getKarMozgasforma(faf.getKarMozgasforma()));
        ujFafSor.add(faf.getKarEredmeny());
        ujFafSor.add(getTorzsMozgasforma(faf.getTorzsMozgasforma()));
        ujFafSor.add(faf.getTorzsEredmeny());
        ujFafSor.add(getKeringesMozgasforma(faf.getKarMozgasforma()));
        ujFafSor.add(faf.getKeringesEredmeny());
        ujFafSor.add(faf.getPontszam());
        ujFafSor.add(faf.getBmi());
        ujFafSor.add(faf.getTestZsirIndex());
        return ujFafSor;
    }

    private String getKarMozgasforma(int kod) {
        String s = "";
        switch (kod) {
            case 0:
                s = "fekvőtámasz";
                break;
            case 1:
                s = "húzódzkodás";
                break;
            case 2:
                s = "hajlított karú függés";
                break;
        }
        return s;
    }

    private String getTorzsMozgasforma(int kod) {
        String s = "";
        switch (kod) {
            case 0:
                s = "felülés";
                break;
            case 1:
                s = "lapocka emelés";
                break;
            case 2:
                s = "függő térdemelés";
                break;
        }
        return s;
    }

    private String getKeringesMozgasforma(int kod) {
        String s = "";
        switch (kod) {
            case 0:
                s = "3200m futás";
                break;
            case 1:
                s = "2000m futás";
                break;
            case 2:
                s = "1600m menet";
                break;
            case 3:
                s = "ergometria";
                break;
        }
        return s;
    }
}
