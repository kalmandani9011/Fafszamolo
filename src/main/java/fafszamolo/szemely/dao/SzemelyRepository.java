package fafszamolo.szemely.dao;

import fafszamolo.model.Szemely;
import fafszamolo.exception.FafDAOException;
import java.util.List;

/**
 * A személyeket tartalmazó fájlok, adatbázisok elérésére szolgáló osztályok közös interface-je.
 */
public interface SzemelyRepository {
    
    /**
     * Az összes személy lekérdezésére szolgáló metódus.
     *
     * @return Visszaadja az összes személyt tartalmazó listát.
     */
    List<Szemely> findAll() throws FafDAOException;
    
    /**
     * SZTSZ szám szerint megkeresi az adott személyt.
     *
     * @param sztsz Az SZTSZ szám, ami alapján keressük a személyt.
     * @return Visszaadja a személy objektumot.
     */
    Szemely findBySZTSZ(int sztsz) throws FafDAOException;
    
    /**
     * Név szerint megkeresi az adott személyt vagy személyeket.
     *
     * @param nev A név, ami alapján keressük a személyt vagy személyeket.
     * @return Visszaadja a személyek listáját.
     */
    List<Szemely> findByNev(String nev) throws FafDAOException;
    
    /**
     * Elmenti az adatbázisba a személy objektumot. Ha nem létezik még a személy
     * az adatbázisban, akkor újat ad hozzá, ha létezik, akkor felülírja a
     * meglévőt.
     *
     * @param sz A személy, akit el akarunk menteni az adatbázisba.
     */
    void saveSzemely(Szemely sz) throws FafDAOException;
    
    /**
     * Új személy felvitele az adatbázisba.
     */
    void insertSzemely(Szemely sz) throws FafDAOException;
    
    /**
     * Személy adatainak frissítése az adatbázisban.
     */
    void updateSzemely(Szemely sz) throws FafDAOException;
    
    /**
     * Töröl egy személyt a megadott sorszám alapján az adatbázisból.
     */
    void deleteSzemely(int sorszam) throws FafDAOException;
    
}
