package fafszamolo.szemely.dao.impl;

import fafszamolo.szemely.dao.SzemelyRepository;
import fafszamolo.model.Rendfokozat;
import fafszamolo.model.Szemely;
import fafszamolo.exception.FafDAOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * A személyeket tartalmazó .csv fájl elérésére való osztály.
 */
public class SzemelyRepositoryCSVImpl implements SzemelyRepository {

    private String utvonal;

    /**
     * Normál esetben ezzel a konstruktorral hozzunk létre az objektumot, a
     * személy adatbázis így elérhető.
     */
    public SzemelyRepositoryCSVImpl() {
        this.utvonal = "src/main/java/fafszamolo/csv/";
    }

    /**
     * Ezzel a konstruktorral beállítható az elérési útvonal a személy
     * adatbázishoz. Ezzel lehetséges a tesztelés is akár.
     */
    public SzemelyRepositoryCSVImpl(String utvonal) {
        this.utvonal = utvonal;
    }

    @Override
    public List<Szemely> findAll() throws FafDAOException {
        List<Szemely> lista = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(utvonal + "szemelyek.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (!line.contains("#")) {
                    if (!line.contains(";")) {
                        throw new FafDAOException("Hiba a fájl olvasása közben: túl kevés adat, vagy nem megfelelő szeparátor.");
                    }
                    String[] tomb = line.split(";");
                    Integer sorszam = Integer.parseInt(tomb[0]);
                    int sztsz = Integer.parseInt(tomb[1]);
                    String nev = tomb[2];
                    Rendfokozat rf = Rendfokozat.valueOf(tomb[3]);
                    String alakulat = tomb[4];
                    LocalDate szulDatum = LocalDate.parse(tomb[5]);
                    String anyjaNeve = tomb[6];
                    boolean ferfi = Boolean.parseBoolean(tomb[7]);
                    int magassag = Integer.parseInt(tomb[8]);
                    int suly = Integer.parseInt(tomb[9]);
                    double testZsir = Double.parseDouble(tomb[10]);
                    lista.add(new Szemely(sorszam, sztsz, nev, rf, alakulat, szulDatum, anyjaNeve, ferfi, magassag, suly, testZsir));
                }
            }
            return lista;
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public Szemely findBySZTSZ(int sztsz) throws FafDAOException {
        for (Szemely sz : findAll()) {
            if (sz.getSztsz() == sztsz) {
                return sz;
            }
        }
        return null;
    }

    @Override
    public List<Szemely> findByNev(String nev) throws FafDAOException {
        List<Szemely> lista = new ArrayList<>();
        for (Szemely sz : findAll()) {
            if (sz.getNev().equals(nev)) {
                lista.add(sz);
            }
        }
        return lista;
    }

    @Override
    public void saveSzemely(Szemely sz) throws FafDAOException {
        if (sz.getSorszam() == null) {
            insertSzemely(sz);
        } else {
            updateSzemely(sz);
        }
    }

    @Override
    public void insertSzemely(Szemely sz) throws FafDAOException {
        List<Szemely> lista = findAll();
        Szemely tmp = new Szemely(sz.getSztsz(), sz.getNev(), sz.getRf(), sz.getAlakulat(), sz.getSzulDatum(), sz.getAnyjaNeve(), sz.isFerfi(),
                sz.getMagassag(), sz.getSuly(), sz.getTestZsir());
        tmp.setSorszam(lista.get(lista.size() - 1).getSorszam() + 1);
        lista.add(tmp);
        kiir(lista);
    }

    @Override
    public void updateSzemely(Szemely sz) throws FafDAOException {
        List<Szemely> lista = findAll();
        int index = 0;
        for (Szemely szemely : lista) {
            if (szemely.getSorszam() == sz.getSorszam()) {
                index = lista.indexOf(szemely);
            }
        }
        lista.set(index, sz);
        kiir(lista);
    }

    @Override
    public void deleteSzemely(int sorszam) throws FafDAOException {
        List<Szemely> lista = new ArrayList<>();
        for (Szemely sz : findAll()) {
            if (sz.getSorszam() != sorszam) {
                lista.add(sz);
            }
        }
        kiir(lista);
    }
    
    /**
     * Segédmetódus a fájlba való kiíráshoz.
     */
    private void kiir(List<Szemely> lista) throws FafDAOException {
        try (PrintWriter pw = new PrintWriter(new FileWriter(utvonal + "szemelyek.csv"))) {
            for (Szemely szemely : lista) {
                pw.println(szemely.getSorszam() + ";" + szemely.getSztsz() + ";" + szemely.getNev() + ";" + szemely.getRf().name() + ";" + 
                        szemely.getAlakulat() + ";" + szemely.getSzulDatum() + ";" + szemely.getAnyjaNeve() + ";" + szemely.isFerfi() + ";" + 
                        szemely.getMagassag() + ";"+ szemely.getSuly() + ";" + szemely.getTestZsir());
            }
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

}
