package fafszamolo.szemely.dao.impl;

import fafszamolo.szemely.dao.SzemelyRepository;
import fafszamolo.model.Rendfokozat;
import fafszamolo.model.Szemely;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import fafszamolo.exception.FafDAOException;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * A személyeket tartalmazó .json fájl elérésére való osztály.
 */
public class SzemelyRepositoryJSONImpl implements SzemelyRepository {

    private String utvonal;

    /**
     * Normál esetben ezzel a konstruktorral hozzunk létre az objektumot, a
     * személy adatbázis így elérhető.
     */
    public SzemelyRepositoryJSONImpl() throws FafDAOException {
        this.utvonal = "src/fafszamolo/json/";
    }

    /**
     * Ezzel a konstruktorral beállítható az elérési útvonal a személy
     * adatbázishoz. Ezzel lehetséges a tesztelés is akár.
     */
    public SzemelyRepositoryJSONImpl(String utvonal) throws FafDAOException {
        this.utvonal = utvonal;
    }

    @Override
    public List<Szemely> findAll() throws FafDAOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(new File(utvonal + "szemelyek.json"));
            List<Szemely> lista = new ArrayList<>();
            for (int i = 0; i < root.size(); i++) {
                Integer sorszam = root.get(i).get("sorszam").asInt();
                int sztsz = root.get(i).get("sztsz").asInt();
                String nev = root.get(i).get("nev").asText();
                Rendfokozat rf = Rendfokozat.valueOf(root.get(i).get("rf").asText());
                String alakulat = root.get(i).get("alakulat").asText();
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                LocalDate szulDatum = LocalDate.parse(root.get(i).get("szulDatum").asText(), dtf);
                String anyjaNeve = root.get(i).get("anyjaNeve").asText();
                boolean ferfi = root.get(i).get("ferfi").asBoolean();
                int magassag = root.get(i).get("magassag").asInt();
                int suly = root.get(i).get("suly").asInt();
                double testZsir = root.get(i).get("testZsir").asDouble();
                Szemely szemely = new Szemely(sorszam, sztsz, nev, rf, alakulat, szulDatum, anyjaNeve, ferfi, magassag, suly, testZsir);
                lista.add(szemely);
            }
            return lista;
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    @Override
    public Szemely findBySZTSZ(int sztsz) throws FafDAOException {
        List<Szemely> lista = findAll();
        for (Szemely sz : lista) {
            if (sz.getSztsz() == sztsz) {
                return sz;
            }
        }
        return null;
    }

    @Override
    public List<Szemely> findByNev(String nev) throws FafDAOException {
        List<Szemely> lista = new ArrayList<>();
        for (Szemely sz : findAll()) {
            if (sz.getNev().equals(nev)) {
                lista.add(sz);
            }
        }
        return lista;
    }

    @Override
    public void saveSzemely(Szemely sz) throws FafDAOException {
        if (sz.getSorszam() == null) {
            insertSzemely(sz);
        } else {
            updateSzemely(sz);
        }
    }

    @Override
    public void insertSzemely(Szemely sz) throws FafDAOException {
        List<Szemely> lista = findAll();
        sz.setSorszam(lista.get(lista.size() - 1).getSorszam() + 1);
        lista.add(sz);
        kiir(lista);
    }

    @Override
    public void updateSzemely(Szemely sz) throws FafDAOException {
        List<Szemely> lista = findAll();
        int index = 0;
        for (Szemely szemely : lista) {
            if (szemely.getSorszam() == sz.getSorszam()) {
                index = lista.indexOf(szemely);
            }
        }
        lista.set(index, sz);
        kiir(lista);
    }

    @Override
    public void deleteSzemely(int sorszam) throws FafDAOException {
        List<Szemely> lista = new ArrayList<>();
        for (Szemely sz : findAll()) {
            if (sz.getSorszam() != sorszam) {
                lista.add(sz);
            }
        }
        kiir(lista);
    }

    /**
     * Segédmetódus a fájlba való kiíráshoz.
     */
    private void kiir(List<Szemely> lista) throws FafDAOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
            ArrayNode szemelyek = mapper.createArrayNode();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            for (Szemely tmp : lista) {
                ObjectNode ujSzemely = szemelyek.addObject();
                ujSzemely.put("sorszam", tmp.getSorszam());
                ujSzemely.put("sztsz", tmp.getSztsz());
                ujSzemely.put("nev", tmp.getNev());
                ujSzemely.put("rf", tmp.getRf().name());
                ujSzemely.put("alakulat", tmp.getAlakulat());
                ujSzemely.put("nev", tmp.getNev());
                ujSzemely.put("szulDatum", tmp.getSzulDatum().format(dtf));
                ujSzemely.put("anyjaNeve", tmp.getAnyjaNeve());
                ujSzemely.put("ferfi", tmp.isFerfi());
                ujSzemely.put("magassag", tmp.getMagassag());
                ujSzemely.put("suly", tmp.getSuly());
                ujSzemely.put("testZsir", tmp.getTestZsir());
            }
            writer.writeValue(new File(utvonal + "szemelyek.json"), szemelyek);
        } catch (IOException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
}
