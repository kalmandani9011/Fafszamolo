package fafszamolo.szemely.dao.impl;

import fafszamolo.model.Rendfokozat;
import fafszamolo.model.Szemely;
import fafszamolo.exception.FafDAOException;
import fafszamolo.szemely.dao.SzemelyRepository;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * A személyeket tartalmazó adatbázissal való kommunikációra alkalmas osztály.
 */
public class SzemelyRepositoryJDBCImpl implements SzemelyRepository {

    private Connection conn;
    private PreparedStatement findAll;
    private PreparedStatement insertSzemely;
    private PreparedStatement updateSzemely;
    private PreparedStatement deleteSzemely;
    private PreparedStatement findBySZTSZ;
    private PreparedStatement findByNev;
    
    public SzemelyRepositoryJDBCImpl(Connection conn) throws FafDAOException {
        try {
            this.conn = conn;
            this.findAll = this.conn.prepareStatement("SELECT * FROM szemely");
            this.findBySZTSZ = this.conn.prepareStatement("SELECT * FROM szemely WHERE sztsz = ?");
            this.findByNev = this.conn.prepareStatement("SELECT * FROM szemely WHERE nev LIKE ?");
            this.insertSzemely = this.conn.prepareStatement("INSERT INTO szemely (sztsz, nev, rendfokozat, alakulat, szulDatum, anyjaNeve, ferfi, magassag,"
                    + "suly, testZsir) VALUES (?,?,?,?,?,?,?,?,?,?)");
            this.updateSzemely = this.conn.prepareStatement("UPDATE szemely SET sztsz = ?, nev = ?, rendfokozat = ?, alakulat = ?, szulDatum = ?, anyjaNeve = ?,"
                    + " ferfi = ?, magassag = ?, suly = ?, testZsir = ? WHERE sorszam = ?");
            this.deleteSzemely = this.conn.prepareStatement("DELETE FROM szemely WHERE sorszam = ?");
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
    
    public List<Szemely> findAll() throws FafDAOException {
        try {
            ResultSet all = this.findAll.executeQuery();
            List<Szemely> lista = makeList(all);
            return lista;
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
    
    public Szemely findBySZTSZ(int sztsz) throws FafDAOException {
        try {
            this.findBySZTSZ.setInt(1, sztsz);
            ResultSet rsSztsz = this.findBySZTSZ.executeQuery();
            Szemely sz = null;
            while (rsSztsz.next()) {
                sz = makeOne(rsSztsz);
            }
            return sz;
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    
    public List<Szemely> findByNev(String nev) throws FafDAOException {
        try {
            this.findByNev.setString(1, nev);
            ResultSet rsNev = this.findByNev.executeQuery();
            List<Szemely> lista = makeList(rsNev);
            return lista;
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
    
    public void saveSzemely(Szemely sz) throws FafDAOException {
        if (sz.getSorszam() == null) {
            insertSzemely(sz);
        } else {
            updateSzemely(sz);
        }
    }
    
    public void insertSzemely(Szemely sz) throws FafDAOException {
        try {
            this.insertSzemely.setInt(1, sz.getSztsz());
            this.insertSzemely.setString(2, sz.getNev());
            this.insertSzemely.setString(3, sz.getRf().name());
            this.insertSzemely.setString(4, sz.getAlakulat());
            this.insertSzemely.setDate(5, Date.valueOf(sz.getSzulDatum()));
            this.insertSzemely.setString(6, sz.getAnyjaNeve());
            this.insertSzemely.setBoolean(7, sz.isFerfi());
            this.insertSzemely.setInt(8, sz.getMagassag());
            this.insertSzemely.setInt(9, sz.getSuly());
            this.insertSzemely.setDouble(10, sz.getTestZsir());
            this.insertSzemely.executeUpdate();
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    public void updateSzemely(Szemely sz) throws FafDAOException {
        try {
            this.updateSzemely.setInt(1, sz.getSztsz());
            this.updateSzemely.setString(2, sz.getNev());
            this.updateSzemely.setString(3, sz.getRf().name());
            this.updateSzemely.setString(4, sz.getAlakulat());
            this.updateSzemely.setDate(5, Date.valueOf(sz.getSzulDatum()));
            this.updateSzemely.setString(6, sz.getAnyjaNeve());
            this.updateSzemely.setBoolean(7, sz.isFerfi());
            this.updateSzemely.setInt(8, sz.getMagassag());
            this.updateSzemely.setInt(9, sz.getSuly());
            this.updateSzemely.setDouble(10, sz.getTestZsir());
            this.updateSzemely.setInt(11, sz.getSorszam());
            this.updateSzemely.executeUpdate();
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
    
    public void deleteSzemely(int sorszam) throws FafDAOException {
        try {
            this.deleteSzemely.setInt(1, sorszam);
            this.deleteSzemely.executeUpdate();
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
    
    /**
     * Segédmetódus, ami listát készít.
     */
    private List<Szemely> makeList(ResultSet rs) throws FafDAOException {
        try {
            List<Szemely> lista = new ArrayList();
            while (rs.next()) {
                lista.add(makeOne(rs));
            }
            return lista;
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

    /**
     * Segédmetódus, ami egy személy objektumot készít.
     */
    private Szemely makeOne(ResultSet rs) throws FafDAOException {
        try {
            Szemely sz = new Szemely();
            sz.setSorszam(rs.getInt("sorszam"));
            sz.setSztsz(rs.getInt("sztsz"));
            sz.setNev(rs.getString("nev"));
            sz.setRf(Rendfokozat.valueOf(rs.getString("rendfokozat")));
            sz.setAlakulat(rs.getString("alakulat"));
            sz.setSzulDatum(rs.getDate("szulDatum").toLocalDate());
            sz.setAnyjaNeve(rs.getString("anyjaNeve"));
            sz.setIsFerfi(rs.getBoolean("ferfi"));
            sz.setMagassag(rs.getInt("magassag"));
            sz.setSuly(rs.getInt("suly"));
            sz.setTestZsir(rs.getDouble("testZsir"));
            return sz;
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }

}
