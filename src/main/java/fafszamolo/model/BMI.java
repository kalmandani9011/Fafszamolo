package fafszamolo.model;

/**
 *A BMI index meghatározásához szükséges osztály.
 */
public class BMI {

    private int magassag;
    private int ffiKorcsopEgy;
    private int noKorcsopEgy;
    private int ffiKorcsopKetto;
    private int noKorcsopKetto;
    private int ffiKorcsopHarom;
    private int noKorcsopHarom;
    private int ffiKorcsopNegy;
    private int noKorcsopNegy;
    private int minSuly;

    public BMI() {
    }

    public BMI(int magassag, int ffiKorcsopEgy, int noKorcsopEgy, int ffiKorcsopKetto, int noKorcsopKetto, int ffiKorcsopHarom, int noKorcsopHarom, int ffiKorcsopNegy, int noKorcsopNegy, int minSuly) {
        this.magassag = magassag;
        this.ffiKorcsopEgy = ffiKorcsopEgy;
        this.noKorcsopEgy = noKorcsopEgy;
        this.ffiKorcsopKetto = ffiKorcsopKetto;
        this.noKorcsopKetto = noKorcsopKetto;
        this.ffiKorcsopHarom = ffiKorcsopHarom;
        this.noKorcsopHarom = noKorcsopHarom;
        this.ffiKorcsopNegy = ffiKorcsopNegy;
        this.noKorcsopNegy = noKorcsopNegy;
        this.minSuly = minSuly;
    }

    /**
     * Visszaadja a korcsoportnak megfelelő értéket.
     * @param korcsoport Ez alapján azonosítja be a megfelelő korcsoportot.
     */
    public int getKorcsoport(String korcsoport) {
        switch (korcsoport) {
            case "ffiKorcsopEgy":
                return this.ffiKorcsopEgy;
            case "ffiKorcsopKetto":
                return this.ffiKorcsopKetto;
            case "ffiKorcsopHarom":
                return this.ffiKorcsopHarom;
            case "ffiKorcsopNegy":
                return this.ffiKorcsopNegy;
            case "noKorcsopEgy":
                return this.noKorcsopEgy;
            case "noKorcsopKetto":
                return this.noKorcsopKetto;
            case "noKorcsopHarom":
                return this.noKorcsopHarom;
            case "noKorcsopNegy":
                return this.noKorcsopNegy;
        }
        return 0;
    }

    public int getMagassag() {
        return magassag;
    }

    public void setMagassag(int magassag) {
        this.magassag = magassag;
    }

    public int getFfiKorcsopEgy() {
        return ffiKorcsopEgy;
    }

    public void setFfiKorcsopEgy(int ffiKorcsopEgy) {
        this.ffiKorcsopEgy = ffiKorcsopEgy;
    }

    public int getNoKorcsopEgy() {
        return noKorcsopEgy;
    }

    public void setNoKorcsopEgy(int noKorcsopEgy) {
        this.noKorcsopEgy = noKorcsopEgy;
    }

    public int getFfiKorcsopKetto() {
        return ffiKorcsopKetto;
    }

    public void setFfiKorcsopKetto(int ffiKorcsopKetto) {
        this.ffiKorcsopKetto = ffiKorcsopKetto;
    }

    public int getNoKorcsopKetto() {
        return noKorcsopKetto;
    }

    public void setNoKorcsopKetto(int noKorcsopKetto) {
        this.noKorcsopKetto = noKorcsopKetto;
    }

    public int getFfiKorcsopHarom() {
        return ffiKorcsopHarom;
    }

    public void setFfiKorcsopHarom(int ffiKorcsopHarom) {
        this.ffiKorcsopHarom = ffiKorcsopHarom;
    }

    public int getNoKorcsopHarom() {
        return noKorcsopHarom;
    }

    public void setNoKorcsopHarom(int noKorcsopHarom) {
        this.noKorcsopHarom = noKorcsopHarom;
    }

    public int getFfiKorcsopNegy() {
        return ffiKorcsopNegy;
    }

    public void setFfiKorcsopNegy(int ffiKorcsopNegy) {
        this.ffiKorcsopNegy = ffiKorcsopNegy;
    }

    public int getNoKorcsopNegy() {
        return noKorcsopNegy;
    }

    public void setNoKorcsopNegy(int noKorcsopNegy) {
        this.noKorcsopNegy = noKorcsopNegy;
    }

    public int getMinSuly() {
        return minSuly;
    }

    public void setMinSuly(int minSuly) {
        this.minSuly = minSuly;
    }

}
