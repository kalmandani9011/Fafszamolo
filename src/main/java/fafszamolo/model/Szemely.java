package fafszamolo.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * A személy adatai tárolására szolgáló osztály.
 */
public class Szemely {

    private Integer sorszam;
    private int sztsz;
    private String nev;
    private Rendfokozat rf;
    private String alakulat;
    private LocalDate szulDatum;
    private String anyjaNeve;
    private boolean ferfi;
    private int magassag;
    private int suly;
    private double testZsir;
    private List<FizikaiAllapotFelmeres> felmeresek;

    public Szemely() {
    }

    public Szemely(int sztsz, String nev, Rendfokozat rf, String alakulat, LocalDate szulDatum, String anyjaNeve, boolean ferfi, int magassag,
            int suly, double testZsir) {
        this.sztsz = sztsz;
        this.nev = nev;
        this.rf = rf;
        this.alakulat = alakulat;
        this.szulDatum = szulDatum;
        this.anyjaNeve = anyjaNeve;
        this.ferfi = ferfi;
        this.magassag = magassag;
        this.suly = suly;
        this.testZsir = testZsir;
        this.felmeresek = new ArrayList<>();
    }

    public Szemely(Integer sorszam, int sztsz, String nev, Rendfokozat rf, String alakulat, LocalDate szulDatum, String anyjaNeve, boolean ferfi,
            int magassag, int suly, double testZsir) {
        this.sorszam = sorszam;
        this.sztsz = sztsz;
        this.nev = nev;
        this.rf = rf;
        this.alakulat = alakulat;
        this.szulDatum = szulDatum;
        this.anyjaNeve = anyjaNeve;
        this.ferfi = ferfi;
        this.magassag = magassag;
        this.suly = suly;
        this.testZsir = testZsir;
        this.felmeresek = new ArrayList<>();
    }

    public Integer getSorszam() {
        return sorszam;
    }

    public void setSorszam(Integer sorszam) {
        this.sorszam = sorszam;
    }

    /**
     * A személy korának meghatározása.
     */
    public int getKor() {
        if (szulDatum != null) {
            return LocalDate.now().getYear() - szulDatum.getYear();
        } else {
            return 0;
        }
    }

    public int getSztsz() {
        return sztsz;
    }

    public void setSztsz(int sztsz) {
        this.sztsz = sztsz;
    }

    public String getAlakulat() {
        return alakulat;
    }

    public void setAlakulat(String alakulat) {
        this.alakulat = alakulat;
    }

    public String getNev() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }

    public Rendfokozat getRf() {
        return rf;
    }

    public void setRf(Rendfokozat rf) {
        this.rf = rf;
    }

    public LocalDate getSzulDatum() {
        return szulDatum;
    }

    public void setSzulDatum(LocalDate szulDatum) {
        this.szulDatum = szulDatum;
    }

    public String getAnyjaNeve() {
        return anyjaNeve;
    }

    public void setAnyjaNeve(String anyjaNeve) {
        this.anyjaNeve = anyjaNeve;
    }

    public boolean isFerfi() {
        return ferfi;
    }

    public void setIsFerfi(boolean ferfi) {
        this.ferfi = ferfi;
    }

    public int getMagassag() {
        return magassag;
    }

    public void setMagassag(int magassag) {
        this.magassag = magassag;
    }

    public int getSuly() {
        return suly;
    }

    public void setSuly(int suly) {
        this.suly = suly;
    }

    public double getTestZsir() {
        return testZsir;
    }

    public void setTestZsir(double testZsir) {
        this.testZsir = testZsir;
    }

    public List<FizikaiAllapotFelmeres> getFelmeresek() {
        return felmeresek;
    }

    public void setFelmeresek(List<FizikaiAllapotFelmeres> felmeresek) {
        this.felmeresek = felmeresek;
    }

    @Override
    public String toString() {
        if (rf != null) {
            return String.format("%-4d", sorszam) + "| " + sztsz + " | " + String.format("%-40s", nev + " " + rf.getTeljesNev());
        } else return "";
    }
}
