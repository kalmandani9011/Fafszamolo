package fafszamolo.model;

/**
 *A lapocka emelés mozgásforma osztálya.
 */
public class LapockaEmeles extends TorzsMozgasformak {
    
    public LapockaEmeles(){}
    
    public LapockaEmeles(int ismetles, int ffiKorcsopEgy, int noKorcsopEgy, int ffiKorcsopKetto, int noKorcsopKetto, int ffiKorcsopHarom, 
            int noKorcsopHarom, int ffiKorcsopNegy, int noKorcsopNegy, int ffiKorcsopOt, int noKorcsopOt, int ffiKorcsopHat, int noKorcsopHat, 
            int ffiKorcsopHet, int noKorcsopHet, int ffiKorcsopNyolc, int noKorcsopNyolc, int ffiKorcsopKilenc, int noKorcsopKilenc) {
        super(ismetles, ffiKorcsopEgy, noKorcsopEgy, ffiKorcsopKetto, noKorcsopKetto, ffiKorcsopHarom, noKorcsopHarom, ffiKorcsopNegy, noKorcsopNegy,
                ffiKorcsopOt, noKorcsopOt, ffiKorcsopHat, noKorcsopHat, ffiKorcsopHet, noKorcsopHet, ffiKorcsopNyolc, noKorcsopNyolc, ffiKorcsopKilenc,
                noKorcsopKilenc);
    }
}