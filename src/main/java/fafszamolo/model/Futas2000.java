package fafszamolo.model;

/**
 *A 2000 m-es futás mozgásforma osztálya.
 */
public class Futas2000 extends KeringesMozgasformak {

    private int ido;

    public Futas2000(){}
    
    public Futas2000(int ido, int ffiKorcsopHat, int noKorcsopHat, int ffiKorcsopHet, int noKorcsopHet, int ffiKorcsopNyolc, int noKorcsopNyolc,
            int ffiKorcsopKilenc, int noKorcsopKilenc) {
        super(ffiKorcsopHat, noKorcsopHat, ffiKorcsopHet, noKorcsopHet, ffiKorcsopNyolc, noKorcsopNyolc, ffiKorcsopKilenc, noKorcsopKilenc);
        this.ido = ido;
    }

    /**Visszaadja a korcsoportnak megfelelő értéket.
     * @param korcsoport Ez alapján azonosítja be a megfelelő korcsoportot.
     */
    @Override
    public int getKorcsoport(String korcsoport) {
        switch (korcsoport) {
            case "ffiKorcsopHat":
                return this.ffiKorcsopHat;
            case "ffiKorcsopHet":
                return this.ffiKorcsopHet;
            case "ffiKorcsopNyolc":
                return this.ffiKorcsopNyolc;
            case "ffiKorcsopKilenc":
                return this.ffiKorcsopKilenc;
            case "noKorcsopHat":
                return this.noKorcsopHat;
            case "noKorcsopHet":
                return this.noKorcsopHet;
            case "noKorcsopNyolc":
                return this.noKorcsopNyolc;
            case "noKorcsopKilenc":
                return this.noKorcsopKilenc;
        }
        return 0;
    }
    
    public int getIdo() {
        return ido;
    }

    public void setIdo(int ido) {
        this.ido = ido;
    }
}