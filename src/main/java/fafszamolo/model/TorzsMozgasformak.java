package fafszamolo.model;

/**
 * Törzs mozgásformák közös absztrakt osztálya.
 */
public abstract class TorzsMozgasformak {
    
    protected int ismetles;
    protected int ffiKorcsopEgy;
    protected int noKorcsopEgy;
    protected int ffiKorcsopKetto;
    protected int noKorcsopKetto;
    protected int ffiKorcsopHarom;
    protected int noKorcsopHarom;
    protected int ffiKorcsopNegy;
    protected int noKorcsopNegy;
    protected int ffiKorcsopOt;
    protected int noKorcsopOt;
    protected int ffiKorcsopHat;
    protected int noKorcsopHat;
    protected int ffiKorcsopHet;
    protected int noKorcsopHet;
    protected int ffiKorcsopNyolc;
    protected int noKorcsopNyolc;
    protected int ffiKorcsopKilenc;
    protected int noKorcsopKilenc;
    
    public TorzsMozgasformak(){}
    
    /**
     * A felülés, a lapocka emelés és a függő térdemelés mozgásformák előállítására.
     */
    public TorzsMozgasformak(int ismetles, int ffiKorcsopEgy, int noKorcsopEgy, int ffiKorcsopKetto, int noKorcsopKetto, int ffiKorcsopHarom, int noKorcsopHarom,
            int ffiKorcsopNegy, int noKorcsopNegy, int ffiKorcsopOt, int noKorcsopOt, int ffiKorcsopHat, int noKorcsopHat, int ffiKorcsopHet,
            int noKorcsopHet, int ffiKorcsopNyolc, int noKorcsopNyolc, int ffiKorcsopKilenc, int noKorcsopKilenc) {
        this.ismetles = ismetles;
        this.ffiKorcsopEgy = ffiKorcsopEgy;
        this.noKorcsopEgy = noKorcsopEgy;
        this.ffiKorcsopKetto = ffiKorcsopKetto;
        this.noKorcsopKetto = noKorcsopKetto;
        this.ffiKorcsopHarom = ffiKorcsopHarom;
        this.noKorcsopHarom = noKorcsopHarom;
        this.ffiKorcsopNegy = ffiKorcsopNegy;
        this.noKorcsopNegy = noKorcsopNegy;
        this.ffiKorcsopOt = ffiKorcsopOt;
        this.noKorcsopOt = noKorcsopOt;
        this.ffiKorcsopHat = ffiKorcsopHat;
        this.noKorcsopHat = noKorcsopHat;
        this.ffiKorcsopHet = ffiKorcsopHet;
        this.noKorcsopHet = noKorcsopHet;
        this.ffiKorcsopNyolc = ffiKorcsopNyolc;
        this.noKorcsopNyolc = noKorcsopNyolc;
        this.ffiKorcsopKilenc = ffiKorcsopKilenc;
        this.noKorcsopKilenc = noKorcsopKilenc;
    }
    
    /**Visszaadja a korcsoportnak megfelelő értéket.
     * @param korcsoport Ez alapján azonosítja be a megfelelő korcsoportot.
     */
    public int getKorcsoport(String korcsoport) {
        switch (korcsoport) {
            case "ffiKorcsopEgy":
                return this.ffiKorcsopEgy;
            case "ffiKorcsopKetto":
                return this.ffiKorcsopKetto;
            case "ffiKorcsopHarom":
                return this.ffiKorcsopHarom;
            case "ffiKorcsopNegy":
                return this.ffiKorcsopNegy;
            case "ffiKorcsopOt":
                return this.ffiKorcsopOt;
            case "ffiKorcsopHat":
                return this.ffiKorcsopHat;
            case "ffiKorcsopHet":
                return this.ffiKorcsopHet;
            case "ffiKorcsopNyolc":
                return this.ffiKorcsopNyolc;
            case "ffiKorcsopKilenc":
                return this.ffiKorcsopKilenc;
            case "noKorcsopEgy":
                return this.noKorcsopEgy;
            case "noKorcsopKetto":
                return this.noKorcsopKetto;
            case "noKorcsopHarom":
                return this.noKorcsopHarom;
            case "noKorcsopNegy":
                return this.noKorcsopNegy;
            case "noKorcsopOt":
                return this.noKorcsopOt;
            case "noKorcsopHat":
                return this.noKorcsopHat;
            case "noKorcsopHet":
                return this.noKorcsopHet;
            case "noKorcsopNyolc":
                return this.noKorcsopNyolc;
            case "noKorcsopKilenc":
                return this.noKorcsopKilenc;
        }
        return 0;
    }

    public void setIsmetles(int ismetles) {
        this.ismetles = ismetles;
    }
    
    public void setFfiKorcsopEgy(int ffiKorcsopEgy) {
        this.ffiKorcsopEgy = ffiKorcsopEgy;
    }

    public void setNoKorcsopEgy(int noKorcsopEgy) {
        this.noKorcsopEgy = noKorcsopEgy;
    }

    public void setFfiKorcsopKetto(int ffiKorcsopKetto) {
        this.ffiKorcsopKetto = ffiKorcsopKetto;
    }

    public void setNoKorcsopKetto(int noKorcsopKetto) {
        this.noKorcsopKetto = noKorcsopKetto;
    }

    public void setFfiKorcsopHarom(int ffiKorcsopHarom) {
        this.ffiKorcsopHarom = ffiKorcsopHarom;
    }

    public void setNoKorcsopHarom(int noKorcsopHarom) {
        this.noKorcsopHarom = noKorcsopHarom;
    }

    public void setFfiKorcsopNegy(int ffiKorcsopNegy) {
        this.ffiKorcsopNegy = ffiKorcsopNegy;
    }

    public void setNoKorcsopNegy(int noKorcsopNegy) {
        this.noKorcsopNegy = noKorcsopNegy;
    }

    public void setFfiKorcsopOt(int ffiKorcsopOt) {
        this.ffiKorcsopOt = ffiKorcsopOt;
    }

    public void setNoKorcsopOt(int noKorcsopOt) {
        this.noKorcsopOt = noKorcsopOt;
    }

    public void setFfiKorcsopHat(int ffiKorcsopHat) {
        this.ffiKorcsopHat = ffiKorcsopHat;
    }

    public void setNoKorcsopHat(int noKorcsopHat) {
        this.noKorcsopHat = noKorcsopHat;
    }

    public void setFfiKorcsopHet(int ffiKorcsopHet) {
        this.ffiKorcsopHet = ffiKorcsopHet;
    }

    public void setNoKorcsopHet(int noKorcsopHet) {
        this.noKorcsopHet = noKorcsopHet;
    }

    public void setFfiKorcsopNyolc(int ffiKorcsopNyolc) {
        this.ffiKorcsopNyolc = ffiKorcsopNyolc;
    }

    public void setNoKorcsopNyolc(int noKorcsopNyolc) {
        this.noKorcsopNyolc = noKorcsopNyolc;
    }

    public void setFfiKorcsopKilenc(int ffiKorcsopKilenc) {
        this.ffiKorcsopKilenc = ffiKorcsopKilenc;
    }

    public void setNoKorcsopKilenc(int noKorcsopKilenc) {
        this.noKorcsopKilenc = noKorcsopKilenc;
    }

    public int getIsmetles() {
        return ismetles;
    }
    
    public int getFfiKorcsopEgy() {
        return ffiKorcsopEgy;
    }

    public int getNoKorcsopEgy() {
        return noKorcsopEgy;
    }

    public int getFfiKorcsopKetto() {
        return ffiKorcsopKetto;
    }

    public int getNoKorcsopKetto() {
        return noKorcsopKetto;
    }

    public int getFfiKorcsopHarom() {
        return ffiKorcsopHarom;
    }

    public int getNoKorcsopHarom() {
        return noKorcsopHarom;
    }

    public int getFfiKorcsopNegy() {
        return ffiKorcsopNegy;
    }

    public int getNoKorcsopNegy() {
        return noKorcsopNegy;
    }

    public int getFfiKorcsopOt() {
        return ffiKorcsopOt;
    }

    public int getNoKorcsopOt() {
        return noKorcsopOt;
    }

    public int getFfiKorcsopHat() {
        return ffiKorcsopHat;
    }

    public int getNoKorcsopHat() {
        return noKorcsopHat;
    }

    public int getFfiKorcsopHet() {
        return ffiKorcsopHet;
    }

    public int getNoKorcsopHet() {
        return noKorcsopHet;
    }

    public int getFfiKorcsopNyolc() {
        return ffiKorcsopNyolc;
    }

    public int getNoKorcsopNyolc() {
        return noKorcsopNyolc;
    }

    public int getFfiKorcsopKilenc() {
        return ffiKorcsopKilenc;
    }

    public int getNoKorcsopKilenc() {
        return noKorcsopKilenc;
    }
}
