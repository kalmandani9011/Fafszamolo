package fafszamolo.model;

/**
 *A 3200 m-es futás mozgásforma osztálya.
 */
public class Futas3200 extends KeringesMozgasformak {
    
    private int ido;

    
    public Futas3200() {}
    
    public Futas3200(int ido, int ffiKorcsopEgy, int noKorcsopEgy, int ffiKorcsopKetto, int noKorcsopKetto, int ffiKorcsopHarom, int noKorcsopHarom,
            int ffiKorcsopNegy, int noKorcsopNegy, int ffiKorcsopOt, int noKorcsopOt) {
        super(ffiKorcsopEgy, noKorcsopEgy, ffiKorcsopKetto, noKorcsopKetto, ffiKorcsopHarom, noKorcsopHarom, ffiKorcsopNegy, noKorcsopNegy, 
                ffiKorcsopOt, noKorcsopOt);
        this.ido = ido;
    }

    /**Visszaadja a korcsoportnak megfelelő értéket.
     * @param korcsoport Ez alapján azonosítja be a megfelelő korcsoportot.
     */
    @Override
    public int getKorcsoport(String korcsoport) {
        switch (korcsoport) {
            case "ffiKorcsopEgy":
                return this.ffiKorcsopEgy;
            case "ffiKorcsopKetto":
                return this.ffiKorcsopKetto;
            case "ffiKorcsopHarom":
                return this.ffiKorcsopHarom;
            case "ffiKorcsopNegy":
                return this.ffiKorcsopNegy;
            case "ffiKorcsopOt":
                return this.ffiKorcsopOt;
            case "noKorcsopEgy":
                return this.noKorcsopEgy;
            case "noKorcsopKetto":
                return this.noKorcsopKetto;
            case "noKorcsopHarom":
                return this.noKorcsopHarom;
            case "noKorcsopNegy":
                return this.noKorcsopNegy;
            case "noKorcsopOt":
                return this.noKorcsopOt;
        }
        return 0;
    }
    
    public int getIdo() {
        return ido;
    }

    public void setIdo(int ido) {
        this.ido = ido;
    }
}