package fafszamolo.model;

/**
 *Ergometria mozgásforma osztálya.
 */
public class Ergometria extends KeringesMozgasformak {
    
    private int wattKg;

    public Ergometria(){}
    
    public Ergometria(int wattKg, int ffiKorcsopEgy, int noKorcsopEgy, int ffiKorcsopKetto, int noKorcsopKetto, int ffiKorcsopHarom, 
            int noKorcsopHarom, int ffiKorcsopNegy, int noKorcsopNegy, int ffiKorcsopOt, int noKorcsopOt, int ffiKorcsopHat, int noKorcsopHat, 
            int ffiKorcsopHet, int noKorcsopHet) {
        super(ffiKorcsopEgy, noKorcsopEgy, ffiKorcsopKetto, noKorcsopKetto, ffiKorcsopHarom, noKorcsopHarom, ffiKorcsopNegy, noKorcsopNegy,
                ffiKorcsopOt, noKorcsopOt, ffiKorcsopHat, noKorcsopHat, ffiKorcsopHet, noKorcsopHet);
        this.wattKg = wattKg;
    }

    /**Visszaadja a korcsoportnak megfelelő értéket.
     * @param korcsoport Ez alapján azonosítja be a megfelelő korcsoportot.
     */
    @Override
    public int getKorcsoport(String korcsoport) {
        switch (korcsoport) {
            case "ffiKorcsopEgy":
                return this.ffiKorcsopEgy;
            case "ffiKorcsopKetto":
                return this.ffiKorcsopKetto;
            case "ffiKorcsopHarom":
                return this.ffiKorcsopHarom;
            case "ffiKorcsopNegy":
                return this.ffiKorcsopNegy;
            case "ffiKorcsopOt":
                return this.ffiKorcsopOt;
            case "ffiKorcsopHat":
                return this.ffiKorcsopHat;
            case "ffiKorcsopHet":
                return this.ffiKorcsopHet;
            case "noKorcsopEgy":
                return this.noKorcsopEgy;
            case "noKorcsopKetto":
                return this.noKorcsopKetto;
            case "noKorcsopHarom":
                return this.noKorcsopHarom;
            case "noKorcsopNegy":
                return this.noKorcsopNegy;
            case "noKorcsopOt":
                return this.noKorcsopOt;
            case "noKorcsopHat":
                return this.noKorcsopHat;
            case "noKorcsopHet":
                return this.noKorcsopHet;
        }
        return 0;
    }
    
    public int getWattKg() {
        return wattKg;
    }

    public void setWattKg(int wattKg) {
        this.wattKg = wattKg;
    }
}