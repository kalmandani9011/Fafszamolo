package fafszamolo.model;

/**
 *A Magyar Honvédségben használatos rendfokozatok.
 */
public enum Rendfokozat {
    
    KOZKATONA("kk."){
        @Override
        public String getTeljesNev() {
            return "közkatona";
        }
    }, ORVEZETO("őrv."){
        @Override
        public String getTeljesNev() {
            return "őrvezető";
        }
    }, TIZEDES("tiz."){
        @Override
        public String getTeljesNev() {
            return "tizedes";
        }
    }, SZAKASZVEZETO("szkv."){
        @Override
        public String getTeljesNev() {
            return "szakaszvezető";
        }
    }, ORMESTER("őrm."){
        @Override
        public String getTeljesNev() {
            return "őrmester";
        }
    }, TORZSORMESTER("tőrm."){
        @Override
        public String getTeljesNev() {
            return "törzsőrmester";
        }
    }, FOTORZSORMESTER("ftőrm."){
        @Override
        public String getTeljesNev() {
            return "főtörzsőrmester";
        }
    }, ZASZLOS("zls."){
        @Override
        public String getTeljesNev() {
            return "zászlós";
        }
    }, TORZSZASZLOS("tzls."){
        @Override
        public String getTeljesNev() {
            return "törzszászlós";
        }
    }, FOTORZSZASZLOS("ftzls."){
        @Override
        public String getTeljesNev() {
            return "főtörzszászlós";
        }
    }, HADNAGY("hdgy."){
        @Override
        public String getTeljesNev() {
            return "hadnagy";
        }
    }, FOHADNAGY("fhdgy."){
        @Override
        public String getTeljesNev() {
            return "főhadnagy";
        }
    }, SZAZADOS("szds."){
        @Override
        public String getTeljesNev() {
            return "százados";
        }
    }, ORNAGY("őrgy."){
        @Override
        public String getTeljesNev() {
            return "őrnagy";
        }
    }, ALEZREDES("alez."){
        @Override
        public String getTeljesNev() {
            return "alezredes";
        }
    }, EZREDES("ezds."){
        @Override
        public String getTeljesNev() {
            return "ezredes";
        }
    }, DANDARTABORNOK("ddtbk."){
        @Override
        public String getTeljesNev() {
            return "dandártábornok";
        }
    }, VEZERORNAGY("vőrgy."){
        @Override
        public String getTeljesNev() {
            return "vezérőrnagy";
        }
    }, ALTABORNAGY("altbgy."){
        @Override
        public String getTeljesNev() {
            return "altábornagy";
        }
    }, VEZEREZREDES("vezds."){
        @Override
        public String getTeljesNev() {
            return "vezérezredes";
        }
    };
    
    private String rovidites;
    
    Rendfokozat(String rovidites) {
        this.rovidites = rovidites;
    }
    
    public String getRovidites() {
        return this.rovidites;
    }
    
    public abstract String getTeljesNev();
    
    @Override
    public String toString() {
        return getTeljesNev();
    }
    
    /**
     * A rendfokozatnak megfelelő állománykategória meghatározására szolgál.
     */
    public String getAllomanyKategoria() {
        switch (this.toString()) {
            case "KOZKATONA": return "legénység";
            case "ORVEZETO": return "legénység";
            case "TIZEDES": return "legénység";
            case "SZAKASZVEZETO": return "legénység";
            case "ORMESTER": return "altiszt";
            case "TORZSORMESTER": return "altiszt";
            case "FOTORZSORMESTER": return "altiszt";
            case "ZASZLOS": return "altiszt";
            case "TORZSZASZLOS": return "altiszt";
            case "FOTORZSZASZLOS": return "altiszt";
            case "HADNAGY": return "tiszt";
            case "FOHADNAGY": return "tiszt";
            case "SZAZADOS": return "tiszt";
            case "ORNAGY": return "főtiszt";
            case "ALEZREDES": return "főtiszt";
            case "EZREDES": return "főtiszt";
            case "DANDARTABORNOK": return "főtiszt";
            case "VEZERORNAGY": return "főtiszt";
            case "ALTABORNAGY": return "főtiszt";
            case "VEZEREZREDES": return "főtiszt";
        }
        return null;
    }
    
}
