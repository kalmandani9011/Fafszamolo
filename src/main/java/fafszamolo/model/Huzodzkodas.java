package fafszamolo.model;

/**
 *A húzódzkodás mozgásforma osztálya.
 */
public class Huzodzkodas extends KarMozgasformak {

    private int ismetles;
    
    public Huzodzkodas(){}
    
    public Huzodzkodas(int ismetles, int ffiKorcsopEgy, int noKorcsopEgy, int ffiKorcsopKetto, int noKorcsopKetto, int ffiKorcsopHarom, 
            int noKorcsopHarom, int ffiKorcsopNegy, int noKorcsopNegy, int ffiKorcsopOt, int noKorcsopOt, int ffiKorcsopHat, int noKorcsopHat, 
            int ffiKorcsopHet, int noKorcsopHet, int ffiKorcsopNyolc, int noKorcsopNyolc, int ffiKorcsopKilenc, int noKorcsopKilenc) {
        super(ffiKorcsopEgy, noKorcsopEgy, ffiKorcsopKetto, noKorcsopKetto, ffiKorcsopHarom, noKorcsopHarom, ffiKorcsopNegy, noKorcsopNegy,
                ffiKorcsopOt, noKorcsopOt, ffiKorcsopHat, noKorcsopHat, ffiKorcsopHet, noKorcsopHet, ffiKorcsopNyolc, noKorcsopNyolc, ffiKorcsopKilenc,
                noKorcsopKilenc);
        this.ismetles = ismetles;
    }

    public int getIsmetles() {
        return ismetles;
    }

    public void setIsmetles(int ismetles) {
        this.ismetles = ismetles;
    }
}