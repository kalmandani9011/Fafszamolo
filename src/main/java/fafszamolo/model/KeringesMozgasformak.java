package fafszamolo.model;

/**
 * Keringés mozgásformák közös absztrakt osztálya.
 */
public abstract class KeringesMozgasformak {
    
    protected int ffiKorcsopEgy;
    protected int noKorcsopEgy;
    protected int ffiKorcsopKetto;
    protected int noKorcsopKetto;
    protected int ffiKorcsopHarom;
    protected int noKorcsopHarom;
    protected int ffiKorcsopNegy;
    protected int noKorcsopNegy;
    protected int ffiKorcsopOt;
    protected int noKorcsopOt;
    protected int ffiKorcsopHat;
    protected int noKorcsopHat;
    protected int ffiKorcsopHet;
    protected int noKorcsopHet;
    protected int ffiKorcsopNyolc;
    protected int noKorcsopNyolc;
    protected int ffiKorcsopKilenc;
    protected int noKorcsopKilenc;

    public KeringesMozgasformak(){}
    
    /**
     * 3200 m futás előállítására.
     */
    public KeringesMozgasformak(int ffiKorcsopEgy, int noKorcsopEgy, int ffiKorcsopKetto, int noKorcsopKetto, int ffiKorcsopHarom, 
            int noKorcsopHarom, int ffiKorcsopNegy, int noKorcsopNegy, int ffiKorcsopOt, int noKorcsopOt) {
        this.ffiKorcsopEgy = ffiKorcsopEgy;
        this.noKorcsopEgy = noKorcsopEgy;
        this.ffiKorcsopKetto = ffiKorcsopKetto;
        this.noKorcsopKetto = noKorcsopKetto;
        this.ffiKorcsopHarom = ffiKorcsopHarom;
        this.noKorcsopHarom = noKorcsopHarom;
        this.ffiKorcsopNegy = ffiKorcsopNegy;
        this.noKorcsopNegy = noKorcsopNegy;
        this.ffiKorcsopOt = ffiKorcsopOt;
        this.noKorcsopOt = noKorcsopOt;
    }

    /**
     * 2000 m futás előállítására.
     */
    public KeringesMozgasformak(int ffiKorcsopHat, int noKorcsopHat, int ffiKorcsopHet, int noKorcsopHet, int ffiKorcsopNyolc, int noKorcsopNyolc, 
            int ffiKorcsopKilenc, int noKorcsopKilenc) {
        this.ffiKorcsopHat = ffiKorcsopHat;
        this.noKorcsopHat = noKorcsopHat;
        this.ffiKorcsopHet = ffiKorcsopHet;
        this.noKorcsopHet = noKorcsopHet;
        this.ffiKorcsopNyolc = ffiKorcsopNyolc;
        this.noKorcsopNyolc = noKorcsopNyolc;
        this.ffiKorcsopKilenc = ffiKorcsopKilenc;
        this.noKorcsopKilenc = noKorcsopKilenc;
    }
    
    /**
     *  1600 m menet előállítására.
     */
    public KeringesMozgasformak(int ffiKorcsopEgy, int noKorcsopEgy, int ffiKorcsopKetto, int noKorcsopKetto, int ffiKorcsopHarom, 
            int noKorcsopHarom, int ffiKorcsopNegy, int noKorcsopNegy, int ffiKorcsopOt, int noKorcsopOt, int ffiKorcsopHat, int noKorcsopHat,
            int ffiKorcsopHet, int noKorcsopHet, int ffiKorcsopNyolc, int noKorcsopNyolc, int ffiKorcsopKilenc, int noKorcsopKilenc) {
        this.ffiKorcsopEgy = ffiKorcsopEgy;
        this.noKorcsopEgy = noKorcsopEgy;
        this.ffiKorcsopKetto = ffiKorcsopKetto;
        this.noKorcsopKetto = noKorcsopKetto;
        this.ffiKorcsopHarom = ffiKorcsopHarom;
        this.noKorcsopHarom = noKorcsopHarom;
        this.ffiKorcsopNegy = ffiKorcsopNegy;
        this.noKorcsopNegy = noKorcsopNegy;
        this.ffiKorcsopOt = ffiKorcsopOt;
        this.noKorcsopOt = noKorcsopOt;
        this.ffiKorcsopHat = ffiKorcsopHat;
        this.noKorcsopHat = noKorcsopHat;
        this.ffiKorcsopHet = ffiKorcsopHet;
        this.noKorcsopHet = noKorcsopHet;
        this.ffiKorcsopNyolc = ffiKorcsopNyolc;
        this.noKorcsopNyolc = noKorcsopNyolc;
        this.ffiKorcsopKilenc = ffiKorcsopKilenc;
        this.noKorcsopKilenc = noKorcsopKilenc;
    }
    
    /**
     * Ergometria előállítására.
     */
    public KeringesMozgasformak(int ffiKorcsopEgy, int noKorcsopEgy, int ffiKorcsopKetto, int noKorcsopKetto, int ffiKorcsopHarom, 
            int noKorcsopHarom, int ffiKorcsopNegy, int noKorcsopNegy, int ffiKorcsopOt, int noKorcsopOt, int ffiKorcsopHat, int noKorcsopHat,
            int ffiKorcsopHet, int noKorcsopHet) {
        this.ffiKorcsopEgy = ffiKorcsopEgy;
        this.noKorcsopEgy = noKorcsopEgy;
        this.ffiKorcsopKetto = ffiKorcsopKetto;
        this.noKorcsopKetto = noKorcsopKetto;
        this.ffiKorcsopHarom = ffiKorcsopHarom;
        this.noKorcsopHarom = noKorcsopHarom;
        this.ffiKorcsopNegy = ffiKorcsopNegy;
        this.noKorcsopNegy = noKorcsopNegy;
        this.ffiKorcsopOt = ffiKorcsopOt;
        this.noKorcsopOt = noKorcsopOt;
        this.ffiKorcsopHat = ffiKorcsopHat;
        this.noKorcsopHat = noKorcsopHat;
        this.ffiKorcsopHet = ffiKorcsopHet;
        this.noKorcsopHet = noKorcsopHet;
    }
    
    public abstract int getKorcsoport(String korcsoport);

    public int getFfiKorcsopEgy() {
        return ffiKorcsopEgy;
    }

    public void setFfiKorcsopEgy(int ffiKorcsopEgy) {
        this.ffiKorcsopEgy = ffiKorcsopEgy;
    }

    public int getNoKorcsopEgy() {
        return noKorcsopEgy;
    }

    public void setNoKorcsopEgy(int noKorcsopEgy) {
        this.noKorcsopEgy = noKorcsopEgy;
    }

    public int getFfiKorcsopKetto() {
        return ffiKorcsopKetto;
    }

    public void setFfiKorcsopKetto(int ffiKorcsopKetto) {
        this.ffiKorcsopKetto = ffiKorcsopKetto;
    }

    public int getNoKorcsopKetto() {
        return noKorcsopKetto;
    }

    public void setNoKorcsopKetto(int noKorcsopKetto) {
        this.noKorcsopKetto = noKorcsopKetto;
    }

    public int getFfiKorcsopHarom() {
        return ffiKorcsopHarom;
    }

    public void setFfiKorcsopHarom(int ffiKorcsopHarom) {
        this.ffiKorcsopHarom = ffiKorcsopHarom;
    }

    public int getNoKorcsopHarom() {
        return noKorcsopHarom;
    }

    public void setNoKorcsopHarom(int noKorcsopHarom) {
        this.noKorcsopHarom = noKorcsopHarom;
    }

    public int getFfiKorcsopNegy() {
        return ffiKorcsopNegy;
    }

    public void setFfiKorcsopNegy(int ffiKorcsopNegy) {
        this.ffiKorcsopNegy = ffiKorcsopNegy;
    }

    public int getNoKorcsopNegy() {
        return noKorcsopNegy;
    }

    public void setNoKorcsopNegy(int noKorcsopNegy) {
        this.noKorcsopNegy = noKorcsopNegy;
    }

    public int getFfiKorcsopOt() {
        return ffiKorcsopOt;
    }

    public void setFfiKorcsopOt(int ffiKorcsopOt) {
        this.ffiKorcsopOt = ffiKorcsopOt;
    }

    public int getNoKorcsopOt() {
        return noKorcsopOt;
    }

    public void setNoKorcsopOt(int noKorcsopOt) {
        this.noKorcsopOt = noKorcsopOt;
    }

    public int getFfiKorcsopHat() {
        return ffiKorcsopHat;
    }

    public void setFfiKorcsopHat(int ffiKorcsopHat) {
        this.ffiKorcsopHat = ffiKorcsopHat;
    }

    public int getNoKorcsopHat() {
        return noKorcsopHat;
    }

    public void setNoKorcsopHat(int noKorcsopHat) {
        this.noKorcsopHat = noKorcsopHat;
    }

    public int getFfiKorcsopHet() {
        return ffiKorcsopHet;
    }

    public void setFfiKorcsopHet(int ffiKorcsopHet) {
        this.ffiKorcsopHet = ffiKorcsopHet;
    }

    public int getNoKorcsopHet() {
        return noKorcsopHet;
    }

    public void setNoKorcsopHet(int noKorcsopHet) {
        this.noKorcsopHet = noKorcsopHet;
    }

    public int getFfiKorcsopNyolc() {
        return ffiKorcsopNyolc;
    }

    public void setFfiKorcsopNyolc(int ffiKorcsopNyolc) {
        this.ffiKorcsopNyolc = ffiKorcsopNyolc;
    }

    public int getNoKorcsopNyolc() {
        return noKorcsopNyolc;
    }

    public void setNoKorcsopNyolc(int noKorcsopNyolc) {
        this.noKorcsopNyolc = noKorcsopNyolc;
    }

    public int getFfiKorcsopKilenc() {
        return ffiKorcsopKilenc;
    }

    public void setFfiKorcsopKilenc(int ffiKorcsopKilenc) {
        this.ffiKorcsopKilenc = ffiKorcsopKilenc;
    }

    public int getNoKorcsopKilenc() {
        return noKorcsopKilenc;
    }

    public void setNoKorcsopKilenc(int noKorcsopKilenc) {
        this.noKorcsopKilenc = noKorcsopKilenc;
    }
}
