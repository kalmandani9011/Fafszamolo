package fafszamolo.model;

/**
 *Az 1600 m-es menet mozgásforma osztálya.
 */
public class Menet1600 extends KeringesMozgasformak {
    
    private int ido;

    public Menet1600(){}
    
    public Menet1600(int ido, int ffiKorcsopEgy, int noKorcsopEgy, int ffiKorcsopKetto, int noKorcsopKetto, int ffiKorcsopHarom, int noKorcsopHarom, 
            int ffiKorcsopNegy, int noKorcsopNegy, int ffiKorcsopOt, int noKorcsopOt, int ffiKorcsopHat, int noKorcsopHat, int ffiKorcsopHet, 
            int noKorcsopHet, int ffiKorcsopNyolc, int noKorcsopNyolc, int ffiKorcsopKilenc, int noKorcsopKilenc) {
        super(ffiKorcsopEgy, noKorcsopEgy, ffiKorcsopKetto, noKorcsopKetto, ffiKorcsopHarom, noKorcsopHarom, ffiKorcsopNegy, noKorcsopNegy,
                ffiKorcsopOt, noKorcsopOt, ffiKorcsopHat, noKorcsopHat, ffiKorcsopHet, noKorcsopHet, ffiKorcsopNyolc, noKorcsopNyolc, ffiKorcsopKilenc,
                noKorcsopKilenc);
        this.ido = ido;
    }
    
    /**Visszaadja a korcsoportnak megfelelő értéket.
     * @param korcsoport Ez alapján azonosítja be a megfelelő korcsoportot.
     */
    @Override
    public int getKorcsoport(String korcsoport) {
        switch (korcsoport) {
            case "ffiKorcsopEgy":
                return this.ffiKorcsopEgy;
            case "ffiKorcsopKetto":
                return this.ffiKorcsopKetto;
            case "ffiKorcsopHarom":
                return this.ffiKorcsopHarom;
            case "ffiKorcsopNegy":
                return this.ffiKorcsopNegy;
            case "ffiKorcsopOt":
                return this.ffiKorcsopOt;
            case "ffiKorcsopHat":
                return this.ffiKorcsopHat;
            case "ffiKorcsopHet":
                return this.ffiKorcsopHet;
            case "ffiKorcsopNyolc":
                return this.ffiKorcsopNyolc;
            case "ffiKorcsopKilenc":
                return this.ffiKorcsopKilenc;
            case "noKorcsopEgy":
                return this.noKorcsopEgy;
            case "noKorcsopKetto":
                return this.noKorcsopKetto;
            case "noKorcsopHarom":
                return this.noKorcsopHarom;
            case "noKorcsopNegy":
                return this.noKorcsopNegy;
            case "noKorcsopOt":
                return this.noKorcsopOt;
            case "noKorcsopHat":
                return this.noKorcsopHat;
            case "noKorcsopHet":
                return this.noKorcsopHet;
            case "noKorcsopNyolc":
                return this.noKorcsopNyolc;
            case "noKorcsopKilenc":
                return this.noKorcsopKilenc;
        }
        return 0;
    }

    public int getIdo() {
        return ido;
    }

    public void setIdo(int ido) {
        this.ido = ido;
    }

}