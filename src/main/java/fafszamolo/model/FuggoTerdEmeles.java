package fafszamolo.model;

/**
 *A függő térdemelés mozgásforma osztálya.
 */
public class FuggoTerdEmeles extends TorzsMozgasformak {

    public FuggoTerdEmeles(){}
    
    public FuggoTerdEmeles(int ismetles, int ffiKorcsopEgy, int noKorcsopEgy, int ffiKorcsopKetto, int noKorcsopKetto, int ffiKorcsopHarom,
            int noKorcsopHarom, int ffiKorcsopNegy, int noKorcsopNegy, int ffiKorcsopOt, int noKorcsopOt, int ffiKorcsopHat, int noKorcsopHat,
            int ffiKorcsopHet, int noKorcsopHet, int ffiKorcsopNyolc, int noKorcsopNyolc, int ffiKorcsopKilenc, int noKorcsopKilenc) {
        super(ismetles, ffiKorcsopEgy, noKorcsopEgy, ffiKorcsopKetto, noKorcsopKetto, ffiKorcsopHarom, noKorcsopHarom, ffiKorcsopNegy, noKorcsopNegy,
                ffiKorcsopOt, noKorcsopOt, ffiKorcsopHat, noKorcsopHat, ffiKorcsopHet, noKorcsopHet, ffiKorcsopNyolc, noKorcsopNyolc, ffiKorcsopKilenc,
                noKorcsopKilenc);
    }
}