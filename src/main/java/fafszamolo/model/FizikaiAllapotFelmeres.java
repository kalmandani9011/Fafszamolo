package fafszamolo.model;

import java.time.LocalDate;

/**
 *A fizikai állapotfelmérés osztálya. Többek között tartalmazza a felmérés végrehajtás idejét, a végrehajtott mozgásformákat, azok eredményeit
 * valamint az összpontszámot.
 */
public class FizikaiAllapotFelmeres {
    
    private Integer sorszam;
    private int sztsz;
    private LocalDate vegrehajtasIdeje;
    private int karMozgasforma;
    private int karEredmeny;
    private int torzsMozgasforma;
    private int torzsEredmeny;
    private int keringesMozgasforma;
    private int keringesEredmeny;
    private int pontszam;
    private String bmi;
    private String testZsirIndex;
    
    public FizikaiAllapotFelmeres() {}
    
    public FizikaiAllapotFelmeres(Integer sorszam, int sztsz, LocalDate vegrehajtasIdeje, int karMozgasforma, int karEredmeny, int torzsMozgasforma, int torzsEredmeny, 
            int keringesMozgasforma, int keringesEredmeny) {
        this.sorszam = sorszam;
        this.sztsz = sztsz;
        this.vegrehajtasIdeje = vegrehajtasIdeje;
        this.karMozgasforma = karMozgasforma;
        this.karEredmeny = karEredmeny;
        this.torzsMozgasforma = torzsMozgasforma;
        this.torzsEredmeny = torzsEredmeny;
        this.keringesMozgasforma = keringesMozgasforma;
        this.keringesEredmeny = keringesEredmeny;
    }
    
    public FizikaiAllapotFelmeres(int sztsz, LocalDate vegrehajtasIdeje, int karMozgasforma, int karEredmeny, int torzsMozgasforma, int torzsEredmeny, 
            int keringesMozgasforma, int keringesEredmeny) {
        this.sztsz = sztsz;
        this.vegrehajtasIdeje = vegrehajtasIdeje;
        this.karMozgasforma = karMozgasforma;
        this.karEredmeny = karEredmeny;
        this.torzsMozgasforma = torzsMozgasforma;
        this.torzsEredmeny = torzsEredmeny;
        this.keringesMozgasforma = keringesMozgasforma;
        this.keringesEredmeny = keringesEredmeny;
    }
    
    public FizikaiAllapotFelmeres(Integer sorszam, int sztsz, LocalDate vegrehajtasIdeje, int karMozgasforma, int karEredmeny, int torzsMozgasforma, 
            int torzsEredmeny, int keringesMozgasforma, int keringesEredmeny, int pontszam, String bmi, String testZsirIndex) {
        this.sorszam = sorszam;
        this.sztsz = sztsz;
        this.vegrehajtasIdeje = vegrehajtasIdeje;
        this.karMozgasforma = karMozgasforma;
        this.karEredmeny = karEredmeny;
        this.torzsMozgasforma = torzsMozgasforma;
        this.torzsEredmeny = torzsEredmeny;
        this.keringesMozgasforma = keringesMozgasforma;
        this.keringesEredmeny = keringesEredmeny;
        this.pontszam = pontszam;
        this.bmi = bmi;
        this.testZsirIndex = testZsirIndex;
    }

    public Integer getSorszam() {
        return sorszam;
    }

    public void setSorszam(Integer sorszam) {
        this.sorszam = sorszam;
    }

    public int getSztsz() {
        return sztsz;
    }

    public void setSztsz(int sztsz) {
        this.sztsz = sztsz;
    }

    public LocalDate getVegrehajtasIdeje() {
        return vegrehajtasIdeje;
    }

    public void setVegrehajtasIdeje(LocalDate vegrehajtasIdeje) {
        this.vegrehajtasIdeje = vegrehajtasIdeje;
    }

    public int getKarMozgasforma() {
        return karMozgasforma;
    }

    public void setKarMozgasforma(int karMozgasforma) {
        this.karMozgasforma = karMozgasforma;
    }

    public int getKarEredmeny() {
        return karEredmeny;
    }

    public void setKarEredmeny(int karEredmeny) {
        this.karEredmeny = karEredmeny;
    }

    public int getTorzsMozgasforma() {
        return torzsMozgasforma;
    }

    public void setTorzsMozgasforma(int torzsMozgasforma) {
        this.torzsMozgasforma = torzsMozgasforma;
    }

    public int getTorzsEredmeny() {
        return torzsEredmeny;
    }

    public void setTorzsEredmeny(int torzsEredmeny) {
        this.torzsEredmeny = torzsEredmeny;
    }

    public int getKeringesMozgasforma() {
        return keringesMozgasforma;
    }

    public void setKeringesMozgasforma(int keringesMozgasforma) {
        this.keringesMozgasforma = keringesMozgasforma;
    }

    public int getKeringesEredmeny() {
        return keringesEredmeny;
    }

    public void setKeringesEredmeny(int keringesEredmeny) {
        this.keringesEredmeny = keringesEredmeny;
    }

    public int getPontszam() {
        return pontszam;
    }

    public void setPontszam(int pontszam) {
        this.pontszam = pontszam;
    }

    public String getBmi() {
        return bmi;
    }

    public void setBmi(String bmi) {
        this.bmi = bmi;
    }

    public String getTestZsirIndex() {
        return testZsirIndex;
    }

    public void setTestZsirIndex(String testZsirIndex) {
        this.testZsirIndex = testZsirIndex;
    }
    
    
}
