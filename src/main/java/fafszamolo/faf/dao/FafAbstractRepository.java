package fafszamolo.faf.dao;

import fafszamolo.exception.FafDAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author
 */
public abstract class FafAbstractRepository<T> implements FafRepository<T> {
    
    protected Connection conn;
    protected PreparedStatement findAll;
    protected PreparedStatement insert;
    protected PreparedStatement update;
    protected PreparedStatement delete;
    
    @Override
    public List<T> findAll() throws FafDAOException {
        try {
            ResultSet all = this.findAll.executeQuery();
            List<T> lista = makeList(all);
            return lista;
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
    
    @Override
    public abstract <T> void save(T obj) throws FafDAOException;
    
    @Override
    public abstract <T> void insert(T obj) throws FafDAOException;
    
    @Override
    public abstract <T> void update(T obj) throws FafDAOException;
    
    @Override
    public void delete(int sorszam) throws FafDAOException {
        try {
            this.delete.setInt(1, sorszam);
            this.delete.executeUpdate();
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
    
    @Override
    public List<T> makeList(ResultSet rs) throws FafDAOException {
        try {
            List<T> lista = new ArrayList();
            while (rs.next()) {
                lista.add(makeOne(rs));
            }
            return lista;
        } catch (SQLException ex) {
            throw new FafDAOException(ex.getMessage());
        }
    }
    
    @Override
    public abstract T makeOne(ResultSet rs) throws FafDAOException;
}
