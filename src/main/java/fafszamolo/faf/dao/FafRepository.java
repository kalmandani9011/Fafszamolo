package fafszamolo.faf.dao;

import fafszamolo.exception.FafDAOException;
import java.sql.ResultSet;
import java.util.List;

/**
 *
 * @author
 */
public interface FafRepository<T> {
    
    List<T> findAll() throws FafDAOException;
      
    <T> void save(T obj) throws FafDAOException;
    
    <T> void insert(T obj) throws FafDAOException;
    
    <T> void update(T obj) throws FafDAOException;
    
    void delete(int sorszam) throws FafDAOException;
    
    /**
     * Segédmetódus, ami listát készít.
     */
    List<T> makeList(ResultSet rs) throws FafDAOException;
    
    T makeOne(ResultSet rs) throws FafDAOException;
}
