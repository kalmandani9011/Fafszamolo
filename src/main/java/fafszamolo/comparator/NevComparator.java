package fafszamolo.comparator;

import fafszamolo.model.Szemely;
import java.util.Comparator;

/**
 *
 * @author
 */
public class NevComparator implements Comparator <Szemely> {
    
    @Override
    public int compare(Szemely sz, Szemely sz2) {
        return sz.getNev().compareToIgnoreCase(sz2.getNev());
    }
}
