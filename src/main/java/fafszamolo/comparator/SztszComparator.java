package fafszamolo.comparator;

import fafszamolo.model.Szemely;
import java.util.Comparator;

/**
 *
 * @author
 */
public class SztszComparator implements Comparator <Szemely> {
    
    @Override
    public int compare(Szemely sz, Szemely sz2) {
        return sz.getSztsz() - sz2.getSztsz();
    }
}
