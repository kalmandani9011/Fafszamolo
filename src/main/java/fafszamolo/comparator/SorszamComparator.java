package fafszamolo.comparator;

import fafszamolo.model.Szemely;
import java.util.Comparator;

/**
 *
 * @author
 */
public class SorszamComparator implements Comparator <Szemely> {
    
    @Override
    public int compare(Szemely sz, Szemely sz2) {
        return sz.getSorszam().compareTo(sz2.getSorszam());
    }
}
